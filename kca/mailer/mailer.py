from django.core.mail import EmailMultiAlternatives, EmailMessage, get_connection
from .constants import *
from .email_templates import *
import threading
from django.conf import settings

class SendNotification(threading.Thread):
    def __init__(self, logged_in_user, callback, *callback_args):
        threading.Thread.__init__(self)

        # self.request = request
        self.logged_in_user = logged_in_user
        self.callback = callback
        self.args = callback_args
    def send_user_create_mail(self, registered_user, raw_password, to_addr, from_addr=COMPANY_MAIL_ADDR):
        print "hereeeeeeeeeeee"
        to_addr = self._clean_to_addr(to_addr)
        from_addr = self._clean_from_addr(from_addr)

        self.subject = USER_CREATE_SUBJECT
        context = RequestContext(
            self.request,
            {
                'registered_user': registered_user,
                'raw_password': raw_password,
                'login_link': self.request.build_absolute_uri(settings.LOGIN_URL),
                'company_name': COMPANY_NAME
            }
        )
        self.msg = Template(USER_CREATE_MSG).render(context)
        print "okkkkkkkkkkk"
        mail_obj = EmailMultiAlternatives(
            self.subject,
            '',  # we have nothing in plain text to send.
            from_addr,
            to_addr
        )
        mail_obj.attach_alternative(self.msg, "text/html")
        print "suceesssssss"
        return mail_obj.send()