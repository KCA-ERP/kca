import os
import re
import base64
import hashlib
import tempfile
import shutil
import random
from contextlib import contextmanager
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger, InvalidPage


def paginator(objects, page_no, per_page):
    paginator = Paginator(objects, per_page)
    try:
        objects = paginator.page(page_no)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        objects = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        objects = paginator.page(paginator.num_pages)

    try:
        prev_page_no = objects.previous_page_number()
    except InvalidPage as e:
        prev_page_no = page_no
    try:
        next_page_no = objects.next_page_number()
    except InvalidPage as e:
        next_page_no = page_no
    current_page_number = objects.number
    num_of_pages = objects.paginator.num_pages
    return {
        'objects': objects,
        'prev_page_no': prev_page_no,
        'next_page_no': next_page_no,
        'current_page_number': current_page_number,
        'num_of_pages': num_of_pages
    }


def random_subset(iterator, K):
    result = []
    N = 0

    for item in iterator:
        N += 1
        if len(result) < K:
            result.append(item)
        else:
            s = int(random.random() * N)
            if s < K:
                result[s] = item

    return result


@contextmanager
def tempinput(file_):
    temp = tempfile.NamedTemporaryFile(delete=False)
    shutil.copyfileobj(file_, temp)
    os.chmod(temp.name, 777)
    temp.close()
    yield temp.name
    os.unlink(temp.name)


def django_template_converter(template):
    re_to_check = r'\[([^]]*)\]'
    re_to_replace = r'{{\1}}'
    return {
        'tokens': re.findall(re_to_check, template),
        'template': re.sub(re_to_check, re_to_replace, template)
    }


def get_random_token():
    return base64.b64encode(hashlib.sha256( str(random.getrandbits(256)) ).digest(), random.choice(['rA','aZ','gQ','hH','hG','aR','DD'])).rstrip('==')


def template_cleaner(template):
    clean_regex = {
        r'([."])P([\d]+)': r'\1P%s\2',
        r'([."])T([\d]+)': r'\1T%s\2',
        r'(<title.+>)(.+)(</title>)': r'\1\3'
    }
    for key, value in clean_regex.iteritems():
        try:
            template = re.sub(key, value % get_random_token(), template)
        except TypeError as e:
            template = re.sub(key, value, template)
    return template
