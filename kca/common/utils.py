import re
from enum import Enum


class BaseEnum(object):

    def as_dict(self):
        return self.__dict__

    def values(self):
        return self.__dict__.values()

    def keys(self):
        return self.__dict__.keys()

    def value(self, key):
        return self.__dict__.get(key, '')


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        # determine user ip if he uses proxy
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class Menu(Enum):
    administration_company = 1
    administration_branch = 2
    administration_financial_year = 3
    administration_user = 4
    administration_user_permission = 5
    
    store_master = 6
    store_transactions_rfq = 6

    store_master_store=8
    store_master_rack=10
    store_master_unit=16
    store_master_brand=17
    store_master_size=18
    store_master_pattern=19
    store_master_itemgroup=13
    store_master_itemsubgroup=14
    store_master_item=12
    store_master_supplier=20
    store_master_category=21
    store_master_itemrackmapping=22

    @staticmethod
    def get(member):
        try:
            return getattr(Menu, member)
        except AttributeError, e:
            return None
