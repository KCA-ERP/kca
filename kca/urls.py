from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.views import login, logout
from .views import HomeView, SetModule, SetBranch, ChangePassword

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'kca.views.home', name='home'),
                       # url(r'^accounts/login', login),
                       url(r'^accounts/login',
                           'django.contrib.auth.views.login', name='login_page'),
                       url(r'^logout$', 'django.contrib.auth.views.logout_then_login',
                           name='logout'),
                       # url(r'^changepassword$',
                       #     'django.contrib.auth.views.password_change', {
                       #         'post_change_redirect': '/password-changed/', 'template_name': 'password.html'},
                       #     name='changepassword'),
                       url(r'^changepassword$', ChangePassword.as_view(),
                           name='changepassword'),
                       # {'post_change_redirect': '/'}
                       url(r'^administration/',
                           include('administration.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', HomeView.as_view(),
                           name='home'),
                       url(r'^customapi/', include('customapi.urls')),
                       url(r'^set_branch/(?P<id>\d+)/', SetBranch.as_view(),
                           name='set_branch'),
                       url(r'^set_module/(?P<id>\d+)/', SetModule.as_view(),
                           name='set_module'),
                       url(r'^store/',
                           include('store.urls')),
                       )
