var storeApp = angular.module('store', ['angularFileUpload']);

storeApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});

storeApp.directive('numbersOnly', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           // this next if is necessary for when using ng-required on your input. 
           // In such cases, when a letter is typed first, this parser will be called
           // again, and the 2nd time, the value will be undefined
           if (inputValue == undefined) return '' 
           var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
           if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           }  
           return transformedInput;         
       });
     }
   };
});

function dateconverter(date) {
    date = date.split("/");
    day = date[0];
    month = date[1];
    year = date[2];
    return (year + "-" + month + "-" + day);
}

function revertdateconverter(date) {
    date = date.split("-");
    day = date[0];
    month = date[1];
    year = date[2];
    return (year + "/" + month + "/" + day);
}

function datetimespliter(datetime) {
    var date = datetime.split("T");
    return (date[0]);
}



storeApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

storeApp.controller('rfqController', function($scope, $http, $interval, $timeout) {
    $scope.item_details = [];
    $scope.items_to_send = [];
    $scope.items = new Array();
    $scope.users = new Array();
    $scope.rfq_details = new Array();
    $scope.search_text = '';
    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.user_id = '';
    $scope.rfq_id = '';
    $scope.row_inserted = false;
    $scope.item_select_error = "";

    $scope.get_users = function() {
        $http.get(
            '/customapi/users', {
                'responseType': 'json',
                'params': {
                    'filter_branch': true,
                }
            }
        ).success(function(data) {
            if (data.objects) {
                $scope.users = data.objects;
            }
        }).error(function() {
        })
    }

    $scope.get_rfqs = function(page_no, per_page, no_of_item) {
        $http.get(
            '/customapi/rfqs', {
                'responseType': 'json',
                'params': {
                    'page_no': page_no,
                    'per_page': per_page,
                    'search_text': $scope.search_text,
                    'show_all': no_of_item
                }
            }
        ).success(function(data) {
            if (data.objects) {
                $scope.rfq_details = data.objects;
            }
            $scope.cur_page = data.current_page_number;
            $scope.next_page = data.next_page_number;
            $scope.prev_page = data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;
            $scope.rfq_no = data.next_rfq_no;
        }).error(function() {
        })
    }

    $scope.get_rfq_items = function(rfq_id){

    }

    $scope.get_items = function(){
         $http.get(
            '/customapi/items', {
                'responseType': 'json',
                'params': {
                    'show_all':1
                }
            }).success(function(data) {
            if (data.objects) {
                $scope.items = data.objects;
                $scope.item_details = [
                    {
                        item_code: $scope.items,
                        item_short_code: $scope.items,
                        item_description: $scope.items,
                        req_quantity: '',
                    }
                ];
            }}).error(function() {})
    }
    $scope.get_users();
    $scope.get_items();
    $scope.get_rfqs();  
    $scope.nextPage = function() {
        $scope.get_rfqs($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_rfqs($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_rfqs($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_rfqs($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_rfqs($scope.cur_page, $scope.per_page);
    }

    $scope.selectFile = function (files) {
        for (var i = 0; i < files.length; i++) {
            var fl = files[i];
            console.log(fl);
            // $upload.upload({
            //     url: 'my/upload/url',
            //     file: $fl,
            //     progress: function (e) { }
            // }).then(function (data, status, headers, config) {
            //     console.log(data);
            // });
        }
    }
    $scope.resetItems = function(){
        $scope.item_details = [
            {
                item_code: $scope.items,
                item_short_code: $scope.items,
                item_description: $scope.items,
                req_quantity: '',
            }
        ];
    }
    $scope.itemSelected =function(index,id){
        $scope.item_details[index].item_code = id;
        $scope.item_details[index].item_short_code = id;
        $scope.item_details[index].item_description = id;   
        }

    $scope.addItem = function(index,item_id,req_quantity){
        var data_filled=true;
        $scope.item_select_error = '';
        console.log($scope.item_details);

        if (item_id == '' || item_id == null)
            data_filled = false;
        if (req_quantity == '' || req_quantity == null)
            data_filled = false;
        for (var i = 0; i<$scope.items_to_send.length; i++){
            if ($scope.items_to_send[i].item_id == item_id){
                data_filled = false;
                break;
            }
        }
        for (var i = 0; i< $scope.item_details.length; i++){
            if (typeof($scope.item_details[i].item_code) != 'number' || typeof($scope.item_details[i].item_short_code) != 'number' || typeof($scope.item_details[i].item_description) != 'number'){
                data_filled = false;
                break;
            }
            if ($scope.item_details[i].req_quantity == ''){
                data_filled = false;
                break;
            }
        }
        if (data_filled){
            $scope.item_select_error = "";
            $scope.row_inserted = true;
            console.log($scope.item_details[index].item_code);
            $scope.items_to_send.push({
                'item_id': item_id,
                'req_quantity': req_quantity
            });
            $scope.item_details.push({
                item_code: $scope.items,
                item_short_code: $scope.items,
                item_description: $scope.items,
                req_quantity: '',
            });
        }
        else{
            $scope.item_select_error = "* Please fill all the fields"
        }

    }
    $scope.removeItem = function(index){
        $scope.item_select_error = '';
        $scope.item_details.splice(index, 1);
    }
    $scope.resetAlerts = function(){
        $scope.alert_success = "";
        $scope.alert_failed = "";
        $scope.rfq_no_error = "";
        $scope.rfq_date_error = "";
        $scope.quot_date_error = "";
        $scope.user_select_error = "";
        $scope.item_select_error = "";
    }
    $scope.save_rfq_details = function() {
        $scope.resetAlerts();
        var rfq_date = $('#rfq_date').val();
        var quot_date = $('#quot_date').val();
        var rfq_no = $('#rfq_no').val();
        var terms = $('#terms').val();
        var is_valid = true;
        console.log($scope.item_details);
        if (rfq_no == "") {
            $scope.rfq_no_error = "* This field is required";
            is_valid = false;
        }
        var today = new Date();
        console.log('rfq_date');
        var rfqDate = new Date(rfq_date);
        var quotDate = new Date(quot_date);
        if (rfq_date == "") {
            $scope.rfq_date_error = "* This field is required";
            is_valid = false;
        }
        else if (rfqDate.valueOf() > today.valueOf()){
            $scope.rfq_date_error = "* Future date is not permitted";
            is_valid = false;
        }

        if (quot_date == "" ) {
            $scope.quot_date_error = "* This field is required";
            is_valid = false;
        } else if (quotDate.valueOf() < rfqDate.valueOf()) {
            $scope.quot_date_error = "* This date should be greater than or equal to RFQ date.";
            is_valid = false;
        }

        $scope.items_to_send = [];
        console.log('monitor');
        for (var i =0; i< $scope.item_details.length; i++){
            console.log($scope.item_details[i].req_quantity);
            if (typeof($scope.item_details[i].item_code) != 'number' || typeof($scope.item_details[i].item_short_code) != 'number' || typeof($scope.item_details[i].item_description) != 'number'){
                $scope.item_select_error = "* Please select an item.";
                is_valid = false;
                break;
            }
            if ($scope.item_details[i].req_quantity == ""){
                $scope.item_select_error = "* Please fill required quantity field.";
                is_valid = false;
                console.log('true');
                break;
            }
            else{
                console.log('false');
            }
            $scope.items_to_send.push({
                'item_id': $scope.item_details[i].item_description,
                'req_quantity': $scope.item_details[i].req_quantity
            });
        }

        if ($scope.user_id == ""){
            $scope.user_select_error = "* This field is required";
            is_valid = false;
        }
        // if ($scope.items_to_send.length ==0){
        //     $scope.item_select_error = "* This field is required";
        // }
        var rfqdate = dateconverter($('#rfq_date').val());
        var quotdate = dateconverter($('#quot_date').val());
        if (!$scope.edit_mode) {
            if (is_valid) {
                console.log('start send');
                $scope.values_to_pass = {
                    'rfq_no': rfq_no,
                    'rfq_date': rfqdate,
                    'quot_date': quotdate,
                    'terms': terms,
                    'forwarded_to': $scope.user_id,
                    'item_details': $scope.items_to_send,
                    'data_add_mac': location.host,
                }
                console.log('sending');
                $http.post(
                    '/customapi/rfqs',
                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "RFQ added successfully";
                        $scope.get_rfqs();
                        $timeout(function() {
                            $scope.alert_success = "";
                            $scope.clearForm();
                        }, 3000);
                    } else {
                        $scope.alert_failed = "Failed to add RFQ";
                        $scope.rfq_no_error = data.form_errors.rfq_no_error;
                        $scope.item_select_error = data.form_errors.item_select_error;
                        $timeout(function() {
                            $scope.alert_failed = "";
                        }, 3000);
                    }
                }).error(function() {
                })
            } else {
                $scope.alert_failed = "Failed to add RFQ";
            }
        } else {
            if (is_valid) {
                $scope.values_to_pass = {
                    'rfq_no': rfq_no,
                    'rfq_date': rfqdate,
                    'quot_date': quotdate,
                    'terms': terms,
                    'forwarded_to': $scope.user_id,
                    'item_details': $scope.items_to_send,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/rfq/' + $scope.rfq_id,
                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "RFQ Edited successfully";
                        $scope.get_rfqs();
                        $scope.edit_mode = false;
                        $timeout(function() {
                            $scope.alert_success = "";
                            $scope.clearForm();
                        }, 3000);

                    } else {
                        if (data.form_errors.alert_failed){
                            $scope.alert_failed = data.form_errors.alert_failed;
                        }
                        else{
                            $scope.alert_failed = "Failed to Edit RFQ";
                        }
                        if(data.form_errors.rfq_no_error)
                            $scope.rfq_no_error = data.form_errors.rfq_no_error;
                        if(data.form_errors.item_select_error)
                            $scope.item_select_error = data.form_errors.item_select_error;
                        $scope.get_rfqs();
                        $scope.edit_mode = false;
                        $timeout(function() {
                            $scope.alert_success = "";
                            $scope.clearForm();
                        }, 3000);
                    }
                }).error(function() {
                })
            } else {
                $scope.alert_failed = "Failed to add RFQ";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }
    $scope.clearForm = function() {
        $scope.rfq_date = "";
        $scope.quot_date = "";
        $scope.terms = "";
        $scope.user_id = "";
        $scope.resetItems();
    }

    $scope.delete_rfq = function(id) {
        $scope.resetAlerts();
        $scope.loading = true;
        bootbox.confirm("Are you sure want to delete this RFQ?", function(result) {
            if (result == true) {
                $http.post(
                    '/customapi/rfq/' + id, [], {
                        'responseType': 'json',
                        'headers': {
                            'X-HTTP-Method-Override': 'DELETE'
                        }
                    }).success(function(response) {
                    if (response.success) {
                        $scope.alert_success = "RFQ Deleted successfully";
                        $.each($scope.rfq_details, function(index, rfq_details) {
                            if (rfq_details.pk == id) {
                                $scope.rfq_details.splice(index, 1);
                                $scope.clearForm();
                            }
                        });
                    } else {
                        $scope.alert_failed = "This record is already in use. Deletion restricted.";
                    }
                });
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 5000);
            }
        });
    }
    $scope.edit_rfq = function(id) {
        $scope.resetAlerts();
        $scope.rfq_id = id;
        $http.get(
            '/customapi/rfq/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            response = JSON.parse(data.objects);
            $scope.rfq_no = response.rfq_no;
            $scope.rfq_date = revertdateconverter(datetimespliter(response.rfq_date));
            $scope.quot_date = revertdateconverter(datetimespliter(response.rfq_last_date));
            $scope.terms = response.terms_condition;
            $scope.user_id = response.forwarded_to;            
            for (var i = 0; i< response.items.length; i++){
                if (i==0){
                    $scope.item_details= [];
                }
                $scope.item_details.push({
                    item_code: $scope.items,
                    item_short_code: $scope.items ,
                    item_description: $scope.items,
                    req_quantity: '',
                });
                $scope.item_details[i].item_code = response.items[i].item_id;
                $scope.item_details[i].item_short_code = response.items[i].item_id;
                $scope.item_details[i].item_description = response.items[i].item_id;  
                $scope.item_details[i].req_quantity = response.items[i].req_qty;  
            }
        });
    }

    $scope.view_rfq = function(id) {
        $scope.rfq_id = id;
        $http.get(
            '/customapi/rfq/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            response = JSON.parse(data.objects);
            $scope.rfq_no = response.rfq_no;
            $scope.rfq_date = revertdateconverter(datetimespliter(response.rfq_date));
            $scope.quot_date = revertdateconverter(datetimespliter(response.rfq_last_date));
            $scope.terms = response.terms_condition;
            $scope.user_id = response.forwarded_to;            
            for (var i = 0; i< response.items.length; i++){
                if (i==0){
                    $scope.item_details= [];
                }
                $scope.item_details.push({
                    item_code: $scope.items,
                    item_short_code: $scope.items,
                    item_description: $scope.items,
                    req_quantity: '',
                });
                $scope.item_details[i].item_code = response.items[i].item_id;
                $scope.item_details[i].item_short_code = response.items[i].item_id;
                $scope.item_details[i].item_description = response.items[i].item_id;  
                $scope.item_details[i].req_quantity = response.items[i].req_qty;  
            }
        });
    }

});






// Store Master
storeApp.controller('storeMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.stores = new Array()
    $scope.search_text = '';
    $scope.count = 0;

    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.store_code = "";
    $scope.store_name = "";
    $scope.remarks = "";
    $scope.alert_success = "";
    $scope.alert_failed = "";
    $scope.store_code_error = "";
    $scope.store_name_error = "";
    $scope.get_stores = function(page_no, per_page,no_of_item){
        $http.get(
        // '/api/administration/financialyears/',
        '/customapi/stores',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.stores = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_stores();
    $scope.nextPage = function () {
        $scope.get_stores($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_stores($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_stores($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_stores($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_stores($scope.cur_page, $scope.per_page);
    }


    $scope.save_store = function() {

        var is_valid = 1;
        if ($scope.store_code == "") {
            $scope.store_code_error = "* This field is required";
            is_valid = 0;
        }
        if ($scope.store_name == "") {
            $scope.store_name_error = "* This field is required";
            is_valid = 0;
        }
        if (!$scope.edit_mode) {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'store_code': $scope.store_code,
                    'store_name': $scope.store_name,
                    'remarks': $scope.remarks,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/stores',

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Store added successfully";
                        $scope.clear_form_fields();
                        $scope.get_stores();
                    } else {
                        $scope.alert_failed = "Failed to add Store";
                        $scope.store_code_error = data.form_errors.store_code;
                        $scope.store_name_error = data.form_errors.store_name;
                        $scope.get_stores();
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Store";
            }
        } else {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'store_code': $scope.store_code,
                    'store_name': $scope.store_name,
                    'remarks': $scope.remarks,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/store/' + $scope.store_id,

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Store Edited successfully";
                        $scope.get_stores();
                        $scope.store_code = "";
                        $scope.store_name = "";
                        $scope.remarks = "";
                        $scope.edit_mode = false;
                        $scope.get_stores();

                    } else {
                        $scope.alert_failed = "Failed to Edit Store";
                        $scope.store_code_error = data.form_errors.store_code;
                        $scope.store_name_error = data.form_errors.store_name;
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Store";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_store = function(id){
        $scope.clear_form_fields();
        $scope.store_id = id;
        $http.get(
        '/customapi/store/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.store_code = response[0].fields.store_code;
            $scope.store_name = response[0].fields.store_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_store = function(id){
        $scope.clear_form_fields();
        $scope.store_id = id;
        $http.get(
        '/customapi/store/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.store_code = response[0].fields.store_code;
            $scope.store_name = response[0].fields.store_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_store = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this store?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/store/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Store Deleted successfully";
                        $.each($scope.stores, function (index, store) {
                            if (store.pk == id) {
                              $scope.stores.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.store_delete_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.store_code = "";
        $scope.store_name = "";
        $scope.remarks = "";
        $scope.store_code_error = "";
        $scope.store_name_error = "";
    }
});


// RackMaster
storeApp.controller('rackMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.store_id = "";
    $scope.search_text = "";
    $scope.rack_location = "";
    $scope.remarks = "";

    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.get_currentbranch_store = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/stores',
        {
            'responseType' : 'json',
            'params' : {
                'show_all': 1,
                'currentbranch_store' : 1,
            }
        }
        ).success(function(data){
            if(data.objects){
                $scope.stores = data.objects;
            }
            $scope.store_id = $scope.stores[0].pk;

        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_currentbranch_store();
    
    $scope.get_racks = function(page_no, per_page,no_of_item){
        $http.get(
        // '/api/administration/financialyears/',
        '/customapi/racks',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.racks = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_racks();
    $scope.nextPage = function () {
        $scope.get_racks($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_racks($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_racks($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_racks($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_racks($scope.cur_page, $scope.per_page);
    }

    $scope.save_rack = function(){
        $scope.rack_location_error = " ";
        $scope.store_id_error = " ";
        var is_valid = 1;
        if($scope.store_id==""){
            $scope.store_id_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.rack_location == ""){
            $scope.rack_location_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'store_id' : $scope.store_id,
                    'rack_location' : $scope.rack_location,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/racks',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Rack added successfully";
                        $scope.clear_form_fields();
                        $scope.get_racks();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Rack";
                        $scope.rack_location_error = data.form_errors.rack_location;
                        $scope.get_racks();
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Rack";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'store_id' : $scope.store_id,
                    'rack_location' : $scope.rack_location,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/rack/'+$scope.rack_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Rack Edited successfully";
                        $scope.store_code = "";
                        $scope.store_name = "";
                        $scope.remarks = "";
                        $scope.edit_mode = false;
                        $scope.get_racks();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Rack";
                        $scope.rack_location_error = data.form_errors.rack_location;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Rack";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_rack = function(id){
        $scope.clear_form_fields();
        $scope.rack_id = id;
        $http.get(
        '/customapi/rack/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;

            console.log(data.objects);
            response = data.objects;
            $scope.store_id = response[0].fields.store_id.pk;
            $scope.rack_location = response[0].fields.rack_location;
            $scope.remarks = response[0].fields.remarks;
            console.log($scope.storemodel);
            });     
    }

    $scope.view_rack = function(id){
        $scope.clear_form_fields();
        $scope.rack_id = id;
        $http.get(
        '/customapi/rack/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.store_id = response[0].fields.store_id.pk;
            $scope.rack_location = response[0].fields.rack_location;
            $scope.remarks = response[0].fields.remarks;
            console.log($scope.storemodel);
            });     
    }

    $scope.delete_rack = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this rack?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/rack/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Rack Deleted successfully";
                        $.each($scope.racks, function (index, rack) {
                            if (rack.pk == id) {
                              $scope.racks.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.store_id = "";
        $scope.rack_location = "";
        $scope.remarks = "";
        $scope.rack_location_error = " ";
        $scope.store_id_error = " ";
    }
});

// UnitMaster

storeApp.controller('unitMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.unit_name = "";
    $scope.get_units = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/units',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.units = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_units();
    $scope.nextPage = function () {
        $scope.get_units($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_units($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_units($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_units($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_units($scope.cur_page, $scope.per_page);
    }

    $scope.save_unit = function(){
        $scope.unit_name_error = " ";
        var is_valid = 1;
        if($scope.unit_name==""){
            $scope.unit_name_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'unit_name' : $scope.unit_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/units',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Unit added successfully";
                        $scope.clear_form_fields();
                        $scope.get_units();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Unit";
                        $scope.unit_name_error = data.form_errors.unit_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Unit";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'unit_name' : $scope.unit_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/unit/'+$scope.unit_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Unit Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_units();
                        $scope.clear_form_fields();

                    }
                    else{
                        // alert("false");
                        $scope.alert_failed = "Failed to Edit Unit";
                        $scope.unit_name_error = data.form_errors.unit_name;
                        // $scope.get_financialyears();
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Unit";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_unit = function(id){
        $scope.clear_form_fields();
        $scope.unit_id = id;
        $http.get(
        '/customapi/unit/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.unit_name = response[0].fields.unit_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_unit = function(id){
        $scope.clear_form_fields();
        $scope.unit_id = id;
        $http.get(
        '/customapi/unit/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.unit_name = response[0].fields.unit_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_unit = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this unit?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/unit/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Unit Deleted successfully";
                        $.each($scope.units, function (index, unit) {
                            if (unit.pk == id) {
                              $scope.units.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }
    $scope.clear_form_fields = function(){
        $scope.unit_name = "";
        $scope.remarks = "";
        $scope.unit_name_error = " ";
    }
});

// BrandMaster
storeApp.controller('brandMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.brand_name = "";
    $scope.brand_code = "";
    $scope.remarks = "";
    $scope.get_brands = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/brands',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.brands = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_brands();
    $scope.nextPage = function () {
        $scope.get_brands($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_brands($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_brands($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_brands($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_brands($scope.cur_page, $scope.per_page);
    }

    $scope.save_brand = function(){
        $scope.brand_code_error = " ";
        $scope.brand_name_error = " ";
        var is_valid = 1;
        if($scope.brand_code==""){
            $scope.brand_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.brand_name==""){
            $scope.brand_name_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'brand_code' : $scope.brand_code,
                    'brand_name' : $scope.brand_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/brands',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Brand added successfully";
                        $scope.clear_form_fields();
                        $scope.get_brands();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Brand";
                        $scope.brand_code_error = data.form_errors.brand_code;
                        $scope.brand_name_error = data.form_errors.brand_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Brand";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'brand_code' : $scope.brand_code,
                    'brand_name' : $scope.brand_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/brand/'+$scope.brand_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Brand Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_brands();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Brand";
                        $scope.brand_code_error = data.form_errors.brand_code;
                        $scope.brand_name_error = data.form_errors.brand_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Brand";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_brand = function(id){
        $scope.clear_form_fields();
        $scope.brand_id = id;
        $http.get(
        '/customapi/brand/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.brand_code = response[0].fields.brand_code;
            $scope.brand_name = response[0].fields.brand_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_brand = function(id){
        $scope.clear_form_fields();
        $scope.brand_id = id;
        $http.get(
        '/customapi/brand/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.brand_code = response[0].fields.brand_code;
            $scope.brand_name = response[0].fields.brand_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_brand = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this brand?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/brand/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Brand Deleted successfully";
                        $.each($scope.brands, function (index, brand) {
                            if (brand.pk == id) {
                              $scope.brands.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.brand_code = "";
        $scope.brand_name = "";
        $scope.remarks = "";
        $scope.brand_code_error = " ";
        $scope.brand_name_error = " ";
    }
});

// SizeMaster
storeApp.controller('sizeMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.size_name = "";
    $scope.remarks = "";

    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.get_sizes = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/sizes',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.sizes = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_sizes();
    $scope.nextPage = function () {
        $scope.get_sizes($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_sizes($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_sizes($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_sizes($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_sizes($scope.cur_page, $scope.per_page);
    }

    $scope.save_size = function(){
        $scope.size_name_error = " ";
        var is_valid = 1;
        if($scope.size_name==""){
            $scope.size_name_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'size_name' : $scope.size_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/sizes',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Size added successfully";
                        $scope.clear_form_fields();
                        $scope.get_sizes();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Size";
                        $scope.size_name_error = data.form_errors.size_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Brand";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'size_name' : $scope.size_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/size/'+$scope.size_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Size Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_sizes();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Size";
                        $scope.size_name_error = data.form_errors.size_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Size";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_size = function(id){
        $scope.clear_form_fields();
        $scope.size_id = id;
        $http.get(
        '/customapi/size/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.size_name = response[0].fields.size_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.view_size = function(id){
        $scope.clear_form_fields();
        $scope.size_id = id;
        $http.get(
        '/customapi/size/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.size_name = response[0].fields.size_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.delete_size = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this size?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/size/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Size Deleted successfully";
                        $.each($scope.sizes, function (index, size) {
                            if (size.pk == id) {
                              $scope.sizes.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.size_name = "";
        $scope.remarks = "";
        $scope.size_name_error = " ";
    }
});

// PatternMaster
storeApp.controller('patternMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.pattern_name = "";
    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.get_patterns = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/patterns',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.patterns = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_patterns();
    $scope.nextPage = function () {
        $scope.get_patterns($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_patterns($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_patterns($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_patterns($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_patterns($scope.cur_page, $scope.per_page);
    }

    $scope.save_pattern = function(){
        $scope.pattern_name_error = " ";
        var is_valid = 1;
        if($scope.pattern_name==""){
            $scope.pattern_name_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'pattern_name' : $scope.pattern_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/patterns',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Pattern added successfully";
                        $scope.clear_form_fields();
                        $scope.get_patterns();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Pattern";
                        $scope.pattern_name_error = data.form_errors.pattern_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Pattern";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'pattern_name' : $scope.pattern_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/pattern/'+$scope.pattern_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Pattern Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_patterns();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Pattern";
                        $scope.pattern_name_error = data.form_errors.pattern_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Pattern";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_pattern = function(id){
        $scope.clear_form_fields();
        $scope.pattern_id = id;
        $http.get(
        '/customapi/pattern/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.pattern_name = response[0].fields.pattern_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.view_pattern = function(id){
        $scope.clear_form_fields();
        $scope.pattern_id = id;
        $http.get(
        '/customapi/pattern/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.pattern_name = response[0].fields.pattern_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.delete_pattern = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this pattern?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/pattern/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Pattern Deleted successfully";
                        $.each($scope.patterns, function (index, pattern) {
                            if (pattern.pk == id) {
                              $scope.patterns.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.pattern_name = "";
        $scope.remarks = "";
        $scope.pattern_name_error = " ";
    }

});


// ItemGroup
storeApp.controller('itemGroupListController', function ($scope, $http, $interval,$timeout) {
    $scope.itemgroup_code = "";
    $scope.itemgroup_name = "";
    $scope.remarks = "";
    $scope.corporate_account_id = "";
    $scope.search_text = "";

    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.get_parent_corporateaccounts = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/corporateaccounts',
        {
            'responseType' : 'json',
            'params' : {
                'show_all': 1,
                'parent_corporate_accounts' :1,
            }
        }
        ).success(function(data){
            if(data.objects){
                $scope.corporate_accounts = data.objects;
            }
        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_parent_corporateaccounts();
    
    $scope.get_itemgroups = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/itemgroups',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.itemgroups = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_itemgroups();
    $scope.nextPage = function () {
        $scope.get_itemgroups($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_itemgroups($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_itemgroups($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_itemgroups($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_itemgroups($scope.cur_page, $scope.per_page);
    }

    $scope.save_itemgroup = function(){
        $scope.itemgroup_code_error = " ";
        $scope.itemgroup_name_error = " ";
        $scope.corporate_account_id_error = " ";

        var is_valid = 1;
        if($scope.itemgroup_code==""){
            $scope.itemgroup_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.itemgroup_name == ""){
            $scope.itemgroup_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.corporate_account_id == ""){
            $scope.corporate_account_id_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'itemgroup_code' : $scope.itemgroup_code,
                    'itemgroup_name' : $scope.itemgroup_name,
                    'purchaseaccount_id' : $scope.corporate_account_id,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/itemgroups',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Itemgroup added successfully";
                        $scope.clear_form_fields();
                        $scope.get_itemgroups();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Itemgroup";
                        $scope.itemgroup_code_error = data.form_errors.itemgroup_code;
                        $scope.itemgroup_name_error = data.form_errors.itemgroup_name;
                        $scope.corporate_account_id_error = data.form_errors.corporate_account_id;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Itemgroup";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'itemgroup_code' : $scope.itemgroup_code,
                    'itemgroup_name' : $scope.itemgroup_name,
                    'purchaseaccount_id' : $scope.corporate_account_id,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/itemgroup/'+$scope.itemgroup_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Itemgroup Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_itemgroups();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Itemgroup";
                        $scope.itemgroup_code_error = data.form_errors.itemgroup_code;
                        $scope.itemgroup_name_error = data.form_errors.itemgroup_name;
                        $scope.corporate_account_id_error = data.form_errors.corporate_account_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Itemgroup";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_itemgroup = function(id){
        $scope.clear_form_fields();
        $scope.itemgroup_id = id;
        $http.get(
        '/customapi/itemgroup/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;

            console.log(data.objects);
            response = data.objects;
            $scope.itemgroup_code = response[0].fields.itemgroup_code;
            $scope.itemgroup_name = response[0].fields.itemgroup_name;
            $scope.corporate_account_id = response[0].fields.purchaseaccount_id.pk;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_itemgroup = function(id){
        $scope.clear_form_fields();
        $scope.rack_id = id;
        $http.get(
        '/customapi/itemgroup/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.itemgroup_code = response[0].fields.itemgroup_code;
            $scope.itemgroup_name = response[0].fields.itemgroup_name;
            $scope.corporate_account_id = response[0].fields.purchaseaccount_id.pk;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_itemgroup = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this itemgroup?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/itemgroup/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Itemgroup Deleted successfully";
                        $.each($scope.itemgroups, function (index, itemgroup) {
                            if (itemgroup.pk == id) {
                              $scope.itemgroups.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.itemgroup_delete_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.itemgroup_code = "";
        $scope.itemgroup_name = "";
        $scope.corporate_account_id = "";
        $scope.remarks = "";
        $scope.itemgroup_code_error = " ";
        $scope.itemgroup_name_error = " ";
        $scope.corporate_account_id_error = " ";

    }
});



// ItemSubGroup
storeApp.controller('itemSubGroupListController', function ($scope, $http, $interval,$timeout) {
    $scope.item_subgroup_code = "";
    $scope.item_subgroup_name = "";
    $scope.remarks = "";
    $scope.itemgroup_id = "";
    $scope.search_text = "";

    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.get_itemgroups = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/itemgroups',
        {
            'responseType' : 'json',
            'params' : {
                'show_all': 1,
            }
        }
        ).success(function(data){
            if(data.objects){
                $scope.itemgroups = data.objects;
            }
        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_itemgroups();
    
    $scope.get_itemsubgroups = function(page_no, per_page,no_of_item){
        $http.get(
        // '/api/administration/financialyears/',
        '/customapi/itemsubgroups',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.itemsubgroups = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_itemsubgroups();
    $scope.nextPage = function () {
        $scope.get_itemsubgroups($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_itemsubgroups($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_itemsubgroups($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_itemsubgroups($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_itemsubgroups($scope.cur_page, $scope.per_page);
    }

    $scope.save_itemsubgroup = function(){
        $scope.item_subgroup_code_error = " ";
        $scope.item_subgroup_name_error = " ";
        $scope.itemgroup_id_error = " ";

        var is_valid = 1;
        if($scope.item_subgroup_code==""){
            $scope.item_subgroup_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.item_subgroup_name == ""){
            $scope.item_subgroup_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.itemgroup_id == ""){
            $scope.itemgroup_id_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'item_subgroup_code' : $scope.item_subgroup_code,
                    'item_subgroup_name' : $scope.item_subgroup_name,
                    'itemgroup_id' : $scope.itemgroup_id,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/itemsubgroups',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Itemsubgroup added successfully";
                        $scope.clear_form_fields();
                        $scope.get_itemsubgroups();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Itemsubgroup";
                        $scope.item_subgroup_code_error = data.form_errors.item_subgroup_code;
                        $scope.item_subgroup_name_error = data.form_errors.item_subgroup_name;
                        $scope.itemgroup_id_error = data.form_errors.itemgroup_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Itemsubgroup";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'item_subgroup_code' : $scope.item_subgroup_code,
                    'item_subgroup_name' : $scope.item_subgroup_name,
                    'itemgroup_id' : $scope.itemgroup_id,
                    'remarks' : $scope.remarks,
                    'data_modi_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/itemsubgroup/'+$scope.itemsubgroup_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Itemsubgroup Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_itemsubgroups();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Itemsubgroup";
                        $scope.item_subgroup_code_error = data.form_errors.item_subgroup_code;
                        $scope.item_subgroup_name_error = data.form_errors.item_subgroup_name;
                        $scope.itemgroup_id_error = data.form_errors.itemgroup_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Itemsubgroup";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_itemsubgroup = function(id){
        $scope.clear_form_fields();
        $scope.itemsubgroup_id = id;
        $http.get(
        '/customapi/itemsubgroup/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;

            console.log(data.objects);
            response = data.objects;
            $scope.item_subgroup_code = response[0].fields.item_subgroup_code;
            $scope.item_subgroup_name = response[0].fields.item_subgroup_name;
            $scope.itemgroup_id = response[0].fields.itemgroup_id.pk;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_itemsubgroup = function(id){
        $scope.clear_form_fields();
        $http.get(
        '/customapi/itemsubgroup/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.item_subgroup_code = response[0].fields.item_subgroup_code;
            $scope.item_subgroup_name = response[0].fields.item_subgroup_name;
            $scope.itemgroup_id = response[0].fields.itemgroup_id.pk;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_itemsubgroup = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this itemsubgroup?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/itemsubgroup/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Itemsubgroup Deleted successfully";
                        $.each($scope.itemsubgroups, function (index, itemsubgroup) {
                            if (itemsubgroup.pk == id) {
                              $scope.itemsubgroups.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.item_subgroup_code = "";
        $scope.item_subgroup_name = "";
        $scope.remarks = "";
        $scope.itemgroup_id = "";
        $scope.item_subgroup_code_error = " ";
        $scope.item_subgroup_name_error = " ";
        $scope.itemgroup_id_error = " ";

    }
});


// ItemMaster
storeApp.controller('ItemMasterListController', function ($scope, $http, $interval,$timeout, $upload) {

    $scope.item_code = "";
    $scope.item_short_code = "";
    $scope.item_name = "";
    $scope.item_name = "";
    $scope.brand_id = "";
    $scope.size_id = "";
    $scope.unit_id = "";
    $scope.pattern_id = "";
    $scope.itemgroup_id = "";
    $scope.item_subgroup_id = "";
    $scope.reorder_level = "";
    $scope.is_active = "";
    $scope.purchaseaccount_id = "";
    $scope.remarks = "";
    $scope.item_image = "";
    $scope.file = "";

    $scope.edit_mode = false;
    $scope.view_mode = false; 

    //File/Image Upload
    $scope.onFileSelect = function($files) {
        //$files: an array of files selected, each file has name, size, and type.
        for (var i = 0; i < $files.length; i++) {
          $scope.file = $files[i];
          console.log($scope.file);
        }
        alert("1");
    }
    //End
    $scope.fill_required_dropdowns = function(page_no, per_page,no_of_item){
        // 1.Brand
            $http.get(
            '/customapi/brands',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.brands = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 2.Size
            $http.get(
            '/customapi/sizes',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.sizes = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 3.Unit
            $http.get(
            '/customapi/units',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.units = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 4.Pattern
            $http.get(
            '/customapi/patterns',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.patterns = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 5.Group
            $http.get(
            '/customapi/itemgroups',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.itemgroups = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

    }
    $scope.fill_required_dropdowns();

    // To get item sub groups and corporate account related to selected Item group
    $scope.get_data_on_itemgroup_change = function(id){
        // 1.ItemSubGroups
        $http.get(
            '/customapi/itemsubgroups',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                    'itemsubgroups_of_selected_itemgroup' : 1,
                    'search_text':id,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.itemsubgroups = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 2.CorporateAccount(purchaseaccount_id)
        $http.get(
            '/customapi/corporateaccounts',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                    'corporateaccounts_of_selected_itemgroup' : 1,
                    'search_text':id,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.corporateaccounts = data.objects;
                    $scope.purchaseaccount_id = $scope.corporateaccounts[0].pk
                }

            }).error(function(){
                // alert("failed");
            })
    }


    $scope.get_items = function(page_no, per_page,no_of_item){
        $http.get(
        // '/api/administration/financialyears/',
        '/customapi/items',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.items = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_items();
    $scope.nextPage = function () {
        $scope.get_items($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_items($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_items($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_items($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_items($scope.cur_page, $scope.per_page);
    }


    $scope.save_item = function(){
        $scope.item_code_error = " ";
        $scope.item_short_code_error = " ";
        $scope.item_name_error = " ";
        $scope.brand_id_error = " ";
        $scope.unit_id_error = " ";
        $scope.itemgroup_id_error = " ";

        var is_valid = 1;
        if($scope.item_code==""){
            $scope.item_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.item_short_code == ""){
            $scope.item_short_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.item_name == ""){
            $scope.item_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.brand_id == ""){
            $scope.brand_id_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.unit_id == ""){
            $scope.unit_id_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.itemgroup_id == ""){
            $scope.itemgroup_id_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                console.log("111");
                $scope.image_dict={
                    'item_image':$scope.file.name
                }
                console.log($scope.file);
                $scope.values_to_pass = {
                    'item_code' : $scope.item_code,
                    'item_short_code' : $scope.item_short_code,
                    'item_name' : $scope.item_name,
                    'brand_id' : $scope.brand_id,
                    'size_id' : $scope.size_id,
                    'unit_id' : $scope.unit_id,
                    'pattern_id' : $scope.pattern_id,
                    'itemgroup_id' : $scope.itemgroup_id,
                    'item_subgroup_id' : $scope.item_subgroup_id,
                    'reorder_level' : $scope.reorder_level,
                    'is_active' : $scope.is_active,
                    'purchaseaccount_id' : $scope.purchaseaccount_id,
                    'remarks' : $scope.remarks,
                    'item_image' : $scope.file.name,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/items',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Item added successfully";
                        // $scope.clear_form_fields();
                        $scope.get_items();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Item";
                        $scope.item_code_error = data.form_errors.item_code;
                        $scope.item_short_code_error = data.form_errors.item_short_code;
                        $scope.item_name_error = data.form_errors.item_name;
                        $scope.brand_id_error = data.form_errors.brand_id;
                        $scope.unit_id_error = data.form_errors.unit_id;
                        $scope.itemgroup_id_error = data.form_errors.itemgroup_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Item";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'item_code' : $scope.item_code,
                    'item_short_code' : $scope.item_short_code,
                    'item_name' : $scope.item_name,
                    'brand_id' : $scope.brand_id,
                    'size_id' : $scope.size_id,
                    'unit_id' : $scope.unit_id,
                    'pattern_id' : $scope.pattern_id,
                    'itemgroup_id' : $scope.itemgroup_id,
                    'item_subgroup_id' : $scope.item_subgroup_id,
                    'reorder_level' : $scope.reorder_level,
                    'is_active' : $scope.is_active,
                    'purchaseaccount_id' : $scope.purchaseaccount_id,
                    'remarks' : $scope.remarks,
                    'item_image' : $scope.item_image,
                    'data_modi_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/item/'+$scope.item_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Item Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_items();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Item";
                        $scope.item_code_error = data.form_errors.item_code;
                        $scope.item_short_code_error = data.form_errors.item_short_code;
                        $scope.item_name_error = data.form_errors.item_name;
                        $scope.brand_id_error = data.form_errors.brand_id;
                        $scope.unit_id_error = data.form_errors.unit_id;
                        $scope.itemgroup_id_error = data.form_errors.itemgroup_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Item";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_item = function(id){
        $scope.clear_form_fields();
        $scope.item_id = id;
        $http.get(
        '/customapi/item/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;

            console.log(data.objects);
            response = data.objects;
            $scope.item_code = response[0].fields.item_code;
            $scope.item_short_code = response[0].fields.item_short_code;
            $scope.item_name = response[0].fields.item_name;
            $scope.brand_id = response[0].fields.brand_id.pk;
            $scope.unit_id = response[0].fields.unit_id.pk;
            $scope.itemgroup_id = response[0].fields.itemgroup_id.pk;
            $scope.purchaseaccount_id = response[0].fields.purchaseaccount_id.pk;
            if(response[0].fields.size_id)
                $scope.size_id = response[0].fields.size_id.pk;
            if(response[0].fields.pattern_id)
                $scope.pattern_id = response[0].fields.pattern_id.pk;
            if(response[0].fields.item_subgroup_id)
                $scope.item_subgroup_id = response[0].fields.item_subgroup_id.pk;
            $scope.reorder_level = response[0].fields.reorder_level;
            $scope.is_active = response[0].fields.is_active;
            $scope.item_image = response[0].fields.item_image;
            $scope.remarks = response[0].fields.remarks;
            $scope.get_data_on_itemgroup_change($scope.itemgroup_id); // To initialize subgroup,purchaseaccount scope variables
            });     
    }

    $scope.view_item = function(id){
        $scope.clear_form_fields();
        $scope.item_id = id;
        $http.get(
        '/customapi/item/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.item_code = response[0].fields.item_code;
            $scope.item_short_code = response[0].fields.item_short_code;
            $scope.item_name = response[0].fields.item_name;
            $scope.brand_id = response[0].fields.brand_id.pk;
            $scope.unit_id = response[0].fields.unit_id.pk;
            $scope.itemgroup_id = response[0].fields.itemgroup_id.pk;
            $scope.purchaseaccount_id = response[0].fields.purchaseaccount_id.pk;
            if(response[0].fields.size_id)
                $scope.size_id = response[0].fields.size_id.pk;
            if(response[0].fields.pattern_id)
                $scope.pattern_id = response[0].fields.pattern_id.pk;
            if(response[0].fields.item_subgroup_id){
                $scope.item_subgroup_id = response[0].fields.item_subgroup_id.pk;
                $scope.get_data_on_itemgroup_change($scope.itemgroup_id); // To initialize subgroup,purchaseaccount scope variables
            }
            $scope.reorder_level = response[0].fields.reorder_level;
            $scope.is_active = response[0].fields.is_active;
            $scope.item_image = response[0].fields.item_image;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_item = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this item?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/item/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Item Deleted successfully";
                        $.each($scope.items, function (index, item) {
                            if (item.pk == id) {
                              $scope.items.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.item_code = "";
        $scope.item_short_code = "";
        $scope.item_name = "";
        $scope.item_name = "";
        $scope.brand_id = "";
        $scope.size_id = "";
        $scope.unit_id = "";
        $scope.pattern_id = "";
        $scope.itemgroup_id = "";
        $scope.item_subgroup_id = "";
        $scope.reorder_level = "";
        $scope.is_active = "";
        $scope.purchaseaccount_id = "";
        $scope.remarks = "";
        $scope.item_image = "";

        $scope.item_code_error = " ";
        $scope.item_short_code_error = " ";
        $scope.item_name_error = " ";
        $scope.brand_id_error = " ";
        $scope.unit_id_error = " ";
        $scope.itemgroup_id_error = " ";

    }

    });

// SupplierMaster
storeApp.controller('SupplierMasterListController', function ($scope, $http, $interval,$timeout) {

    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.supplier_code = "";
    $scope.supplier_name = "";
    $scope.contact_person = "";
    $scope.contact_phone = "";
    $scope.contact_mobile = "";
    $scope.contact_email = "";
    $scope.supplier_address = "";
    $scope.country_id = "";
    $scope.state_id = "";
    $scope.pincode = "";
    $scope.office_phone = "";
    $scope.office_mobile = "";
    $scope.office_email_id = "";
    $scope.link_account_id = "";
    $scope.is_active = "";
    $scope.is_billwisesettle = "";
    $scope.credit_limit = 0;
    $scope.is_blacklisted = "";
    $scope.supplier_bank = "";
    $scope.supplier_branch = "";
    $scope.supplier_account_no = "";
    $scope.supplier_ifsc = "";
    $scope.supplier_kgst_no = "";
    $scope.supplier_cst_no = "";
    $scope.supplier_pan = "";


    $scope.fill_required_dropdowns = function(page_no, per_page,no_of_item){
        // 1.CountryMaster
            $http.get(
            '/customapi/countries',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.countries = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

    };
    $scope.fill_required_dropdowns();

    $scope.get_data_on_country_change = function(id){
        // 1.StateMaster
        $http.get(
            '/customapi/states',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                    'states_of_selected_country' : 1,
                    'search_text':id,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.states = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })
    }

    $scope.get_suppliers = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/suppliers',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.suppliers = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_suppliers();
    $scope.nextPage = function () {
        $scope.get_suppliers($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_suppliers($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_suppliers($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_suppliers($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_suppliers($scope.cur_page, $scope.per_page);
    }

    $scope.save_supplier = function(){
        $scope.supplier_code_error = " ";
        $scope.supplier_name_error = " ";

        var is_valid = 1;
        if($scope.supplier_code==""){
            $scope.supplier_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.supplier_name == ""){
            $scope.supplier_name_error = "* This field is required";
            is_valid = 0;
        }
        
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'supplier_code' : $scope.supplier_code,
                    'supplier_name' : $scope.supplier_name,
                    'contact_person' : $scope.contact_person,
                    'contact_phone' : $scope.contact_phone,
                    'contact_mobile' : $scope.contact_mobile,
                    'contact_email' : $scope.contact_email,
                    'supplier_address' : $scope.supplier_address,
                    'country_id' : $scope.country_id,
                    'state_id' : $scope.state_id,
                    'pincode' : $scope.pincode,
                    'office_phone' : $scope.office_phone,
                    'office_mobile' : $scope.office_mobile,
                    'office_email_id' : $scope.office_email_id,
                    'is_active' : $scope.is_active,
                    'is_billwisesettle' : $scope.is_billwisesettle,
                    'credit_limit' : $scope.credit_limit,
                    'is_blacklisted' : $scope.is_blacklisted,
                    'supplier_bank' : $scope.supplier_bank,
                    'supplier_branch' : $scope.supplier_branch,
                    'supplier_account_no' : $scope.supplier_account_no,
                    'supplier_ifsc' : $scope.supplier_ifsc,
                    'supplier_kgst_no' : $scope.supplier_kgst_no,
                    'supplier_cst_no' : $scope.supplier_cst_no,
                    'supplier_pan' : $scope.supplier_pan,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/suppliers',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Supplier added successfully";
                        $scope.clear_form_fields();
                        $scope.get_suppliers();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Supplier";
                        $scope.supplier_code_error = data.form_errors.supplier_code;
                        $scope.supplier_name_error = data.form_errors.supplier_name;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Item";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'supplier_code' : $scope.supplier_code,
                    'supplier_name' : $scope.supplier_name,
                    'contact_person' : $scope.contact_person,
                    'contact_phone' : $scope.contact_phone,
                    'contact_mobile' : $scope.contact_mobile,
                    'contact_email' : $scope.contact_email,
                    'supplier_address' : $scope.supplier_address,
                    'country_id' : $scope.country_id,
                    'state_id' : $scope.state_id,
                    'pincode' : $scope.pincode,
                    'office_phone' : $scope.office_phone,
                    'office_mobile' : $scope.office_mobile,
                    'office_email_id' : $scope.office_email_id,
                    'is_active' : $scope.is_active,
                    'is_billwisesettle' : $scope.is_billwisesettle,
                    'credit_limit' : $scope.credit_limit,
                    'is_blacklisted' : $scope.is_blacklisted,
                    'supplier_bank' : $scope.supplier_bank,
                    'supplier_branch' : $scope.supplier_branch,
                    'supplier_account_no' : $scope.supplier_account_no,
                    'supplier_ifsc' : $scope.supplier_ifsc,
                    'supplier_kgst_no' : $scope.supplier_kgst_no,
                    'supplier_cst_no' : $scope.supplier_cst_no,
                    'supplier_pan' : $scope.supplier_pan,
                    'data_modi_mac' : location.host,
                }
                $http.post(
                    '/customapi/supplier/'+$scope.supplier_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Supplier Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_suppliers();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Supplier";
                        $scope.supplier_code_error = data.form_errors.supplier_code;
                        $scope.supplier_name_error = data.form_errors.supplier_name;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Supplier";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_supplier = function(id){
        $scope.clear_form_fields();
        $scope.supplier_id = id;
        $http.get(
        '/customapi/supplier/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;

            console.log(data.objects);
            response = data.objects;
            $scope.supplier_code = response[0].fields.supplier_code;
            $scope.supplier_name = response[0].fields.supplier_name;
            $scope.contact_person = response[0].fields.contact_person;
            $scope.contact_phone = response[0].fields.contact_phone;
            $scope.contact_mobile = response[0].fields.contact_mobile;
            $scope.contact_email = response[0].fields.contact_email;
            $scope.supplier_address = response[0].fields.supplier_address;
            if(response[0].fields.country_id)
                $scope.country_id = response[0].fields.country_id.pk;
            if(response[0].fields.state_id){
                $scope.state_id = response[0].fields.state_id.pk;
                $scope.get_data_on_country_change($scope.country_id); // To initialize subgroup,purchaseaccount scope variables
            }
            $scope.pincode = response[0].fields.pincode;
            $scope.office_phone = response[0].fields.office_phone;
            $scope.office_mobile = response[0].fields.office_mobile;
            $scope.office_email_id = response[0].fields.office_email_id;
            $scope.is_active = response[0].fields.is_active;
            $scope.is_billwisesettle = response[0].fields.is_billwisesettle;
            $scope.credit_limit = response[0].fields.credit_limit;
            $scope.is_blacklisted = response[0].fields.is_blacklisted;
            $scope.supplier_bank = response[0].fields.supplier_bank;
            $scope.supplier_branch = response[0].fields.supplier_branch;
            $scope.supplier_account_no = response[0].fields.supplier_account_no;
            $scope.supplier_ifsc = response[0].fields.supplier_ifsc;
            $scope.supplier_kgst_no = response[0].fields.supplier_kgst_no;
            $scope.supplier_cst_no = response[0].fields.supplier_cst_no;
            $scope.supplier_pan = response[0].fields.supplier_pan;

            });     
    }

    $scope.view_supplier = function(id){
        $scope.clear_form_fields();
        $scope.supplier_id = id;
        $http.get(
        '/customapi/supplier/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.supplier_code = response[0].fields.supplier_code;
            $scope.supplier_name = response[0].fields.supplier_name;
            $scope.contact_person = response[0].fields.contact_person;
            $scope.contact_phone = response[0].fields.contact_phone;
            $scope.contact_mobile = response[0].fields.contact_mobile;
            $scope.contact_email = response[0].fields.contact_email;
            $scope.supplier_address = response[0].fields.supplier_address;
            if(response[0].fields.country_id)
                $scope.country_id = response[0].fields.country_id.pk;
            if(response[0].fields.state_id){
                $scope.state_id = response[0].fields.state_id.pk;
                $scope.get_data_on_country_change($scope.country_id); // To initialize subgroup,purchaseaccount scope variables
            }
            $scope.pincode = response[0].fields.pincode;
            $scope.office_phone = response[0].fields.office_phone;
            $scope.office_mobile = response[0].fields.office_mobile;
            $scope.office_email_id = response[0].fields.office_email_id;
            $scope.is_active = response[0].fields.is_active;
            $scope.is_billwisesettle = response[0].fields.is_billwisesettle;
            $scope.credit_limit = response[0].fields.credit_limit;
            $scope.is_blacklisted = response[0].fields.is_blacklisted;
            $scope.supplier_bank = response[0].fields.supplier_bank;
            $scope.supplier_branch = response[0].fields.supplier_branch;
            $scope.supplier_account_no = response[0].fields.supplier_account_no;
            $scope.supplier_ifsc = response[0].fields.supplier_ifsc;
            $scope.supplier_kgst_no = response[0].fields.supplier_kgst_no;
            $scope.supplier_cst_no = response[0].fields.supplier_cst_no;
            $scope.supplier_pan = response[0].fields.supplier_pan;

            });     
    }


    $scope.delete_supplier = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this supplier?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/supplier/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Supplier Deleted successfully";
                        $.each($scope.suppliers, function (index, supplier) {
                            if (supplier.pk == id) {
                              $scope.suppliers.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

     $scope.clear_form_fields = function(){
        $scope.supplier_code = "";
        $scope.supplier_name = "";
        $scope.contact_person = "";
        $scope.contact_phone = "";
        $scope.contact_mobile = "";
        $scope.contact_email = "";
        $scope.supplier_address = "";
        $scope.country_id = "";
        $scope.state_id = "";
        $scope.pincode = "";
        $scope.office_phone = "";
        $scope.office_mobile = "";
        $scope.office_email_id = "";
        $scope.link_account_id = "";
        $scope.is_active = "";
        $scope.is_billwisesettle = "";
        $scope.credit_limit = "";
        $scope.is_blacklisted = "";
        $scope.supplier_bank = "";
        $scope.supplier_branch = "";
        $scope.supplier_account_no = "";
        $scope.supplier_ifsc = "";
        $scope.supplier_kgst_no = "";
        $scope.supplier_cst_no = "";
        $scope.supplier_pan = "";

        $scope.supplier_code_error = " ";
        $scope.supplier_name_error = " ";

    }

});


// Category
storeApp.controller('categoryListController', function ($scope, $http, $interval,$timeout) {
    $scope.categories = new Array()
    $scope.search_text = '';
    $scope.count = 0;

    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.category_code = "";
    $scope.category_name = "";
    $scope.remarks = "";
    $scope.alert_success = "";
    $scope.alert_failed = "";
    $scope.category_code_error = "";
    $scope.category_name_error = "";
    $scope.get_categories = function(page_no, per_page,no_of_item){
        $http.get(
        // '/api/administration/financialyears/',
        '/customapi/categories',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.categories = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_categories();
    $scope.nextPage = function () {
        $scope.get_categories($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_categories($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_categories($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_categories($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_categories($scope.cur_page, $scope.per_page);
    }


    $scope.save_category = function() {

        var is_valid = 1;
        if ($scope.category_code == "") {
            $scope.category_code_error = "* This field is required";
            is_valid = 0;
        }
        if ($scope.category_name == "") {
            $scope.category_name_error = "* This field is required";
            is_valid = 0;
        }
        if (!$scope.edit_mode) {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'category_code': $scope.category_code,
                    'category_name': $scope.category_name,
                    'remarks': $scope.remarks,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/categories',

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Category added successfully";
                        $scope.clear_form_fields();
                        $scope.get_categories();
                    } else {
                        $scope.alert_failed = "Failed to add Category";
                        $scope.category_code_error = data.form_errors.category_code;
                        $scope.category_name_error = data.form_errors.category_name;
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Category";
            }
        } else {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'category_code': $scope.category_code,
                    'category_name': $scope.category_name,
                    'remarks': $scope.remarks,
                    'data_modi_mac': location.host,
                }
                $http.post(
                    '/customapi/category/' + $scope.category_id,

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Category Edited successfully";
                        $scope.get_categories();
                        $scope.edit_mode = false;
                        $scope.clear_form_fields();

                    } else {
                        $scope.alert_failed = "Failed to Edit Category";
                        $scope.category_code_error = data.form_errors.category_code;
                        $scope.category_name_error = data.form_errors.category_name;
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Category";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_category = function(id){
        $scope.clear_form_fields();
        $scope.category_id = id;
        $http.get(
        '/customapi/category/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.category_code = response[0].fields.category_code;
            $scope.category_name = response[0].fields.category_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_category = function(id){
        $scope.clear_form_fields();
        $scope.category_id = id;
        $http.get(
        '/customapi/category/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.category_code = response[0].fields.category_code;
            $scope.category_name = response[0].fields.category_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_category = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this category?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/category/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Category Deleted successfully";
                        $.each($scope.categories, function (index, category) {
                            if (category.pk == id) {
                              $scope.categories.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.category_code = "";
        $scope.category_name = "";
        $scope.remarks = "";
        $scope.category_code_error = "";
        $scope.category_name_error = "";
    }
});


// Item-Rack Mapping
storeApp.controller('item_rack_mappingController', function ($scope, $http, $interval,$timeout) {

    $scope.rack_id = "";
    $scope.store_id = "";
    $scope.item_id = "";

    $scope.itemgroup_id = "";
    $scope.item_subgroup_id = "";
    $scope.search_text="";

    $scope.items = new Array();

    // to get all stores of current selected branch
    $scope.get_currentbranch_store = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/stores',
        {
            'responseType' : 'json',
            'params' : {
                'show_all': 1,
                'currentbranch_store' : 1,
            }
        }
        ).success(function(data){
            if(data.objects){
                $scope.stores = data.objects;
            }
            // $scope.store_id = $scope.stores[0].pk
        })
    }
    $scope.get_currentbranch_store();

    // to get all Itemgroups
    $scope.get_itemgroups = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/itemgroups',
        {
            'responseType' : 'json',
            'params' : {
                'show_all': 1,
            }
        }
        ).success(function(data){
            if(data.objects){
                $scope.itemgroups = data.objects;
            }

        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_itemgroups();
    
    // to get all item-rack mappings 
    $scope.get_itemrack_mappings = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/itemrackmappings',
        {
            'responseType' : 'json',
            'params' : {
                // 'page_no':page_no,  
                // 'per_page' : per_page,
                // 'search_text':$scope.search_text,
                'show_all': 1
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.item_rack_mappings = data.objects;
                console.log(data.objects);
            }
        })
    }

    $scope.get_itemrack_mappings();

    // to get all items
    $scope.get_items = function(page_no, per_page,no_of_item){
        var exists=false;
        $scope.item_details = [];
        $http.get(
        '/customapi/items',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : 25,
                'search_text':$scope.search_text,
                'itemgroup_id':$scope.itemgroup_id,
                'item_subgroup_id':$scope.item_subgroup_id,
                'item_description':1,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.items = data.objects;
                console.log(data.objects);
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


            // customizes objects list
            $.each($scope.items, function(index, item) {
                $.each($scope.item_rack_mappings, function(index, item_rack_mapping) {
                    if (item_rack_mapping.fields.item_id.pk == item.pk && item_rack_mapping.fields.store_id.pk==$scope.store_id) {
                        $scope.item_details.push({
                            pk: item.pk,
                            item_code: item.fields.item_code,
                            item_short_code:  item.fields.item_short_code ,
                            item_description: item.fields.item_description,
                            rack_id:item_rack_mapping.fields.rack_id.pk,
                        });
                        exists=true;
                    }
                });
                if(!exists){
                    $scope.item_details.push({
                        pk: item.pk,
                        item_code: item.fields.item_code,
                        item_short_code:  item.fields.item_short_code ,
                        item_description: item.fields.item_description,
                        rack_id:'',
                    });
                }
                exists =false;
            });
            // end

        }).error(function(){
            // alert("failed");
        })
    }
    // $scope.get_items();
    $scope.nextPage = function () {
        $scope.get_items($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_items($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_items($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_items($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_items($scope.cur_page, $scope.per_page);
    }

    // to get all racks of selected store 
    $scope.get_selectedstore_racks = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/racks',
        {
            'responseType' : 'json',
            'params' : {
                // 'page_no':page_no,  
                // 'per_page' : per_page,
                'selected_store_racks' : 1,
                'pk':$scope.store_id,
                'show_all': 1
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                console.log("racks");
                $scope.racks = data.objects;
                $scope.racks.unshift({
                    'fields':{rack_location:"Select"},
                    'pk':-1,
                });
                console.log(data.objects);
            }

        })
    }

    // To get item sub groups selected Item group
    $scope.get_subgroups_on_itemgroup_change = function(id){
        // 1.ItemSubGroups
        $scope.item_subgroup_id = "";
        $http.get(
            '/customapi/itemsubgroups',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                    'itemsubgroups_of_selected_itemgroup' : 1,
                    'search_text':id,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.itemsubgroups = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

    }

    //to save item-rack mapping 
    $scope.save_mapping = function(row,rack_id){
        console.log(row);
        console.log(rack_id);
        $scope.values_to_pass = {
            'store_id': $scope.store_id,
            'rack_id': rack_id,
            'item_id': row.pk,
        }
        $http.post(
            '/customapi/itemrackmappings',

            [$scope.values_to_pass], {
                'responseType': 'json'
            }
        ).success(function(data) {
            if (data.success) {
                $scope.alert_success = data.form_msgs.success;
            } else {
                $scope.alert_failed = "Failed to add Mapping";
            }
        }).error(function() {
            // alert("failed");
        })
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);

    }


    
});