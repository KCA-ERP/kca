var kcaApp = angular.module('kca', []);

kcaApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});


function dateconverter(date) {
    date = date.split("/");
    day = date[0];
    month = date[1];
    year = date[2];
    return (year + "-" + month + "-" + day);
}

function revertdateconverter(date) {
    date = date.split("-");
    day = date[0];
    month = date[1];
    year = date[2];
    return (year + "/" + month + "/" + day);
}

function datetimespliter(datetime) {
    var date = datetime.split("T");
    return (date[0]);
}

kcaApp.controller('companyController', function($scope, $http, $interval, $timeout) {
    
    $scope.clearForm = function() {
        $('#compname').val('');
        $('#addr1').val('');
        $('#addr2').val('');
        $('#addr3').val('');
        $('#phone').val('');
        $('#raddr1').val('');
        $('#id_company_rptaddrline2').val('');
        $('#id_company_logo').val('');
    }

});

kcaApp.controller('financialyearController', function($scope, $http, $interval, $timeout) {
    $scope.financialyears = new Array()
    $scope.search_text = '';
    $scope.count = 0;
    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.get_financialyears = function(page_no, per_page, no_of_item) {
        $http.get(
            // '/api/administration/financialyears/',
            '/customapi/financialyears', {
                'responseType': 'json',
                'params': {
                    'page_no': page_no,
                    'per_page': per_page,
                    'search_text': $scope.search_text,
                    'show_all': no_of_item
                }
            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.financialyears = data.objects;
            }
            $scope.cur_page = data.current_page_number;
            $scope.next_page = data.next_page_number;
            $scope.prev_page = data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_financialyears();
    $scope.nextPage = function() {
        $scope.get_financialyears($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_financialyears($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_financialyears($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_financialyears($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_financialyears($scope.cur_page, $scope.per_page);
    }
    $scope.save_financialyear = function() {
        $scope.alert_success = "";
        $scope.alert_failed = "";
        $scope.fin_year_code_error = "";
        $scope.startdate_error = "";
        $scope.enddate_error = "";
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        var fin_year_code = $('#fyc').val();
        var is_valid = 1;
        if (fin_year_code == "") {
            $scope.fin_year_code_error = "* This field is required";
            is_valid = 0;
        }
        if (startdate == "") {
            $scope.startdate_error = "* This field is required";
            is_valid = 0;
        }
        if (enddate == "") {
            $scope.enddate_error = "* This field is required";
            is_valid = 0;
        } else if (startdate > enddate) {
            $scope.enddate_error = "* Startdate should be less than enddate";
            is_valid = 0;
        }
        var startdate = dateconverter($('#startdate').val());
        var enddate = dateconverter($('#enddate').val());
        if (!$scope.edit_mode) {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'fin_year_code': fin_year_code,
                    'startdate': startdate,
                    'enddate': enddate,
                    'data_add_mac': location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/financialyears',

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Financialyear added successfully";
                        $scope.get_financialyears();
                    } else {
                        $scope.alert_failed = "Failed to add Financialyear";
                        $scope.fin_year_code_error = data.form_errors.fin_year_code;
                        $scope.get_financialyears();
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Financialyear";
            }
        } else {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'fin_year_code': fin_year_code,
                    'startdate': startdate,
                    'enddate': enddate,
                    'data_add_mac': location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/financialyear/' + $scope.financialyear_id,

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Financialyear Edited successfully";
                        $scope.get_financialyears();
                        $scope.fyc = "";
                        $scope.startdate = "";
                        $scope.enddate = "";
                        $scope.edit_mode = false;

                    } else {
                        // alert("false");
                        $scope.alert_failed = "Failed to Edit Financialyear";
                        $scope.fin_year_code_error = data.form_errors.fin_year_code;
                        $scope.get_financialyears();
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Financialyear";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }
    $scope.clear_form_fields = function() {
        $scope.fyc = "";
        $scope.startdate = "";
        $scope.enddate = "";
    }

    $scope.delete_financialyear = function(id) {
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this financialyear?", function(result) {
            if (result == true) {
                $http.post(
                    '/customapi/financialyear/' + id, [], {
                        'responseType': 'json',
                        'headers': {
                            'X-HTTP-Method-Override': 'DELETE'
                        }
                    }).success(function(response) {
                    if (response.success) {
                        $scope.alert_success = "Financialyear Deleted successfully";
                        $.each($scope.financialyears, function(index, financialyears) {
                            if (financialyears.pk == id) {
                                $scope.financialyears.splice(index, 1);
                            }
                        });
                    } else {
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }
    $scope.edit_financialyear = function(id) {
        $scope.financialyear_id = id;
        $http.get(
            '/customapi/financialyear/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.fyc = response[0].fields.fin_year_code;
            $scope.startdate = revertdateconverter(datetimespliter(response[0].fields.starts_from));
            $scope.enddate = revertdateconverter(datetimespliter(response[0].fields.starts_to));
        });
    }

    $scope.view_financialyear = function(id) {
        $scope.financialyear_id = id;
        $http.get(
            '/customapi/financialyear/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            response = data.objects;
            $scope.fyc = response[0].fields.fin_year_code;
            $scope.startdate = revertdateconverter(datetimespliter(response[0].fields.starts_from));
            $scope.enddate = revertdateconverter(datetimespliter(response[0].fields.starts_to));
        });
    }

});


kcaApp.controller('branchController', function($scope, $http, $interval, $timeout) {
    $scope.branches = new Array()
    $scope.search_text = '';
    $scope.view_mode = false;
    $scope.edit_mode = false;

    $scope.get_branches = function(page_no, per_page, no_of_item) {
        $http.get(
            '/customapi/branches', {
                'responseType': 'json',
                'params': {
                    'page_no': page_no,
                    'per_page': per_page,
                    'search_text': $scope.search_text,
                    'show_all': no_of_item
                }
            }
        ).success(function(data) {
            if (data.objects) {
                $scope.branches = data.objects;
            }
            $scope.cur_page = data.current_page_number;
            $scope.next_page = data.next_page_number;
            $scope.prev_page = data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function() {})
    }
    $scope.get_branches();
    $scope.nextPage = function() {
        $scope.get_branches($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_branches($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_branches($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_branches($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_branches($scope.cur_page, $scope.per_page);
    }
    $scope.save_branch = function() {
        $scope.alert_success = "";
        $scope.alert_failed = "";
        $scope.branch_code_error = "";
        $scope.branch_name_error = "";
        $scope.head_office_exists_error = "";
        var branch_code = $('#branch_code').val();
        var branch_name = $('#branch_name').val();
        var branch_address1 = $('#add1').val();
        var branch_address2 = $('#add2').val();
        var branch_address3 = $('#add3').val();
        var branch_phone = $('#branch_phone').val();
        var branch_rptaddrline1 = $('#radd1').val();
        var branch_rptaddrline2 = $('#radd2').val();
        var is_head_office = $scope.is_head_office;

        var is_valid = true;
        if (branch_code == "") {
            $scope.branch_code_error = "* This field is required";
            is_valid = false;
        }
        if (branch_name == "") {
            $scope.branch_name_error = "* This field is required";
            is_valid = false;
        }

        if (!$scope.edit_mode) {
            // add operation
            if (is_valid) {
                $scope.values_to_pass = {
                    'branch_code': branch_code,
                    'branch_name': branch_name,
                    'branch_address1': branch_address1,
                    'branch_address2': branch_address2,
                    'branch_address3': branch_address3,
                    'branch_phone': branch_phone,
                    'branch_rptaddrline1': branch_rptaddrline1,
                    'branch_rptaddrline2': branch_rptaddrline2,
                    'is_head_office': is_head_office,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/branches', [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Branch added successfully";
                        $scope.get_branches();
                        $timeout(function() {
                            $scope.alert_failed = "";
                            $scope.alert_success = "";
                            $scope.clear_form_fields();
                        }, 3000);
                    } else {
                        $scope.branch_code_error = data.form_errors.branch_code_error;
                        $scope.branch_name_error = data.form_errors.branch_name_error;
                        $scope.head_office_exists_error = data.form_errors.head_office_exists_error;
                    }
                }).error(function() {})
            } else {
                $scope.alert_failed = "Failed to add branch";
            }
        } else {
            // edit operation
            if (is_valid) {
                $scope.values_to_pass = {
                    'branch_code': branch_code,
                    'branch_name': branch_name,
                    'branch_address1': branch_address1,
                    'branch_address2': branch_address2,
                    'branch_address3': branch_address3,
                    'branch_phone': branch_phone,
                    'branch_rptaddrline1': branch_rptaddrline1,
                    'branch_rptaddrline2': branch_rptaddrline2,
                    'is_head_office': is_head_office,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/branch/' + $scope.branch_id,

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Branch Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_branches();
                        $timeout(function() {
                            $scope.alert_failed = "";
                            $scope.alert_success = "";
                            $scope.clear_form_fields();
                        }, 3000);
                    } else {
                        $scope.alert_failed = "Failed to Edit Branch";
                        $scope.branch_code_error = data.form_errors.branch_code_error;
                        $scope.branch_name_error = data.form_errors.branch_name_error;
                        $scope.head_office_exists_error = data.form_errors.head_office_exists_error;
                    }
                }).error(function() {})
            } else {
                $scope.alert_failed = "Failed to add Financialyear";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }
    $scope.edit_branch = function(id) {
        $scope.view_mode = false;
        $scope.branch_id = id;
        $http.get(
            '/customapi/branch/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            response = data.objects;
            console.log(response[0]);
            $scope.branch_code = response[0].fields.branch_shortname;
            $scope.branch_name = response[0].fields.branch_name;
            $scope.add1 = response[0].fields.branch_addressline1;
            $scope.add2 = response[0].fields.branch_addressline2;
            $scope.add3 = response[0].fields.branch_addressline3;
            $scope.branch_phone = response[0].fields.branch_phone;
            $scope.radd1 = response[0].fields.branch_rptaddrline1;
            $scope.radd2 = response[0].fields.branch_rptaddrline2;
            $scope.is_head_office = response[0].fields.is_corporate_office;
            $scope.edit_mode = true;
        });
    }
    $scope.view_branch = function(id) {
        $scope.edit_mode = false;
        $scope.branch_id = id;
        $http.get(
            '/customapi/branch/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            response = data.objects;
            console.log(response[0]);
            $scope.branch_code = response[0].fields.branch_shortname;
            $scope.branch_name = response[0].fields.branch_name;
            $scope.add1 = response[0].fields.branch_addressline1;
            $scope.add2 = response[0].fields.branch_addressline2;
            $scope.add3 = response[0].fields.branch_addressline3;
            $scope.branch_phone = response[0].fields.branch_phone;
            $scope.radd1 = response[0].fields.branch_rptaddrline1;
            $scope.radd2 = response[0].fields.branch_rptaddrline2;
            $scope.is_head_office = response[0].fields.is_corporate_office;
            $scope.view_mode = true;
        });
    }
    $scope.delete_branch = function(id) {
        $scope.view_mode = false;
        $scope.edit_mode = false;
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this branch?", function(result) {
            if (result == true) {
                $http.post(
                    '/customapi/branch/' + id, [], {
                        'responseType': 'json',
                        'headers': {
                            'X-HTTP-Method-Override': 'DELETE'
                        }
                    }).success(function(response) {
                    if (response.success) {
                        $scope.alert_success = "Branch Deleted successfully";
                        $.each($scope.branches, function(index, branches) {
                            if (branches.pk == id) {
                                $scope.branches.splice(index, 1);
                            }
                        });
                    } else {
                        $scope.alert_failed = "Failed to delete Branch";
                    }
                });
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }
    $scope.clear_form_fields = function() {
        $scope.branch_code = "";
        $scope.branch_name = "";
        $scope.add1 = "";
        $scope.add2 = "";
        $scope.add3 = "";
        $scope.branch_phone = "";
        $scope.radd1 = "";
        $scope.radd2 = "";
        $scope.is_head_office = "";
    }
});


kcaApp.controller('userPermissionController', function($scope, $http, $interval, $timeout) {
    $scope.users = new Array();
    $scope.module_details = new Array();
    $scope.search_text = '';
    $scope.count = 0;
    $scope.edit = 0;
    $scope.last_checked = false;
    $scope.editing_user = '';

    $scope.selectall_add = 1;
    $scope.selectall_edit = 1;
    $scope.selectall_delete = 1;
    $scope.selectall_view = 1;

    $scope.selectall_add_permissions = function(module_name, permission) {
        if ($scope.module_details) {
            for (var i = 0; i < $scope.module_details.length; i++) {
                console.log($scope.module_details[i].module_name);
                if ($scope.module_details[i].module_name == module_name) {
                    console.log('correct');
                    for (j = 0; j < $scope.module_details[i].menus.length; j++) {
                        console.log($scope.module_details[i].menus[j]);
                        console.log('before');
                        console.log($scope.module_details[i].menus[j].add_permitted);
                        $scope.module_details[i].menus[j].add_permitted = true;
                        console.log('after');
                        console.log($scope.module_details[i].menus[j].add_permitted);

                    }
                    break;
                }
                //console.log($scope.module_details[i]['menus']);
            }
        }
        console.log('loop finished');
        console.log($scope.module_details);
        // angular.forEach($scope.module_details,function(module){
        // 	$scope.branch_selection.push(item.pk) 
        // 	});
    };

    $scope.toggleSelection = function(module_id, menu_id, permission_type) {
        if ($scope.module_details) {
            for (var i = 0; i < $scope.module_details.length; i++) {
                if ($scope.module_details[i].module_id == module_id) {
                    for (var j = 0; j < $scope.module_details[i].menus.length; j++) {
                        if ($scope.module_details[i].menus[j].menu_id == menu_id) {
                            switch (permission_type) {
                                case "add_permitted":
                                    $scope.module_details[i].menus[j].add_permitted = !$scope.module_details[i].menus[j].add_permitted;
                                    break;
                                case "edit_permitted":
                                    $scope.module_details[i].menus[j].edit_permitted = !$scope.module_details[i].menus[j].edit_permitted;
                                    break;
                                case "delete_permitted":
                                    $scope.module_details[i].menus[j].delete_permitted = !$scope.module_details[i].menus[j].delete_permitted;
                                    break;
                                case "view_permitted":
                                    $scope.module_details[i].menus[j].view_permitted = !$scope.module_details[i].menus[j].view_permitted;
                                    break;
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
    };


    $scope.get_users = function(page_no, per_page, no_of_item) {
        $http.get(
            '/customapi/activeusers', {
                'responseType': 'json',
                'params': {
                    'page_no': page_no,
                    'per_page': per_page,
                    'search_text': $scope.search_text,
                    'show_all': no_of_item
                }
            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.users = data.objects;
            }
            $scope.cur_page = data.current_page_number;
            $scope.next_page = data.next_page_number;
            $scope.prev_page = data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_users();
    $scope.nextPage = function() {
        $scope.get_users($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_users($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    }
    $scope.save_permissions = function() {
        $scope.alert_success = "";
        $scope.alert_failed = "";

        $http.post(
            '/customapi/userpermission/' + $scope.editing_user,
            $scope.module_details, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }, {
                'responseType': 'json'
            }
        ).success(function(data) {
            if (data.success) {
                $scope.alert_success = "Permissions successfully updated";
                $scope.editing_user = '';
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                    $scope.module_details = [];
                }, 3000);
            } else {

            }
        }).error(function() {

        })
    }

    $scope.edit_permission = function(id) {
        $http.get(
            '/customapi/userpermission/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit = 1;
            $scope.editing_user = id;
            response = JSON.parse(data.objects);
            console.log('our data');
            console.log(response);
            console.log(response[0]);
            $scope.module_details = response;
        });
    }

});


kcaApp.controller('passwordController', function($scope, $http, $interval, $timeout) {
    $scope.alert_success = "";
    $scope.alert_failed = "";

    $scope.change_password = function() {
        $scope.values_to_pass = {
            old_password: $('#id_old_password').val(),
            new_password1: $('#id_new_password1').val(),
            new_password2: $('#id_new_password2').val()
        }
        $http.post(
            '/customapi/changepassword', [$scope.values_to_pass], {
                'responseType': 'json'
            }
        ).success(function(data) {
            if (data.success == true) {
                console.log('data');
                console.log(data);
                $scope.alert_success = "Password successfully changed, please re-login.";
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
                clearFormErrors('change_password_form');
                $('#id_old_password').val('');
                $('#id_new_password1').val('');
                $('#id_new_password2').val('');

            } else {
                showFormErrors(data.errors, 'change_password_form');
                $scope.alert_failed = "Password change failed.";
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
            }
        }).error(function(data) {});
    };
    $scope.clearForm = function() {
        $('#id_old_password').val('');
        $('#id_new_password1').val('');
        $('#id_new_password2').val('');
        clearFormErrors('change_password_form');
    }

});



kcaApp.controller('Administartion_UserController', function($scope, $http, $interval, $timeout) {
    $scope.branches = new Array();
    $scope.modules = new Array();
    $scope.users = new Array();
    $scope.alert_success = "";
    $scope.alert_failed = "";
    $scope.username = "";
    $scope.address1 = "";
    $scope.address2 = "";
    $scope.phone = "";
    $scope.email = "";
    $scope.is_active = " ";
    $scope.email_error = " ";
    $scope.username_error = " ";

    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.search_text = '';
    $scope.count = 0;

    $scope.selectall_branch = 1;
    $scope.selectall_module = 1;

    $scope.get_branches = function() {
        $http.get(
            '/customapi/branches', {
                'responseType': 'json',

            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.branches = data.objects;
                console.log
            }

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_branches();
    $scope.get_modules = function() {
        $http.get(
            '/customapi/modules', {
                'responseType': 'json',

            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.modules = data.objects;
                console.log
            }

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_modules();
    $scope.save_user = function() {
            $scope.email_error = " ";
            $scope.username_error = " ";
            var is_valid = 1;
            if ($scope.username == "") {
                $scope.username_error = "* This field is required";
                is_valid = 0;
            }
            var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;

            if (!emailFilter.test($scope.email)) {
                $scope.email_error = 'Please enter a valid e-mail address.';
                is_valid = 0;
            }
            if ($scope.email == "") {
                $scope.email_error = "* This field is required";
                is_valid = 0;
            }
            if (!$scope.edit_mode) {
                if (is_valid) {
                    $scope.values_to_pass = {
                        'username': $scope.username,
                        'address1': $scope.address1,
                        'address2': $scope.address2,
                        'phone': $scope.phone,
                        'email': $scope.email,
                        'data_add_mac': "",
                    }
                    $http.post(
                        '/customapi/users',

                        [$scope.values_to_pass, $scope.branch_selection, $scope.module_selection], {
                            'responseType': 'json'
                        }
                    ).success(function(data) {
                        if (data.success) {
                            $scope.alert_success = "User added successfully";
                            $scope.get_users();
                        } else {
                            $scope.alert_failed = "Failed to add user";
                            $scope.email_error = data.form_errors.email_error;
                        }
                        $timeout(function() {
                            $scope.alert_failed = "";
                            $scope.alert_success = "";
                        }, 3000);
                    }).error(function() {
                        // alert("failed");
                    })
                } else {
                    $scope.alert_failed = "Failed to add user";
                }
            } else {
                if (is_valid) {
                    $scope.values_to_pass = {
                        'username': $scope.username,
                        'address1': $scope.address1,
                        'address2': $scope.address2,
                        'phone': $scope.phone,
                        'email': $scope.email,
                        'data_add_mac': "",
                        'is_active': $scope.is_active,
                    }
                    $http.post(
                        '/customapi/user/' + $scope.user_id,

                        [$scope.values_to_pass, $scope.branch_selection, $scope.module_selection], {
                            'responseType': 'json'
                        }
                    ).success(function(data) {
                        console.log(data);
                        if (data.success) {
                            $scope.alert_success = "User Edited successfully";
                            $scope.clear_form_fields();
                            $scope.get_users();
                        } else {
                            $scope.alert_failed = "Failed to Edit User";
                            $scope.email_error = data.form_errors.email_error;
                        }

                    }).error(function() {
                        // alert("failed");
                    })
                } else {
                    $scope.alert_failed = "Failed to Edit User";
                }

            }
            $timeout(function() {
                $scope.alert_failed = "";
                $scope.alert_success = "";
            }, 3000);

        }
        /* Creating Selected Module's List*/
    $scope.module_selection = [];
    $scope.mod_toggleSelection = function toggleSelection(module_id) {
        var idx = $scope.module_selection.indexOf(module_id);
        // is currently selected
        if (idx > -1) {
            $scope.module_selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.module_selection.push(module_id);
        }
        console.log($scope.module_selection);
    };
    /* Creating Selected Branch's List*/
    $scope.branch_selection = [];
    $scope.br_toggleSelection = function toggleSelection(module_id) {
        var idx = $scope.branch_selection.indexOf(module_id);
        // is currently selected
        if (idx > -1) {
            $scope.branch_selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.branch_selection.push(module_id);
        }
        console.log($scope.branch_selection);
    };

    $scope.selectall_branches = function() {
        $scope.branch_selection = [];
        if ($scope.selectall_branch) {
            angular.forEach($scope.branches, function(item) {
                $scope.branch_selection.push(item.pk)
            });
            $scope.selectall_branch = "";
            console.log($scope.branch_selection);
        } else {
            $scope.branch_selection = [];
            $scope.selectall_branch = 1;
            console.log($scope.branch_selection);
        }

    }
    $scope.selectall_modules = function() {
        $scope.module_selection = [];
        if ($scope.selectall_module) {
            angular.forEach($scope.modules, function(item) {
                $scope.module_selection.push(item.pk)
            });
            $scope.selectall_module = "";
            console.log($scope.module_selection);
        } else {
            $scope.module_selection = [];
            $scope.selectall_module = 1;
            console.log($scope.module_selection);
        }

    }

    $scope.get_users = function(page_no, per_page, no_of_item) {
        $http.get(
            '/customapi/users', {
                'responseType': 'json',
                'params': {
                    'page_no': page_no,
                    'per_page': per_page,
                    'search_text': $scope.search_text,
                    'show_all': no_of_item
                }
            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.users = data.objects;
            }
            $scope.cur_page = data.current_page_number;
            $scope.next_page = data.next_page_number;
            $scope.prev_page = data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_users();
    $scope.nextPage = function() {
        $scope.get_users($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_users($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    }

    $scope.delete_user = function(id) {
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this user?", function(result) {
            if (result == true) {
                $http.post(
                    '/customapi/user/' + id, [], {
                        'responseType': 'json',
                        'headers': {
                            'X-HTTP-Method-Override': 'DELETE'
                        }
                    }).success(function(response) {
                    if (response.success) {
                        $scope.alert_success = "User Deleted successfully";
                        console.log($scope.users);
                        $.each($scope.users, function(index, users) {
                            if (users.pk == id) {
                                $scope.users.splice(index, 1);
                            }
                        });
                    } else {
                        // $scope.alert_failed = "Failed to delete User";
                        // alert("false");
                        $scope.alert_failed = response.form_errors.user_delete_error;
                        $scope.get_financialyears();
                    }
                });
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.edit_user = function(id) {
        $scope.user_id = id;
        $http.get(
            '/customapi/user/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            if (data.success) {
                $scope.edit_mode = true;
                $scope.view_mode = false;
                // console.log(data.objects);
                response = data.objects;
                $scope.username = response[0].fields.user.fields.first_name;
                $scope.address1 = response[0].fields.address1;
                $scope.address2 = response[0].fields.address2;
                $scope.phone = response[0].fields.phone;
                $scope.email = response[0].fields.user.fields.email;
                // $scope.is_active = 1;
                $scope.is_active = response[0].fields.user.fields.is_active;
                $scope.module_list = response[0].fields.modules;
                $scope.branch_list = response[0].fields.branches;
                $scope.branch_selection = $scope.branch_list;
                $scope.module_selection = $scope.module_list;
                // console.log($scope.module_list);
            }

        });
    }

    $scope.view_user = function(id) {
        $scope.user_id = id;
        $http.get(
            '/customapi/user/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            if (data.success) {
                $scope.edit_mode = false;
                $scope.view_mode = true;
                // console.log(data.objects);
                response = data.objects;
                $scope.username = response[0].fields.user.fields.first_name;
                $scope.address1 = response[0].fields.address1;
                $scope.address2 = response[0].fields.address2;
                $scope.phone = response[0].fields.phone;
                $scope.email = response[0].fields.user.fields.email;
                // $scope.is_active = 1;
                $scope.is_active = response[0].fields.user.fields.is_active;
                $scope.module_list = response[0].fields.modules;
                $scope.branch_list = response[0].fields.branches;
                $scope.branch_selection = $scope.branch_list;
                $scope.module_selection = $scope.module_list;
                // console.log($scope.module_list);
            }
        });
    }

    $scope.check_module = function(module_id) {
        return $scope.module_list.indexOf(module_id);
    }
    $scope.user_activate = function() {
        $scope.is_active = !$scope.is_active;
    }
    $scope.clear_form_fields = function() {
            $scope.username = "";
            $scope.address1 = "";
            $scope.address2 = "";
            $scope.phone = "";
            $scope.email = "";
            $scope.is_active = "";
            $scope.module_selection = [];
            $scope.branch_selection = [];
            $scope.email_error = " ";
            $scope.username_error = " ";
            $scope.alert_failed = "";
            $scope.alert_success = "";

        }
});




// Store Master
kcaApp.controller('storeMasterListController', function ($scope, $http, $interval,$timeout) {
	$scope.stores = new Array()
	$scope.search_text = '';
  	$scope.count = 0;

  	$scope.edit_mode = false;
    $scope.view_mode = false;

  	$scope.store_code = "";
	$scope.store_name = "";
	$scope.remarks = "";
	$scope.alert_success = "";
	$scope.alert_failed = "";
	$scope.store_code_error = "";
	$scope.store_name_error = "";
	$scope.get_stores = function(page_no, per_page,no_of_item){
		$http.get(
		// '/api/administration/financialyears/',
		'/customapi/stores',
		{
			'responseType' : 'json',
			'params' : {
				'page_no':page_no,	
				'per_page' : per_page,
				'search_text':$scope.search_text,
      			'show_all': no_of_item
			}
		}
		).success(function(data){
			// this callback will be called asynchronously
       		// when the response is available
       		if(data.objects){
       			$scope.stores = data.objects;
       		}
       		$scope.cur_page=data.current_page_number;
		    $scope.next_page=data.next_page_number;
		    $scope.prev_page=data.previous_page_number;
		    $scope.last_page = data.last_page;
		    $scope.total = data.num_of_pages;
		    $scope.per_page = data.per_page;

		}).error(function(){
			// alert("failed");
		})
	}
	$scope.get_stores();
	$scope.nextPage = function () {
        $scope.get_stores($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_stores($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_stores($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_stores($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_stores($scope.cur_page, $scope.per_page);
    }


    $scope.save_store = function() {

        var is_valid = 1;
        if ($scope.store_code == "") {
            $scope.store_code_error = "* This field is required";
            is_valid = 0;
        }
        if ($scope.store_name == "") {
            $scope.store_name_error = "* This field is required";
            is_valid = 0;
        }
        if (!$scope.edit_mode) {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'store_code': $scope.store_code,
                    'store_name': $scope.store_name,
                    'remarks': $scope.remarks,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/stores',

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Store added successfully";
                        $scope.clear_form_fields();
                        $scope.get_stores();
                    } else {
                        $scope.alert_failed = "Failed to add Store";
                        $scope.store_code_error = data.form_errors.store_code;
                        $scope.store_name_error = data.form_errors.store_name;
                        $scope.get_stores();
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Store";
            }
        } else {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'store_code': $scope.store_code,
                    'store_name': $scope.store_name,
                    'remarks': $scope.remarks,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/store/' + $scope.store_id,

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Store Edited successfully";
                        $scope.get_stores();
                        $scope.store_code = "";
                        $scope.store_name = "";
                        $scope.remarks = "";
                        $scope.edit_mode = false;
                        $scope.get_stores();

                    } else {
                        $scope.alert_failed = "Failed to Edit Store";
                        $scope.store_code_error = data.form_errors.store_code;
                        $scope.store_name_error = data.form_errors.store_name;
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Store";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_store = function(id){
    	$scope.clear_form_fields();
		$scope.store_id = id;
  		$http.get(
		'/customapi/store/'+id,
			{
				'responseType' : 'json'
		    }
		).success(function(data, status, headers, config) {
			$scope.edit_mode = true;
            $scope.view_mode = false;
			console.log(data.objects);
			response = data.objects;
			$scope.store_code = response[0].fields.store_code;
			$scope.store_name = response[0].fields.store_name;
			$scope.remarks = response[0].fields.remarks;
			});		
  	}

    $scope.view_store = function(id){
        $scope.clear_form_fields();
        $scope.store_id = id;
        $http.get(
        '/customapi/store/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.store_code = response[0].fields.store_code;
            $scope.store_name = response[0].fields.store_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

  	$scope.delete_store = function(id){
	    $scope.loading = true;
	    bootbox.confirm("Are sure want to delete this store?", function(result) {
	        if(result==true){
			    $http.post(
			    '/customapi/store/'+id,
			    [],
			    {
			      'responseType' : 'json',
			      'headers': {'X-HTTP-Method-Override': 'DELETE'}
			    }).success(function(response){
			    	if (response.success) {
			        	$scope.alert_success = "Store Deleted successfully";
			            $.each($scope.stores, function (index, store) {
			            	if (store.pk == id) {
			                  $scope.stores.splice(index,1);
			            	}
			        	});
			    	}	
			    	else{
                        $scope.alert_failed = response.form_errors.store_delete_error;
			    	}
		     	});
		     	$timeout( function(){ 
			    	$scope.alert_failed = ""; 
			    	$scope.alert_success = "";
			    }, 3000);
	        }
        });
  	}

    $scope.clear_form_fields = function(){
    	$scope.store_code = "";
		$scope.store_name = "";
		$scope.remarks = "";
		$scope.store_code_error = "";
		$scope.store_name_error = "";
    }
});


// RackMaster
kcaApp.controller('rackMasterListController', function ($scope, $http, $interval,$timeout) {
	$scope.store_id = "";
	$scope.search_text = "";
	$scope.rack_location = "";
	$scope.remarks = "";

    $scope.edit_mode = false;
    $scope.view_mode = false;
	$scope.get_currentbranch_store = function(page_no, per_page,no_of_item){
		$http.get(
		'/customapi/stores',
		{
			'responseType' : 'json',
			'params' : {
      			'show_all': 1,
      			'currentbranch_store' : 1,
			}
		}
		).success(function(data){
       		if(data.objects){
       			$scope.stores = data.objects;
       		}
            $scope.store_id = $scope.stores[0].pk;

		}).error(function(){
			// alert("failed");
		})
	}
	$scope.get_currentbranch_store();
	
	$scope.get_racks = function(page_no, per_page,no_of_item){
		$http.get(
		// '/api/administration/financialyears/',
		'/customapi/racks',
		{
			'responseType' : 'json',
			'params' : {
				'page_no':page_no,	
				'per_page' : per_page,
				'search_text':$scope.search_text,
      			'show_all': no_of_item
			}
		}
		).success(function(data){
			// this callback will be called asynchronously
       		// when the response is available
       		if(data.objects){
       			$scope.racks = data.objects;
       		}
       		$scope.cur_page=data.current_page_number;
		    $scope.next_page=data.next_page_number;
		    $scope.prev_page=data.previous_page_number;
		    $scope.last_page = data.last_page;
		    $scope.total = data.num_of_pages;
		    $scope.per_page = data.per_page;


		}).error(function(){
			// alert("failed");
		})
	}
	$scope.get_racks();
	$scope.nextPage = function () {
        $scope.get_racks($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
	        $scope.get_racks($scope.prev_page, $scope.per_page);
	};
	$scope.genPage = function () {
	    $scope.get_racks($scope.cur_page, $scope.per_page);
	};
	$scope.genRecords = function(){
	      $scope.get_racks($scope.cur_page, $scope.per_page);
	}
	$scope.search = function () {
	    $scope.get_racks($scope.cur_page, $scope.per_page);
	}

	$scope.save_rack = function(){
		$scope.rack_location_error = " ";
		$scope.store_id_error = " ";
		var is_valid = 1;
		if($scope.store_id==""){
			$scope.store_id_error = "* This field is required";
			is_valid = 0;
		}
		if($scope.rack_location == ""){
			$scope.rack_location_error = "* This field is required";
			is_valid = 0;
		}
		if(!$scope.edit_mode){
			if(is_valid==1){
				$scope.values_to_pass = {
					'store_id' : $scope.store_id,
					'rack_location' : $scope.rack_location,
					'remarks' : $scope.remarks,
					'data_add_mac' : location.host,
				}
				$http.post(
		            '/customapi/racks',

		            [$scope.values_to_pass],
		            {
		                'responseType': 'json'
		            }
		      	).success(function(data) {
		       		if(data.success){
		       			$scope.alert_success = "Rack added successfully";
		       			$scope.clear_form_fields();
		       			$scope.get_racks();
		       		}
		       		else{
		       			$scope.alert_failed = "Failed to add Rack";
		       			$scope.rack_location_error = data.form_errors.rack_location;
		       			$scope.get_racks();
		       		}
		       	}).error(function(){
		       		// alert("failed");
		       	})
		    }
		    else{
		    	$scope.alert_failed = "Failed to add Rack";
		    }
		}
		else{
			if(is_valid==1){
				$scope.values_to_pass = {
					'store_id' : $scope.store_id,
                    'rack_location' : $scope.rack_location,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
				}
				$http.post(
		            '/customapi/rack/'+$scope.rack_id,

		            [$scope.values_to_pass],
		            {
		                'responseType': 'json'
		            }
		      	).success(function(data) {
		       		if(data.success){
		       			$scope.alert_success = "Rack Edited successfully";
		       			$scope.store_code = "";
						$scope.store_name = "";
						$scope.remarks = "";
		       			$scope.edit_mode = false;
                        $scope.get_racks();
                        $scope.clear_form_fields();

		       		}
		       		else{
		       			$scope.alert_failed = "Failed to Edit Rack";
		       			$scope.rack_location_error = data.form_errors.rack_location;
		       		}
		       	}).error(function(){
		       		// alert("failed");
		       	})
		    }
		    else{
		    	$scope.alert_failed = "Failed to Edit Rack";
		    }
		}
	    $timeout( function(){ 
	    	$scope.alert_failed = ""; 
	    	$scope.alert_success = "";
	    }, 3000);
    }

    $scope.edit_rack = function(id){
    	$scope.clear_form_fields();
		$scope.rack_id = id;
  		$http.get(
		'/customapi/rack/'+id,
			{
				'responseType' : 'json'
		    }
		).success(function(data, status, headers, config) {
			$scope.edit_mode = true;
            $scope.view_mode = false;

			console.log(data.objects);
			response = data.objects;
			$scope.store_id = response[0].fields.store_id.pk;
			$scope.rack_location = response[0].fields.rack_location;
			$scope.remarks = response[0].fields.remarks;
			console.log($scope.storemodel);
			});		
  	}

    $scope.view_rack = function(id){
        $scope.clear_form_fields();
        $scope.rack_id = id;
        $http.get(
        '/customapi/rack/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.store_id = response[0].fields.store_id.pk;
            $scope.rack_location = response[0].fields.rack_location;
            $scope.remarks = response[0].fields.remarks;
            console.log($scope.storemodel);
            });     
    }

  	$scope.delete_rack = function(id){
	    $scope.loading = true;
	    bootbox.confirm("Are sure want to delete this rack?", function(result) {
	        if(result==true){
			    $http.post(
			    '/customapi/rack/'+id,
			    [],
			    {
			      'responseType' : 'json',
			      'headers': {'X-HTTP-Method-Override': 'DELETE'}
			    }).success(function(response){
			    	if (response.success) {
			        	$scope.alert_success = "Rack Deleted successfully";
			            $.each($scope.racks, function (index, rack) {
			            	if (rack.pk == id) {
			                  $scope.racks.splice(index,1);
			            	}
			        	});
			    	}	
			    	else{
			    		$scope.alert_failed = response.form_errors.integrity_error;
			    	}
		     	});
		     	$timeout( function(){ 
			    	$scope.alert_failed = ""; 
			    	$scope.alert_success = "";
			    }, 3000);
	        }
        });
  	}

    $scope.clear_form_fields = function(){
    	$scope.store_id = "";
    	$scope.rack_location = "";
    	$scope.remarks = "";
    	$scope.rack_location_error = " ";
		$scope.store_id_error = " ";
    }
});

// UnitMaster

kcaApp.controller('unitMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.unit_name = "";
    $scope.get_units = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/units',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.units = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_units();
    $scope.nextPage = function () {
        $scope.get_units($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_units($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_units($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_units($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_units($scope.cur_page, $scope.per_page);
    }

    $scope.save_unit = function(){
        $scope.unit_name_error = " ";
        var is_valid = 1;
        if($scope.unit_name==""){
            $scope.unit_name_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'unit_name' : $scope.unit_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/units',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Unit added successfully";
                        $scope.clear_form_fields();
                        $scope.get_units();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Unit";
                        $scope.unit_name_error = data.form_errors.unit_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Unit";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'unit_name' : $scope.unit_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/unit/'+$scope.unit_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Unit Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_units();
                        $scope.clear_form_fields();

                    }
                    else{
                        // alert("false");
                        $scope.alert_failed = "Failed to Edit Unit";
                        $scope.unit_name_error = data.form_errors.unit_name;
                        // $scope.get_financialyears();
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Unit";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_unit = function(id){
        $scope.clear_form_fields();
        $scope.unit_id = id;
        $http.get(
        '/customapi/unit/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.unit_name = response[0].fields.unit_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_unit = function(id){
        $scope.clear_form_fields();
        $scope.unit_id = id;
        $http.get(
        '/customapi/unit/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.unit_name = response[0].fields.unit_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_unit = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this unit?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/unit/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Unit Deleted successfully";
                        $.each($scope.units, function (index, unit) {
                            if (unit.pk == id) {
                              $scope.units.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }
    $scope.clear_form_fields = function(){
        $scope.unit_name = "";
        $scope.remarks = "";
        $scope.unit_name_error = " ";
    }
});

// BrandMaster
kcaApp.controller('brandMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.brand_name = "";
    $scope.brand_code = "";
    $scope.remarks = "";
    $scope.get_brands = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/brands',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.brands = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_brands();
    $scope.nextPage = function () {
        $scope.get_brands($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_brands($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_brands($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_brands($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_brands($scope.cur_page, $scope.per_page);
    }

    $scope.save_brand = function(){
        $scope.brand_code_error = " ";
        $scope.brand_name_error = " ";
        var is_valid = 1;
        if($scope.brand_code==""){
            $scope.brand_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.brand_name==""){
            $scope.brand_name_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'brand_code' : $scope.brand_code,
                    'brand_name' : $scope.brand_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/brands',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Brand added successfully";
                        $scope.clear_form_fields();
                        $scope.get_brands();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Brand";
                        $scope.brand_code_error = data.form_errors.brand_code;
                        $scope.brand_name_error = data.form_errors.brand_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Brand";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'brand_code' : $scope.brand_code,
                    'brand_name' : $scope.brand_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/brand/'+$scope.brand_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Brand Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_brands();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Brand";
                        $scope.brand_code_error = data.form_errors.brand_code;
                        $scope.brand_name_error = data.form_errors.brand_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Brand";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_brand = function(id){
        $scope.clear_form_fields();
        $scope.brand_id = id;
        $http.get(
        '/customapi/brand/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.brand_code = response[0].fields.brand_code;
            $scope.brand_name = response[0].fields.brand_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_brand = function(id){
        $scope.clear_form_fields();
        $scope.brand_id = id;
        $http.get(
        '/customapi/brand/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.brand_code = response[0].fields.brand_code;
            $scope.brand_name = response[0].fields.brand_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_brand = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this brand?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/brand/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Brand Deleted successfully";
                        $.each($scope.brands, function (index, brand) {
                            if (brand.pk == id) {
                              $scope.brands.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.brand_code = "";
        $scope.brand_name = "";
        $scope.remarks = "";
        $scope.brand_code_error = " ";
        $scope.brand_name_error = " ";
    }
});

// SizeMaster
kcaApp.controller('sizeMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.size_name = "";
    $scope.remarks = "";

    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.get_sizes = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/sizes',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.sizes = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_sizes();
    $scope.nextPage = function () {
        $scope.get_sizes($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_sizes($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_sizes($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_sizes($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_sizes($scope.cur_page, $scope.per_page);
    }

    $scope.save_size = function(){
        $scope.size_name_error = " ";
        var is_valid = 1;
        if($scope.size_name==""){
            $scope.size_name_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'size_name' : $scope.size_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/sizes',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Size added successfully";
                        $scope.clear_form_fields();
                        $scope.get_sizes();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Size";
                        $scope.size_name_error = data.form_errors.size_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Brand";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'size_name' : $scope.size_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/size/'+$scope.size_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Size Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_sizes();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Size";
                        $scope.size_name_error = data.form_errors.size_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Size";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_size = function(id){
        $scope.clear_form_fields();
        $scope.size_id = id;
        $http.get(
        '/customapi/size/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.size_name = response[0].fields.size_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.view_size = function(id){
        $scope.clear_form_fields();
        $scope.size_id = id;
        $http.get(
        '/customapi/size/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.size_name = response[0].fields.size_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.delete_size = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this size?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/size/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Size Deleted successfully";
                        $.each($scope.sizes, function (index, size) {
                            if (size.pk == id) {
                              $scope.sizes.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.size_name = "";
        $scope.remarks = "";
        $scope.size_name_error = " ";
    }
});

// PatternMaster
kcaApp.controller('patternMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.pattern_name = "";
    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.get_patterns = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/patterns',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.patterns = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_patterns();
    $scope.nextPage = function () {
        $scope.get_patterns($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_patterns($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_patterns($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_patterns($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_patterns($scope.cur_page, $scope.per_page);
    }

    $scope.save_pattern = function(){
        $scope.pattern_name_error = " ";
        var is_valid = 1;
        if($scope.pattern_name==""){
            $scope.pattern_name_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'pattern_name' : $scope.pattern_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/patterns',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Pattern added successfully";
                        $scope.clear_form_fields();
                        $scope.get_patterns();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Pattern";
                        $scope.pattern_name_error = data.form_errors.pattern_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Pattern";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'pattern_name' : $scope.pattern_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/pattern/'+$scope.pattern_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Pattern Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_patterns();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Pattern";
                        $scope.pattern_name_error = data.form_errors.pattern_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Pattern";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_pattern = function(id){
        $scope.clear_form_fields();
        $scope.pattern_id = id;
        $http.get(
        '/customapi/pattern/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.pattern_name = response[0].fields.pattern_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.view_pattern = function(id){
        $scope.clear_form_fields();
        $scope.pattern_id = id;
        $http.get(
        '/customapi/pattern/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.pattern_name = response[0].fields.pattern_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.delete_pattern = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this pattern?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/pattern/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Pattern Deleted successfully";
                        $.each($scope.patterns, function (index, pattern) {
                            if (pattern.pk == id) {
                              $scope.patterns.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.pattern_name = "";
        $scope.remarks = "";
        $scope.pattern_name_error = " ";
    }

});


// ItemGroup
kcaApp.controller('itemGroupListController', function ($scope, $http, $interval,$timeout) {
    $scope.itemgroup_code = "";
    $scope.itemgroup_name = "";
    $scope.remarks = "";
    $scope.corporate_account_id = "";
    $scope.search_text = "";

    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.get_parent_corporateaccounts = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/corporateaccounts',
        {
            'responseType' : 'json',
            'params' : {
                'show_all': 1,
                'parent_corporate_accounts' :1,
            }
        }
        ).success(function(data){
            if(data.objects){
                $scope.corporate_accounts = data.objects;
            }
        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_parent_corporateaccounts();
    
    $scope.get_itemgroups = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/itemgroups',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.itemgroups = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_itemgroups();
    $scope.nextPage = function () {
        $scope.get_itemgroups($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_itemgroups($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_itemgroups($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_itemgroups($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_itemgroups($scope.cur_page, $scope.per_page);
    }

    $scope.save_itemgroup = function(){
        $scope.itemgroup_code_error = " ";
        $scope.itemgroup_name_error = " ";
        $scope.corporate_account_id_error = " ";

        var is_valid = 1;
        if($scope.itemgroup_code==""){
            $scope.itemgroup_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.itemgroup_name == ""){
            $scope.itemgroup_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.corporate_account_id == ""){
            $scope.corporate_account_id_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'itemgroup_code' : $scope.itemgroup_code,
                    'itemgroup_name' : $scope.itemgroup_name,
                    'purchaseaccount_id' : $scope.corporate_account_id,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/itemgroups',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Itemgroup added successfully";
                        $scope.clear_form_fields();
                        $scope.get_itemgroups();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Itemgroup";
                        $scope.itemgroup_code_error = data.form_errors.itemgroup_code;
                        $scope.itemgroup_name_error = data.form_errors.itemgroup_name;
                        $scope.corporate_account_id_error = data.form_errors.corporate_account_id;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Itemgroup";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'itemgroup_code' : $scope.itemgroup_code,
                    'itemgroup_name' : $scope.itemgroup_name,
                    'purchaseaccount_id' : $scope.corporate_account_id,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/itemgroup/'+$scope.itemgroup_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Itemgroup Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_itemgroups();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Itemgroup";
                        $scope.itemgroup_code_error = data.form_errors.itemgroup_code;
                        $scope.itemgroup_name_error = data.form_errors.itemgroup_name;
                        $scope.corporate_account_id_error = data.form_errors.corporate_account_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Itemgroup";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_itemgroup = function(id){
        $scope.clear_form_fields();
        $scope.itemgroup_id = id;
        $http.get(
        '/customapi/itemgroup/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;

            console.log(data.objects);
            response = data.objects;
            $scope.itemgroup_code = response[0].fields.itemgroup_code;
            $scope.itemgroup_name = response[0].fields.itemgroup_name;
            $scope.corporate_account_id = response[0].fields.purchaseaccount_id.pk;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_itemgroup = function(id){
        $scope.clear_form_fields();
        $scope.rack_id = id;
        $http.get(
        '/customapi/itemgroup/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.itemgroup_code = response[0].fields.itemgroup_code;
            $scope.itemgroup_name = response[0].fields.itemgroup_name;
            $scope.corporate_account_id = response[0].fields.purchaseaccount_id.pk;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_itemgroup = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this itemgroup?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/itemgroup/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Itemgroup Deleted successfully";
                        $.each($scope.itemgroups, function (index, itemgroup) {
                            if (itemgroup.pk == id) {
                              $scope.itemgroups.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.itemgroup_delete_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.itemgroup_code = "";
        $scope.itemgroup_name = "";
        $scope.corporate_account_id = "";
        $scope.remarks = "";
        $scope.itemgroup_code_error = " ";
        $scope.itemgroup_name_error = " ";
        $scope.corporate_account_id_error = " ";

    }
});



// ItemSubGroup
kcaApp.controller('itemSubGroupListController', function ($scope, $http, $interval,$timeout) {
    $scope.item_subgroup_code = "";
    $scope.item_subgroup_name = "";
    $scope.remarks = "";
    $scope.itemgroup_id = "";
    $scope.search_text = "";

    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.get_itemgroups = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/itemgroups',
        {
            'responseType' : 'json',
            'params' : {
                'show_all': 1,
            }
        }
        ).success(function(data){
            if(data.objects){
                $scope.itemgroups = data.objects;
            }
        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_itemgroups();
    
    $scope.get_itemsubgroups = function(page_no, per_page,no_of_item){
        $http.get(
        // '/api/administration/financialyears/',
        '/customapi/itemsubgroups',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.itemsubgroups = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_itemsubgroups();
    $scope.nextPage = function () {
        $scope.get_itemsubgroups($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_itemsubgroups($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_itemsubgroups($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_itemsubgroups($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_itemsubgroups($scope.cur_page, $scope.per_page);
    }

    $scope.save_itemsubgroup = function(){
        $scope.item_subgroup_code_error = " ";
        $scope.item_subgroup_name_error = " ";
        $scope.itemgroup_id_error = " ";

        var is_valid = 1;
        if($scope.item_subgroup_code==""){
            $scope.item_subgroup_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.item_subgroup_name == ""){
            $scope.item_subgroup_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.itemgroup_id == ""){
            $scope.itemgroup_id_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'item_subgroup_code' : $scope.item_subgroup_code,
                    'item_subgroup_name' : $scope.item_subgroup_name,
                    'itemgroup_id' : $scope.itemgroup_id,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/itemsubgroups',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Itemsubgroup added successfully";
                        $scope.clear_form_fields();
                        $scope.get_itemsubgroups();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Itemsubgroup";
                        $scope.item_subgroup_code_error = data.form_errors.item_subgroup_code;
                        $scope.item_subgroup_name_error = data.form_errors.item_subgroup_name;
                        $scope.itemgroup_id_error = data.form_errors.itemgroup_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Itemsubgroup";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'item_subgroup_code' : $scope.item_subgroup_code,
                    'item_subgroup_name' : $scope.item_subgroup_name,
                    'itemgroup_id' : $scope.itemgroup_id,
                    'remarks' : $scope.remarks,
                    'data_modi_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/itemsubgroup/'+$scope.itemsubgroup_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Itemsubgroup Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_itemsubgroups();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Itemsubgroup";
                        $scope.item_subgroup_code_error = data.form_errors.item_subgroup_code;
                        $scope.item_subgroup_name_error = data.form_errors.item_subgroup_name;
                        $scope.itemgroup_id_error = data.form_errors.itemgroup_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Itemsubgroup";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_itemsubgroup = function(id){
        $scope.clear_form_fields();
        $scope.itemsubgroup_id = id;
        $http.get(
        '/customapi/itemsubgroup/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;

            console.log(data.objects);
            response = data.objects;
            $scope.item_subgroup_code = response[0].fields.item_subgroup_code;
            $scope.item_subgroup_name = response[0].fields.item_subgroup_name;
            $scope.itemgroup_id = response[0].fields.itemgroup_id.pk;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_itemsubgroup = function(id){
        $scope.clear_form_fields();
        $http.get(
        '/customapi/itemsubgroup/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.item_subgroup_code = response[0].fields.item_subgroup_code;
            $scope.item_subgroup_name = response[0].fields.item_subgroup_name;
            $scope.itemgroup_id = response[0].fields.itemgroup_id.pk;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_itemsubgroup = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this itemsubgroup?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/itemsubgroup/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Itemsubgroup Deleted successfully";
                        $.each($scope.itemsubgroups, function (index, itemsubgroup) {
                            if (itemsubgroup.pk == id) {
                              $scope.itemsubgroups.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.item_subgroup_code = "";
        $scope.item_subgroup_name = "";
        $scope.remarks = "";
        $scope.itemgroup_id = "";
        $scope.item_subgroup_code_error = " ";
        $scope.item_subgroup_name_error = " ";
        $scope.itemgroup_id_error = " ";

    }
});


// ItemMaster
kcaApp.controller('ItemMasterListController', function ($scope, $http, $interval,$timeout) {

    $scope.item_code = "";
    $scope.item_short_code = "";
    $scope.item_name = "";
    $scope.item_name = "";
    $scope.brand_id = "";
    $scope.size_id = "";
    $scope.unit_id = "";
    $scope.pattern_id = "";
    $scope.itemgroup_id = "";
    $scope.item_subgroup_id = "";
    $scope.reorder_level = "";
    $scope.is_active = "";
    $scope.purchaseaccount_id = "";
    $scope.remarks = "";
    $scope.item_image = "";

    $scope.edit_mode = false;
    $scope.view_mode = false; 
    $scope.fill_required_dropdowns = function(page_no, per_page,no_of_item){
        // 1.Brand
            $http.get(
            '/customapi/brands',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.brands = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 2.Size
            $http.get(
            '/customapi/sizes',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.sizes = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 3.Unit
            $http.get(
            '/customapi/units',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.units = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 4.Pattern
            $http.get(
            '/customapi/patterns',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.patterns = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 5.Group
            $http.get(
            '/customapi/itemgroups',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.itemgroups = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

    }
    $scope.fill_required_dropdowns();

    // To get item sub groups and corporate account related to selected Item group
    $scope.get_data_on_itemgroup_change = function(id){
        // 1.ItemSubGroups
        $http.get(
            '/customapi/itemsubgroups',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                    'itemsubgroups_of_selected_itemgroup' : 1,
                    'search_text':id,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.itemsubgroups = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

        // 2.CorporateAccount(purchaseaccount_id)
        $http.get(
            '/customapi/corporateaccounts',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                    'corporateaccounts_of_selected_itemgroup' : 1,
                    'search_text':id,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.corporateaccounts = data.objects;
                    $scope.purchaseaccount_id = $scope.corporateaccounts[0].pk
                }

            }).error(function(){
                // alert("failed");
            })
    }


    $scope.get_items = function(page_no, per_page,no_of_item){
        $http.get(
        // '/api/administration/financialyears/',
        '/customapi/items',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.items = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_items();
    $scope.nextPage = function () {
        $scope.get_items($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_items($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_items($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_items($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_items($scope.cur_page, $scope.per_page);
    }


    $scope.save_item = function(){
        $scope.item_code_error = " ";
        $scope.item_short_code_error = " ";
        $scope.item_name_error = " ";
        $scope.brand_id_error = " ";
        $scope.unit_id_error = " ";
        $scope.itemgroup_id_error = " ";

        var is_valid = 1;
        if($scope.item_code==""){
            $scope.item_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.item_short_code == ""){
            $scope.item_short_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.item_name == ""){
            $scope.item_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.brand_id == ""){
            $scope.brand_id_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.unit_id == ""){
            $scope.unit_id_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.itemgroup_id == ""){
            $scope.itemgroup_id_error = "* This field is required";
            is_valid = 0;
        }
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'item_code' : $scope.item_code,
                    'item_short_code' : $scope.item_short_code,
                    'item_name' : $scope.item_name,
                    'brand_id' : $scope.brand_id,
                    'size_id' : $scope.size_id,
                    'unit_id' : $scope.unit_id,
                    'pattern_id' : $scope.pattern_id,
                    'itemgroup_id' : $scope.itemgroup_id,
                    'item_subgroup_id' : $scope.item_subgroup_id,
                    'reorder_level' : $scope.reorder_level,
                    'is_active' : $scope.is_active,
                    'purchaseaccount_id' : $scope.purchaseaccount_id,
                    'remarks' : $scope.remarks,
                    'item_image' : $scope.item_image,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/items',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Item added successfully";
                        // $scope.clear_form_fields();
                        $scope.get_items();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Item";
                        $scope.item_code_error = data.form_errors.item_code;
                        $scope.item_short_code_error = data.form_errors.item_short_code;
                        $scope.item_name_error = data.form_errors.item_name;
                        $scope.brand_id_error = data.form_errors.brand_id;
                        $scope.unit_id_error = data.form_errors.unit_id;
                        $scope.itemgroup_id_error = data.form_errors.itemgroup_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Item";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'item_code' : $scope.item_code,
                    'item_short_code' : $scope.item_short_code,
                    'item_name' : $scope.item_name,
                    'brand_id' : $scope.brand_id,
                    'size_id' : $scope.size_id,
                    'unit_id' : $scope.unit_id,
                    'pattern_id' : $scope.pattern_id,
                    'itemgroup_id' : $scope.itemgroup_id,
                    'item_subgroup_id' : $scope.item_subgroup_id,
                    'reorder_level' : $scope.reorder_level,
                    'is_active' : $scope.is_active,
                    'purchaseaccount_id' : $scope.purchaseaccount_id,
                    'remarks' : $scope.remarks,
                    'item_image' : $scope.item_image,
                    'data_modi_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/item/'+$scope.item_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Item Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_items();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Item";
                        $scope.item_code_error = data.form_errors.item_code;
                        $scope.item_short_code_error = data.form_errors.item_short_code;
                        $scope.item_name_error = data.form_errors.item_name;
                        $scope.brand_id_error = data.form_errors.brand_id;
                        $scope.unit_id_error = data.form_errors.unit_id;
                        $scope.itemgroup_id_error = data.form_errors.itemgroup_id;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Item";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_item = function(id){
        $scope.clear_form_fields();
        $scope.item_id = id;
        $http.get(
        '/customapi/item/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;

            console.log(data.objects);
            response = data.objects;
            $scope.item_code = response[0].fields.item_code;
            $scope.item_short_code = response[0].fields.item_short_code;
            $scope.item_name = response[0].fields.item_name;
            $scope.brand_id = response[0].fields.brand_id.pk;
            $scope.unit_id = response[0].fields.unit_id.pk;
            $scope.itemgroup_id = response[0].fields.itemgroup_id.pk;
            $scope.purchaseaccount_id = response[0].fields.purchaseaccount_id.pk;
            if(response[0].fields.size_id)
                $scope.size_id = response[0].fields.size_id.pk;
            if(response[0].fields.pattern_id)
                $scope.pattern_id = response[0].fields.pattern_id.pk;
            if(response[0].fields.item_subgroup_id)
                $scope.item_subgroup_id = response[0].fields.item_subgroup_id.pk;
            $scope.reorder_level = response[0].fields.reorder_level;
            $scope.is_active = response[0].fields.is_active;
            $scope.item_image = response[0].fields.item_image;
            $scope.remarks = response[0].fields.remarks;
            $scope.get_data_on_itemgroup_change($scope.itemgroup_id); // To initialize subgroup,purchaseaccount scope variables
            });     
    }

    $scope.view_item = function(id){
        $scope.clear_form_fields();
        $scope.item_id = id;
        $http.get(
        '/customapi/item/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.item_code = response[0].fields.item_code;
            $scope.item_short_code = response[0].fields.item_short_code;
            $scope.item_name = response[0].fields.item_name;
            $scope.brand_id = response[0].fields.brand_id.pk;
            $scope.unit_id = response[0].fields.unit_id.pk;
            $scope.itemgroup_id = response[0].fields.itemgroup_id.pk;
            $scope.purchaseaccount_id = response[0].fields.purchaseaccount_id.pk;
            if(response[0].fields.size_id)
                $scope.size_id = response[0].fields.size_id.pk;
            if(response[0].fields.pattern_id)
                $scope.pattern_id = response[0].fields.pattern_id.pk;
            if(response[0].fields.item_subgroup_id){
                $scope.item_subgroup_id = response[0].fields.item_subgroup_id.pk;
                $scope.get_data_on_itemgroup_change($scope.itemgroup_id); // To initialize subgroup,purchaseaccount scope variables
            }
            $scope.reorder_level = response[0].fields.reorder_level;
            $scope.is_active = response[0].fields.is_active;
            $scope.item_image = response[0].fields.item_image;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_item = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this item?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/item/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Item Deleted successfully";
                        $.each($scope.items, function (index, item) {
                            if (item.pk == id) {
                              $scope.items.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.item_code = "";
        $scope.item_short_code = "";
        $scope.item_name = "";
        $scope.item_name = "";
        $scope.brand_id = "";
        $scope.size_id = "";
        $scope.unit_id = "";
        $scope.pattern_id = "";
        $scope.itemgroup_id = "";
        $scope.item_subgroup_id = "";
        $scope.reorder_level = "";
        $scope.is_active = "";
        $scope.purchaseaccount_id = "";
        $scope.remarks = "";
        $scope.item_image = "";

        $scope.item_code_error = " ";
        $scope.item_short_code_error = " ";
        $scope.item_name_error = " ";
        $scope.brand_id_error = " ";
        $scope.unit_id_error = " ";
        $scope.itemgroup_id_error = " ";

    }

    });

// SupplierMaster
kcaApp.controller('SupplierMasterListController', function ($scope, $http, $interval,$timeout) {

    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.supplier_code = "";
    $scope.supplier_name = "";
    $scope.contact_person = "";
    $scope.contact_phone = "";
    $scope.contact_mobile = "";
    $scope.contact_email = "";
    $scope.supplier_address = "";
    $scope.country_id = "";
    $scope.state_id = "";
    $scope.pincode = "";
    $scope.office_phone = "";
    $scope.office_mobile = "";
    $scope.office_email_id = "";
    $scope.link_account_id = "";
    $scope.is_active = "";
    $scope.is_billwisesettle = "";
    $scope.credit_limit = 0;
    $scope.is_blacklisted = "";
    $scope.supplier_bank = "";
    $scope.supplier_branch = "";
    $scope.supplier_account_no = "";
    $scope.supplier_ifsc = "";
    $scope.supplier_kgst_no = "";
    $scope.supplier_cst_no = "";
    $scope.supplier_pan = "";


    $scope.fill_required_dropdowns = function(page_no, per_page,no_of_item){
        // 1.CountryMaster
            $http.get(
            '/customapi/countries',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.countries = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

    };
    $scope.fill_required_dropdowns();

    $scope.get_data_on_country_change = function(id){
        // 1.StateMaster
        $http.get(
            '/customapi/states',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                    'states_of_selected_country' : 1,
                    'search_text':id,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.states = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })
    }

    $scope.get_suppliers = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/suppliers',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.suppliers = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_suppliers();
    $scope.nextPage = function () {
        $scope.get_suppliers($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_suppliers($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_suppliers($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_suppliers($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_suppliers($scope.cur_page, $scope.per_page);
    }

    $scope.save_supplier = function(){
        $scope.supplier_code_error = " ";
        $scope.supplier_name_error = " ";

        var is_valid = 1;
        if($scope.supplier_code==""){
            $scope.supplier_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.supplier_name == ""){
            $scope.supplier_name_error = "* This field is required";
            is_valid = 0;
        }
        
        if(!$scope.edit_mode){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'supplier_code' : $scope.supplier_code,
                    'supplier_name' : $scope.supplier_name,
                    'contact_person' : $scope.contact_person,
                    'contact_phone' : $scope.contact_phone,
                    'contact_mobile' : $scope.contact_mobile,
                    'contact_email' : $scope.contact_email,
                    'supplier_address' : $scope.supplier_address,
                    'country_id' : $scope.country_id,
                    'state_id' : $scope.state_id,
                    'pincode' : $scope.pincode,
                    'office_phone' : $scope.office_phone,
                    'office_mobile' : $scope.office_mobile,
                    'office_email_id' : $scope.office_email_id,
                    'is_active' : $scope.is_active,
                    'is_billwisesettle' : $scope.is_billwisesettle,
                    'credit_limit' : $scope.credit_limit,
                    'is_blacklisted' : $scope.is_blacklisted,
                    'supplier_bank' : $scope.supplier_bank,
                    'supplier_branch' : $scope.supplier_branch,
                    'supplier_account_no' : $scope.supplier_account_no,
                    'supplier_ifsc' : $scope.supplier_ifsc,
                    'supplier_kgst_no' : $scope.supplier_kgst_no,
                    'supplier_cst_no' : $scope.supplier_cst_no,
                    'supplier_pan' : $scope.supplier_pan,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/suppliers',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Supplier added successfully";
                        $scope.clear_form_fields();
                        $scope.get_suppliers();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Supplier";
                        $scope.supplier_code_error = data.form_errors.supplier_code;
                        $scope.supplier_name_error = data.form_errors.supplier_name;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Item";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'supplier_code' : $scope.supplier_code,
                    'supplier_name' : $scope.supplier_name,
                    'contact_person' : $scope.contact_person,
                    'contact_phone' : $scope.contact_phone,
                    'contact_mobile' : $scope.contact_mobile,
                    'contact_email' : $scope.contact_email,
                    'supplier_address' : $scope.supplier_address,
                    'country_id' : $scope.country_id,
                    'state_id' : $scope.state_id,
                    'pincode' : $scope.pincode,
                    'office_phone' : $scope.office_phone,
                    'office_mobile' : $scope.office_mobile,
                    'office_email_id' : $scope.office_email_id,
                    'is_active' : $scope.is_active,
                    'is_billwisesettle' : $scope.is_billwisesettle,
                    'credit_limit' : $scope.credit_limit,
                    'is_blacklisted' : $scope.is_blacklisted,
                    'supplier_bank' : $scope.supplier_bank,
                    'supplier_branch' : $scope.supplier_branch,
                    'supplier_account_no' : $scope.supplier_account_no,
                    'supplier_ifsc' : $scope.supplier_ifsc,
                    'supplier_kgst_no' : $scope.supplier_kgst_no,
                    'supplier_cst_no' : $scope.supplier_cst_no,
                    'supplier_pan' : $scope.supplier_pan,
                    'data_modi_mac' : location.host,
                }
                $http.post(
                    '/customapi/supplier/'+$scope.supplier_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Supplier Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_suppliers();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Supplier";
                        $scope.supplier_code_error = data.form_errors.supplier_code;
                        $scope.supplier_name_error = data.form_errors.supplier_name;

                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Supplier";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_supplier = function(id){
        $scope.clear_form_fields();
        $scope.supplier_id = id;
        $http.get(
        '/customapi/supplier/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;

            console.log(data.objects);
            response = data.objects;
            $scope.supplier_code = response[0].fields.supplier_code;
            $scope.supplier_name = response[0].fields.supplier_name;
            $scope.contact_person = response[0].fields.contact_person;
            $scope.contact_phone = response[0].fields.contact_phone;
            $scope.contact_mobile = response[0].fields.contact_mobile;
            $scope.contact_email = response[0].fields.contact_email;
            $scope.supplier_address = response[0].fields.supplier_address;
            if(response[0].fields.country_id)
                $scope.country_id = response[0].fields.country_id.pk;
            if(response[0].fields.state_id){
                $scope.state_id = response[0].fields.state_id.pk;
                $scope.get_data_on_country_change($scope.country_id); // To initialize subgroup,purchaseaccount scope variables
            }
            $scope.pincode = response[0].fields.pincode;
            $scope.office_phone = response[0].fields.office_phone;
            $scope.office_mobile = response[0].fields.office_mobile;
            $scope.office_email_id = response[0].fields.office_email_id;
            $scope.is_active = response[0].fields.is_active;
            $scope.is_billwisesettle = response[0].fields.is_billwisesettle;
            $scope.credit_limit = response[0].fields.credit_limit;
            $scope.is_blacklisted = response[0].fields.is_blacklisted;
            $scope.supplier_bank = response[0].fields.supplier_bank;
            $scope.supplier_branch = response[0].fields.supplier_branch;
            $scope.supplier_account_no = response[0].fields.supplier_account_no;
            $scope.supplier_ifsc = response[0].fields.supplier_ifsc;
            $scope.supplier_kgst_no = response[0].fields.supplier_kgst_no;
            $scope.supplier_cst_no = response[0].fields.supplier_cst_no;
            $scope.supplier_pan = response[0].fields.supplier_pan;

            });     
    }

    $scope.view_supplier = function(id){
        $scope.clear_form_fields();
        $scope.supplier_id = id;
        $http.get(
        '/customapi/supplier/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;

            console.log(data.objects);
            response = data.objects;
            $scope.supplier_code = response[0].fields.supplier_code;
            $scope.supplier_name = response[0].fields.supplier_name;
            $scope.contact_person = response[0].fields.contact_person;
            $scope.contact_phone = response[0].fields.contact_phone;
            $scope.contact_mobile = response[0].fields.contact_mobile;
            $scope.contact_email = response[0].fields.contact_email;
            $scope.supplier_address = response[0].fields.supplier_address;
            if(response[0].fields.country_id)
                $scope.country_id = response[0].fields.country_id.pk;
            if(response[0].fields.state_id){
                $scope.state_id = response[0].fields.state_id.pk;
                $scope.get_data_on_country_change($scope.country_id); // To initialize subgroup,purchaseaccount scope variables
            }
            $scope.pincode = response[0].fields.pincode;
            $scope.office_phone = response[0].fields.office_phone;
            $scope.office_mobile = response[0].fields.office_mobile;
            $scope.office_email_id = response[0].fields.office_email_id;
            $scope.is_active = response[0].fields.is_active;
            $scope.is_billwisesettle = response[0].fields.is_billwisesettle;
            $scope.credit_limit = response[0].fields.credit_limit;
            $scope.is_blacklisted = response[0].fields.is_blacklisted;
            $scope.supplier_bank = response[0].fields.supplier_bank;
            $scope.supplier_branch = response[0].fields.supplier_branch;
            $scope.supplier_account_no = response[0].fields.supplier_account_no;
            $scope.supplier_ifsc = response[0].fields.supplier_ifsc;
            $scope.supplier_kgst_no = response[0].fields.supplier_kgst_no;
            $scope.supplier_cst_no = response[0].fields.supplier_cst_no;
            $scope.supplier_pan = response[0].fields.supplier_pan;

            });     
    }


    $scope.delete_supplier = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this supplier?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/supplier/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Supplier Deleted successfully";
                        $.each($scope.suppliers, function (index, supplier) {
                            if (supplier.pk == id) {
                              $scope.suppliers.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

     $scope.clear_form_fields = function(){
        $scope.supplier_code = "";
        $scope.supplier_name = "";
        $scope.contact_person = "";
        $scope.contact_phone = "";
        $scope.contact_mobile = "";
        $scope.contact_email = "";
        $scope.supplier_address = "";
        $scope.country_id = "";
        $scope.state_id = "";
        $scope.pincode = "";
        $scope.office_phone = "";
        $scope.office_mobile = "";
        $scope.office_email_id = "";
        $scope.link_account_id = "";
        $scope.is_active = "";
        $scope.is_billwisesettle = "";
        $scope.credit_limit = "";
        $scope.is_blacklisted = "";
        $scope.supplier_bank = "";
        $scope.supplier_branch = "";
        $scope.supplier_account_no = "";
        $scope.supplier_ifsc = "";
        $scope.supplier_kgst_no = "";
        $scope.supplier_cst_no = "";
        $scope.supplier_pan = "";

        $scope.supplier_code_error = " ";
        $scope.supplier_name_error = " ";

    }

});


// Category
kcaApp.controller('categoryListController', function ($scope, $http, $interval,$timeout) {
    $scope.categories = new Array()
    $scope.search_text = '';
    $scope.count = 0;

    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.category_code = "";
    $scope.category_name = "";
    $scope.remarks = "";
    $scope.alert_success = "";
    $scope.alert_failed = "";
    $scope.category_code_error = "";
    $scope.category_name_error = "";
    $scope.get_categories = function(page_no, per_page,no_of_item){
        $http.get(
        // '/api/administration/financialyears/',
        '/customapi/categories',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.categories = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_categories();
    $scope.nextPage = function () {
        $scope.get_categories($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_categories($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_categories($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_categories($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_categories($scope.cur_page, $scope.per_page);
    }


    $scope.save_category = function() {

        var is_valid = 1;
        if ($scope.category_code == "") {
            $scope.category_code_error = "* This field is required";
            is_valid = 0;
        }
        if ($scope.category_name == "") {
            $scope.category_name_error = "* This field is required";
            is_valid = 0;
        }
        if (!$scope.edit_mode) {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'category_code': $scope.category_code,
                    'category_name': $scope.category_name,
                    'remarks': $scope.remarks,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/categories',

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Category added successfully";
                        $scope.clear_form_fields();
                        $scope.get_categories();
                    } else {
                        $scope.alert_failed = "Failed to add Category";
                        $scope.category_code_error = data.form_errors.category_code;
                        $scope.category_name_error = data.form_errors.category_name;
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Category";
            }
        } else {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'category_code': $scope.category_code,
                    'category_name': $scope.category_name,
                    'remarks': $scope.remarks,
                    'data_modi_mac': location.host,
                }
                $http.post(
                    '/customapi/category/' + $scope.category_id,

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Category Edited successfully";
                        $scope.get_categories();
                        $scope.edit_mode = false;
                        $scope.clear_form_fields();

                    } else {
                        $scope.alert_failed = "Failed to Edit Category";
                        $scope.category_code_error = data.form_errors.category_code;
                        $scope.category_name_error = data.form_errors.category_name;
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Category";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_category = function(id){
        $scope.clear_form_fields();
        $scope.category_id = id;
        $http.get(
        '/customapi/category/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.category_code = response[0].fields.category_code;
            $scope.category_name = response[0].fields.category_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.view_category = function(id){
        $scope.clear_form_fields();
        $scope.category_id = id;
        $http.get(
        '/customapi/category/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            console.log(data.objects);
            response = data.objects;
            $scope.category_code = response[0].fields.category_code;
            $scope.category_name = response[0].fields.category_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_category = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this category?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/category/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Category Deleted successfully";
                        $.each($scope.categories, function (index, category) {
                            if (category.pk == id) {
                              $scope.categories.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = response.form_errors.integrity_error;
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.category_code = "";
        $scope.category_name = "";
        $scope.remarks = "";
        $scope.category_code_error = "";
        $scope.category_name_error = "";
    }
});


// Item-Rack Mapping
kcaApp.controller('item_rack_mappingController', function ($scope, $http, $interval,$timeout) {

    $scope.rack_id = "";
    $scope.store_id = "";
    $scope.item_id = "";

    $scope.itemgroup_id = "";
    $scope.item_subgroup_id = "";
    $scope.search_text="";

    // to get all stores of current selected branch
    $scope.get_currentbranch_store = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/stores',
        {
            'responseType' : 'json',
            'params' : {
                'show_all': 1,
                'currentbranch_store' : 1,
            }
        }
        ).success(function(data){
            if(data.objects){
                $scope.stores = data.objects;
            }
            // $scope.store_id = $scope.stores[0].pk
        })
    }
    $scope.get_currentbranch_store();

    // to get all Itemgroups
    $scope.get_itemgroups = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/itemgroups',
        {
            'responseType' : 'json',
            'params' : {
                'show_all': 1,
            }
        }
        ).success(function(data){
            if(data.objects){
                $scope.itemgroups = data.objects;
            }

        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_itemgroups();
    
    // to get all item-rack mappings 
    $scope.get_itemrack_mappings = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/itemrackmappings',
        {
            'responseType' : 'json',
            'params' : {
                // 'page_no':page_no,  
                // 'per_page' : per_page,
                // 'search_text':$scope.search_text,
                'show_all': 1
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.item_rack_mappings = data.objects;
                console.log(data.objects);
            }
        })
    }

    $scope.get_itemrack_mappings();

    // to get all items
    $scope.get_items = function(page_no, per_page,no_of_item){
        var exists=false;
        $scope.item_details = [];
        $http.get(
        '/customapi/items',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'itemgroup_id':$scope.itemgroup_id,
                'item_subgroup_id':$scope.item_subgroup_id,
                'item_description':1,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.items = data.objects;
                console.log(data.objects);
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


            // customizes objects list
            $.each($scope.items, function(index, item) {
                $.each($scope.item_rack_mappings, function(index, item_rack_mapping) {
                    if (item_rack_mapping.fields.item_id.pk == item.pk && item_rack_mapping.fields.store_id.pk==$scope.store_id) {
                        $scope.item_details.push({
                            pk: item.pk,
                            item_code: item.fields.item_code,
                            item_short_code:  item.fields.item_short_code ,
                            item_description: item.fields.item_description,
                            rack_id:item_rack_mapping.fields.rack_id.pk,
                        });
                        exists=true;
                    }
                });
                if(!exists){
                    $scope.item_details.push({
                        pk: item.pk,
                        item_code: item.fields.item_code,
                        item_short_code:  item.fields.item_short_code ,
                        item_description: item.fields.item_description,
                        rack_id:'',
                    });
                }
                exists =false;
            });
            // end

        }).error(function(){
            // alert("failed");
        })
    }
    // $scope.get_items();
    $scope.nextPage = function () {
        $scope.get_items($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_items($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_items($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_items($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_items($scope.cur_page, $scope.per_page);
    }

    // to get all racks of selected store 
    $scope.get_selectedstore_racks = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/racks',
        {
            'responseType' : 'json',
            'params' : {
                // 'page_no':page_no,  
                // 'per_page' : per_page,
                'selected_store_racks' : 1,
                'pk':$scope.store_id,
                'show_all': 1
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.racks = data.objects;
            }

        })
    }

    // To get item sub groups selected Item group
    $scope.get_subgroups_on_itemgroup_change = function(id){
        // 1.ItemSubGroups
        $scope.item_subgroup_id = "";
        $http.get(
            '/customapi/itemsubgroups',
            {
                'responseType' : 'json',
                'params' : {
                    'show_all': 1,
                    'itemsubgroups_of_selected_itemgroup' : 1,
                    'search_text':id,
                }
            }
            ).success(function(data){
                if(data.objects){
                    $scope.itemsubgroups = data.objects;
                }

            }).error(function(){
                // alert("failed");
            })

    }

    //to save item-rack mapping 
    $scope.save_mapping = function(row,rack_id){
        console.log(row);
        console.log(rack_id);
        $scope.values_to_pass = {
            'store_id': $scope.store_id,
            'rack_id': rack_id,
            'item_id': row.pk,
        }
        $http.post(
            '/customapi/itemrackmappings',

            [$scope.values_to_pass], {
                'responseType': 'json'
            }
        ).success(function(data) {
            if (data.success) {
                $scope.alert_success = "Mapping Added successfully";
            } else {
                $scope.alert_failed = "Failed to add Mapping";
            }
        }).error(function() {
            // alert("failed");
        })
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);

    }


    
});