from django import http
import re
import logging
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from kca.common.permission_check import Manager
from kca.common.utils import Menu
from django.core.urlresolvers import resolve


class HandleRequests(object):

    def process_request(self, request, **kwargs):
        request.logged_in_user = None
        if not re.match(SAFE_TO_REDIRECT_URI_REGEX, request.path):
            if not request.user.is_authenticated():
                logging.info(
                    'User not logged in. Going to redirect to Login page.')
                logging.info('Clearing cache.')
                # Manager.clear_all_cache()
                return redirect('%s?next=%s' % (reverse('login_page'), request.path))
            elif request.user.is_authenticated() and not request.user.is_active:
                logging.info(
                    'User account suspended. Going to redirect to Login page.')
                return redirect('%s?next=%s' % (reverse('login_page'), request.path))
            # elif request.session.get('is_locked', False) and not re.match(reverse('lock_screen_view'), request.path):
            #     logging.info('User account is locked. Going to redirect to lock screen.')
            # return redirect('%s?next=%s' % (reverse('lock_screen_view'),
            # request.path))

            logged_in_user = Manager.get_logged_in_user(
                request.user)
            # employee_require_redirection = Manager.get_employee_permanent_redirection(logged_in_employee)
            # if employee_require_redirection and not re.match(employee_require_redirection, request.path):
            #     return redirect(employee_require_redirection)
            # set lie and privileges in request object to be accessible in all
            # views.

            request.logged_in_user = logged_in_user
            # request.logged_in_employee_layout = Manager.get_employee_html_layout(logged_in_employee)
            # if request.method == 'GET' and 'menu_id' in request.GET:
            #     request.privileges = Manager.get_privileges(logged_in_employee,menu_id)
            #     print "privileges",request.privileges

            # manages menu privileges
            current_url = resolve(request.path_info)
            menu_id = Menu.get(current_url.url_name)
            if menu_id:
                request.session['menu_id'] = menu_id
                module_id = request.session.get('default_module_id')
                request.privileges = Manager.get_permission(
                    request.logged_in_user, module_id, menu_id)
                print request.privileges.__dict__

        return None


SAFE_TO_REDIRECT_URI_REGEX = '(/accounts/login)|(/logout)|(/api/)'


class HttpPostTunnelingMiddleware(object):

    def process_request(self, request):
        if 'HTTP_X_HTTP_METHOD_OVERRIDE' in request.META:
            http_method = request.META['HTTP_X_HTTP_METHOD_OVERRIDE']
            if http_method.lower() == 'delete':
                request.method = 'DELETE'
                request.META['REQUEST_METHOD'] = 'DELETE'
                request.DELETE = http.QueryDict(request.body)
            elif http_method.lower() == 'put':
                request.method = 'PUT'
                request.META['REQUEST_METHOD'] = 'PUT'
                request.PUT = http.QueryDict(request.body)
        return None
