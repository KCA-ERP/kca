# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0005_auto_20150324_0921'),
        ('store', '0007_itemgroup'),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemSubGroup',
            fields=[
                ('item_subgroup_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nItemSubGroupID')),
                ('item_subgroup_code', models.CharField(max_length=10, db_column=b'cItemSubGroupCode')),
                ('item_subgroup_name', models.CharField(max_length=50, db_column=b'cItemSubGroupName')),
                ('remarks', models.CharField(max_length=300, null=True, db_column=b'cRemarks')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='item_subgroup_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='item_subgroup_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
                ('itemgroup_id', models.ForeignKey(to='store.ItemGroup', db_column=b'nItemGroupID')),
            ],
            options={
                'db_table': 'tstitemsubgroup',
            },
            bases=(models.Model,),
        ),
    ]
