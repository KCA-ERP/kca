# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0015_itemrackmapping'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='itemrackmapping',
            name='data_add_by',
        ),
        migrations.RemoveField(
            model_name='itemrackmapping',
            name='data_add_mac',
        ),
        migrations.RemoveField(
            model_name='itemrackmapping',
            name='data_add_on',
        ),
        migrations.RemoveField(
            model_name='itemrackmapping',
            name='data_modi_by',
        ),
        migrations.RemoveField(
            model_name='itemrackmapping',
            name='data_modi_mac',
        ),
        migrations.RemoveField(
            model_name='itemrackmapping',
            name='data_modi_on',
        ),
    ]
