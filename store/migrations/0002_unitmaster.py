# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0001_initial'),
        ('store', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UnitMaster',
            fields=[
                ('unit_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nUnitID')),
                ('unit_name', models.CharField(max_length=50, db_column=b'cUnitName')),
                ('remarks', models.CharField(max_length=300, db_column=b'cRemarks', blank=True)),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='unit_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='unit_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
            ],
            options={
                'db_table': 'tstunitmaster',
            },
            bases=(models.Model,),
        ),
    ]
