from kca.common.api import ModuleAPI
from django.db.models import Q
from models import *
from kca.common.helpers import paginator
from kca.common.constants import STARTING_PAGE, DEFAUT_PER_PAGE
from django.core import serializers
import datetime
from kca.common.permission_check import Manager
from kca.mailer.mailer import SendNotification
from django.core.mail import send_mail
import json
from django.core import serializers
from django.forms.models import model_to_dict
from django.db import IntegrityError, transaction
from administration.models import BranchMaster, CompanyMaster


class StoreMasterAPI(ModuleAPI):
    model = StoreMaster

    @transaction.atomic
    def save_store(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        branch_id = self.httpRequest.session.get('default_branch_id')
        form_errors = {}
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                branch_obj = BranchMaster.objects.get(pk=branch_id)
                store_code_obj = self.model.objects.filter(
                    store_code=dict_to_save['store_code'], branch_id=branch_obj)
                store_name_obj = self.model.objects.filter(
                    store_name=dict_to_save['store_name'], branch_id=branch_obj)
                if store_code_obj:
                    form_errors = {
                        'store_code': '* This code already exists for the branch',
                    }
                    error = 1
                if store_name_obj:
                    form_errors.update(
                        {'store_name': '* This name already exists for the branch'})
                    error = 1
                if error == 1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSaveStoreMaster = {
                        'company_id': company_obj,
                        'branch_id': branch_obj,
                        'store_code': dict_to_save['store_code'],
                        'store_name': dict_to_save['store_name'],
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = StoreMaster(**dictToSaveStoreMaster)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_store(self, store_id):
        store_obj = self.model.objects.filter(pk=store_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                store_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_store(self, store_id, dict_to_save):
        error = 0
        form_errors = {}
        branch_id = self.httpRequest.session.get('default_branch_id')
        branch_obj = BranchMaster.objects.get(pk=branch_id)
        store_code_obj = self.model.objects.filter(
            store_code=dict_to_save['store_code'], branch_id=branch_obj).exclude(pk=store_id)
        store_name_obj = self.model.objects.filter(
            store_name=dict_to_save['store_name'], branch_id=branch_obj).exclude(pk=store_id)
        if store_code_obj:
            form_errors = {
                'store_code': '* This code already exists for the branch',
            }
            error = 1
        if store_name_obj:
            form_errors.update(
                {'store_name': '* This name already exists for the branch'})
            error = 1
        if error == 1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
            store_obj = self.model.objects.get(pk=store_id)
            dictToSaveStoreMaster = {
                'company_id': store_obj.company_id,
                'branch_id': store_obj.branch_id,
                'store_code': dict_to_save['store_code'],
                'store_name': dict_to_save['store_name'],
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_add_mac'],
                'data_modi_by': logged_in_user,
            }

            StoreMaster.objects.filter(pk=store_id).update(
                **dictToSaveStoreMaster)
            return self.respond(success=True)

    @transaction.atomic
    def delete_one(self, store_id):  #Overridden method to prevent foreign key violation
        try:
            with transaction.atomic():
                storemaster_obj = StoreMaster.objects.get(pk=store_id)
                storemaster_obj.delete()
                return self.respond(success=True)
        except IntegrityError:
            form_errors = {
                'store_delete_error': 'Store deletion is not possible due to usage in other forms',
            }
            return self.respond(success=False, form_errors=form_errors)
            # handle_exception()

    def prepare_search(self, stores, **search_kwargs):
        if search_kwargs.get('currentbranch_store'):
            branch_id = self.httpRequest.session.get('default_branch_id')
            branch_obj = BranchMaster.objects.get(pk=branch_id)
            stores = stores.filter(
                Q(branch_id__branch_name__icontains=branch_obj.branch_name)
            )
            return stores.order_by('-data_modi_on')

        if search_kwargs.get('search_text'):
            stores = stores.filter(
                Q(store_code__icontains=search_kwargs.get('search_text')) |
                Q(store_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text'))
            )
        return stores.order_by('-data_modi_on')


class RackMasterAPI(ModuleAPI):
    model = RackMaster
    relations_to_serialize = {
        'store_id': '',
    }
    extras = ('')

    @transaction.atomic
    def save_rack(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        branch_id = self.httpRequest.session.get('default_branch_id')
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                branch_obj = BranchMaster.objects.get(pk=branch_id)
                store_obj = StoreMaster.objects.get(
                    pk=dict_to_save['store_id'])
                rack_location_obj = self.model.objects.filter(
                    rack_location=dict_to_save['rack_location'], store_id=store_obj)
                if rack_location_obj:
                    form_errors = {
                        'rack_location': '* This rack number already exists for the store',
                    }
                    error = 1
                if error == 1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSaveRackMaster = {
                        'company_id': company_obj,
                        'store_id': store_obj,
                        'rack_location': dict_to_save['rack_location'],
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = RackMaster(**dictToSaveRackMaster)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_rack(self, rack_id):
        rack_obj = self.model.objects.filter(pk=rack_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                rack_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_rack(self, rack_id, dict_to_save):
        error = 0
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        store_obj = StoreMaster.objects.get(pk=dict_to_save['store_id'])
        rack_location_obj = self.model.objects.filter(
            rack_location=dict_to_save['rack_location'], store_id=store_obj).exclude(pk=rack_id)
        if rack_location_obj:
            form_errors = {
                'rack_location': '* This rack number already exists for the store',
            }
            error = 1
        if error == 1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            dictToSaveRackMaster = {
                'store_id': store_obj,
                'rack_location': dict_to_save['rack_location'],
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_add_mac'],
                'data_modi_by': logged_in_user,
            }

            RackMaster.objects.filter(pk=rack_id).update(
                **dictToSaveRackMaster)
            return self.respond(success=True)

    def prepare_search(self, racks, **search_kwargs):

        if search_kwargs.get('selected_store_racks'):
            racks = racks.filter(
                Q(store_id__pk__icontains=search_kwargs.get('pk'))
            )
            return racks.order_by('-data_modi_on')
        if search_kwargs.get('search_text'):
            racks = racks.filter(
                Q(rack_location__icontains=search_kwargs.get('search_text')) |
                Q(store_id__store_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text'))
            )
        return racks.order_by('-data_modi_on')


class UnitMasterAPI(ModuleAPI):
    model = UnitMaster

    @transaction.atomic
    def save_unit(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        form_errors = {}
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                unit_name_obj = self.model.objects.filter(
                    unit_name=dict_to_save['unit_name'])
                if unit_name_obj:
                    form_errors = {
                        'unit_name': '* This unit name already exists.',
                    }
                    error = 1
                if error == 1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSaveUnitMaster = {
                        'company_id': company_obj,
                        'unit_name': dict_to_save['unit_name'],
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = UnitMaster(**dictToSaveUnitMaster)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_unit(self, unit_id):
        unit_obj = self.model.objects.filter(pk=unit_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                unit_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_unit(self, unit_id, dict_to_save):
        error = 0
        form_errors = {}
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        unit_name_obj = self.model.objects.filter(
            unit_name=dict_to_save['unit_name']).exclude(pk=unit_id)
        if unit_name_obj:
            form_errors = {
                'unit_name': '* This unit name already exists.',
            }
            error = 1
        if error == 1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            dictToSaveUnitMaster = {
                'unit_name': dict_to_save['unit_name'],
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_add_mac'],
                'data_modi_by': logged_in_user,

            }

            UnitMaster.objects.filter(pk=unit_id).update(
                **dictToSaveUnitMaster)
            return self.respond(success=True)

    def prepare_search(self, racks, **search_kwargs):
        if search_kwargs.get('search_text'):
            racks = racks.filter(
                Q(unit_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text'))
            )
        return racks.order_by('-data_modi_on')


class BrandMasterAPI(ModuleAPI):
    model = BrandMaster

    @transaction.atomic
    def save_brand(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        form_errors = {}
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                brand_code_obj = self.model.objects.filter(
                    brand_code=dict_to_save['brand_code'])
                brand_name_obj = self.model.objects.filter(
                    brand_name=dict_to_save['brand_name'])

                if brand_code_obj:
                    form_errors = {
                        'brand_code': '* This code already exists.',
                    }
                    error = 1
                if brand_name_obj:
                    form_errors.update(
                        {'brand_name': '* This name already exists.'})
                    error = 1
                if error == 1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSaveBrandMaster = {
                        'company_id': company_obj,
                        'brand_code': dict_to_save['brand_code'],
                        'brand_name': dict_to_save['brand_name'],
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = BrandMaster(**dictToSaveBrandMaster)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_brand(self, brand_id):
        brand_obj = self.model.objects.filter(pk=brand_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                brand_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_brand(self, brand_id, dict_to_save):
        error = 0
        form_errors = {}
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        brand_code_obj = self.model.objects.filter(
            brand_code=dict_to_save['brand_code']).exclude(pk=brand_id)
        brand_name_obj = self.model.objects.filter(
            brand_name=dict_to_save['brand_name']).exclude(pk=brand_id)

        if brand_code_obj:
            form_errors = {
                'brand_code': '* This code already exists.',
            }
            error = 1
        if brand_name_obj:
            form_errors.update({'brand_name': '* This name already exists.'})
            error = 1
        if error == 1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            dictToSaveBrandMaster = {
                'brand_code': dict_to_save['brand_code'],
                'brand_name': dict_to_save['brand_name'],
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_add_mac'],
                'data_modi_by': logged_in_user,
            }
            BrandMaster.objects.filter(pk=brand_id).update(
                **dictToSaveBrandMaster)
            return self.respond(success=True)

    def prepare_search(self, brands, **search_kwargs):
        if search_kwargs.get('search_text'):
            brands = brands.filter(
                Q(brand_code__icontains=search_kwargs.get('search_text')) |
                Q(brand_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text'))
            )
        return brands.order_by('-data_modi_on')


class SizeMasterAPI(ModuleAPI):
    model = SizeMaster

    @transaction.atomic
    def save_size(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        form_errors = {}
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                size_name_obj = self.model.objects.filter(
                    size_name=dict_to_save['size_name'])

                if size_name_obj:
                    form_errors.update(
                        {'size_name': '* This name already exists.'})
                    error = 1
                if error == 1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSaveSizeMaster = {
                        'company_id': company_obj,
                        'size_name': dict_to_save['size_name'],
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = SizeMaster(**dictToSaveSizeMaster)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_size(self, size_id):
        size_obj = self.model.objects.filter(pk=size_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                size_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_size(self, size_id, dict_to_save):
        error = 0
        form_errors = {}
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        size_name_obj = self.model.objects.filter(
            size_name=dict_to_save['size_name']).exclude(pk=size_id)

        if size_name_obj:
            form_errors.update({'size_name': '* This name already exists.'})
            error = 1
        if error == 1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            dictToSaveSizeMaster = {
                'size_name': dict_to_save['size_name'],
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_add_mac'],
                'data_modi_by': logged_in_user,
            }
            SizeMaster.objects.filter(pk=size_id).update(
                **dictToSaveSizeMaster)
            return self.respond(success=True)

    def prepare_search(self, sizes, **search_kwargs):
        if search_kwargs.get('search_text'):
            sizes = sizes.filter(
                Q(size_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text'))
            )
        return sizes.order_by('-data_modi_on')


class PatternMasterAPI(ModuleAPI):
    model = PatternMaster

    @transaction.atomic
    def save_pattern(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        form_errors = {}
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                pattern_name_obj = self.model.objects.filter(
                    pattern_name=dict_to_save['pattern_name'])

                if pattern_name_obj:
                    form_errors.update(
                        {'pattern_name': '* This name already exists.'})
                    error = 1
                if error == 1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSavePatternMaster = {
                        'company_id': company_obj,
                        'pattern_name': dict_to_save['pattern_name'],
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = PatternMaster(**dictToSavePatternMaster)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_pattern(self, pattern_id):
        pattern_obj = self.model.objects.filter(pk=pattern_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                pattern_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_pattern(self, pattern_id, dict_to_save):
        error = 0
        form_errors = {}
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        pattern_name_obj = self.model.objects.filter(
            pattern_name=dict_to_save['pattern_name']).exclude(pk=pattern_id)

        if pattern_name_obj:
            form_errors.update({'pattern_name': '* This name already exists.'})
            error = 1
        if error == 1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            dictToSavePatternMaster = {
                'pattern_name': dict_to_save['pattern_name'],
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_add_mac'],
                'data_modi_by': logged_in_user,
            }
            PatternMaster.objects.filter(pk=pattern_id).update(
                **dictToSavePatternMaster)
            return self.respond(success=True)

    def prepare_search(self, patterns, **search_kwargs):
        if search_kwargs.get('search_text'):
            patterns = patterns.filter(
                Q(pattern_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text'))
            )
        return patterns.order_by('-data_modi_on')


class CorporateAccountAPI(ModuleAPI):
    model = CorporateAccount


    def prepare_search(self, corporate_accounts, **search_kwargs):

        if search_kwargs.get('parent_corporate_accounts'):
            corporate_accounts = corporate_accounts.filter(
                Q(account_flag=0)
            )
            return corporate_accounts.order_by('-data_modi_on')

        if search_kwargs.get('corporateaccounts_of_selected_itemgroup'):
            corporate_accounts = corporate_accounts.filter(
                Q(itemgroup=search_kwargs.get('search_text'))
            )
            return corporate_accounts.order_by('-data_modi_on')

        if search_kwargs.get('search_text'):
            corporate_accounts = corporate_accounts.filter(
                
            )
        return corporate_accounts.order_by('-data_modi_on')

    

    def delete_on(): # Should be protected
        pass

class ItemGroupAPI(ModuleAPI):
    model = ItemGroup

    relations_to_serialize = {
        'purchaseaccount_id': '',
    }
    extras = ('')

    @transaction.atomic
    def save_itemgroup(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                corporate_account_obj = CorporateAccount.objects.get(pk=dict_to_save['purchaseaccount_id'])
                itemgroup_code_obj = self.model.objects.filter(itemgroup_code=dict_to_save['itemgroup_code'])
                itemgroup_name_obj = self.model.objects.filter(itemgroup_name=dict_to_save['itemgroup_name'])

                if itemgroup_code_obj:
                    form_errors = {
                        'itemgroup_code': '* This code already exists.',
                    }
                    error = 1
                if itemgroup_name_obj:
                    form_errors.update({'itemgroup_name': '* This name already exists.'})
                    error = 1
                if error==1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSaveItemGroup = {
                        'company_id': company_obj,
                        'itemgroup_code': dict_to_save['itemgroup_code'],
                        'itemgroup_name': dict_to_save['itemgroup_name'],
                        'purchaseaccount_id' : corporate_account_obj,
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = ItemGroup(**dictToSaveItemGroup)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_itemgroup(self,itemgroup_id):
        itemgroup_obj = self.model.objects.filter(pk=itemgroup_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                itemgroup_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
                )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_itemgroup(self, itemgroup_id, dict_to_save):
        error = 0
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        corporate_account_obj = CorporateAccount.objects.get(pk=dict_to_save['purchaseaccount_id'])
        itemgroup_code_obj = self.model.objects.filter(itemgroup_code=dict_to_save['itemgroup_code']).exclude(pk=itemgroup_id)
        itemgroup_name_obj = self.model.objects.filter(itemgroup_name=dict_to_save['itemgroup_name']).exclude(pk=itemgroup_id)

        if itemgroup_code_obj:
            form_errors = {
                'itemgroup_code': '* This code already exists.',
            }
            error = 1
        if itemgroup_name_obj:
            form_errors.update({'itemgroup_name': '* This name already exists.'})
            error = 1
        if error==1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            dictToSaveItemGroup = {
                'itemgroup_code': dict_to_save['itemgroup_code'],
                'itemgroup_name': dict_to_save['itemgroup_name'],
                'purchaseaccount_id' : corporate_account_obj,
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_add_mac'],
                'data_modi_by': logged_in_user,
            }
            ItemGroup.objects.filter(pk=itemgroup_id).update(
                **dictToSaveItemGroup)
            return self.respond(success=True)

    @transaction.atomic
    def delete_one(self, itemgroup_id):  #Overridden method to prevent foreign key violation
        try:
            with transaction.atomic():
                itemgroup_obj = ItemGroup.objects.get(pk=itemgroup_id)
                itemgroup_obj.delete()
                return self.respond(success=True)
        except IntegrityError:
            form_errors = {
                'itemgroup_delete_error': 'Itemgroup deletion is not possible due to usage in other forms',
            }
            return self.respond(success=False, form_errors=form_errors)
            # handle_exception()

    def prepare_search(self, itemgroups, **search_kwargs):

        if search_kwargs.get('search_text'):
            itemgroups = itemgroups.filter(
                Q(itemgroup_code__icontains=search_kwargs.get('search_text')) |
                Q(itemgroup_name__icontains=search_kwargs.get('search_text')) |
                Q(purchaseaccount_id__corporatehead_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text')) 
            )
        return itemgroups.order_by('-data_modi_on')


class ItemSubGroupAPI(ModuleAPI):
    model = ItemSubGroup

    relations_to_serialize = {
        'itemgroup_id': '',
    }
    extras = ('')

    @transaction.atomic
    def save_itemsubgroup(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        form_errors ={}
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                itemgroup_obj = ItemGroup.objects.get(pk=dict_to_save['itemgroup_id'])
                item_subgroup_code_obj = self.model.objects.filter(item_subgroup_code=dict_to_save['item_subgroup_code'])
                item_subgroup_name_obj = self.model.objects.filter(item_subgroup_name=dict_to_save['item_subgroup_name'])

                if item_subgroup_code_obj:
                    form_errors = {
                        'item_subgroup_code': '* This code already exists.',
                    }
                    error = 1
                if item_subgroup_name_obj:
                    form_errors.update({'item_subgroup_name': '* This name already exists.'})
                    error = 1
                if error==1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSaveItemSubGroup = {
                        'company_id': company_obj,
                        'item_subgroup_code': dict_to_save['item_subgroup_code'],
                        'item_subgroup_name': dict_to_save['item_subgroup_name'],
                        'itemgroup_id' : itemgroup_obj,
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = ItemSubGroup(**dictToSaveItemSubGroup)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_itemsubgroup(self,itemsubgroup_id):
        itemsubgroup_obj = self.model.objects.filter(pk=itemsubgroup_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                itemsubgroup_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
                )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_itemsubgroup(self, itemsubgroup_id, dict_to_save):
        error = 0
        form_errors = {}
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        itemgroup_obj = ItemGroup.objects.get(pk=dict_to_save['itemgroup_id'])
        item_subgroup_code_obj = self.model.objects.filter(item_subgroup_code=dict_to_save['item_subgroup_code']).exclude(pk=itemsubgroup_id)
        item_subgroup_name_obj = self.model.objects.filter(item_subgroup_name=dict_to_save['item_subgroup_name']).exclude(pk=itemsubgroup_id)

        if item_subgroup_code_obj:
            form_errors = {
                'item_subgroup_code': '* This code already exists.',
            }
            error = 1
        if item_subgroup_name_obj:
            form_errors.update({'item_subgroup_name': '* This name already exists.'})
            error = 1
        if error==1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            dictToSaveItemSubGroup = {
                'item_subgroup_code': dict_to_save['item_subgroup_code'],
                'item_subgroup_name': dict_to_save['item_subgroup_name'],
                'itemgroup_id' : itemgroup_obj,
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_modi_mac'],
                'data_modi_by': logged_in_user,
            }
            ItemSubGroup.objects.filter(pk=itemsubgroup_id).update(
                **dictToSaveItemSubGroup)
            return self.respond(success=True)

    def prepare_search(self, itemsubgroups, **search_kwargs):

        if search_kwargs.get('itemsubgroups_of_selected_itemgroup'):
            itemsubgroups = itemsubgroups.filter(
                Q(itemgroup_id__pk=search_kwargs.get('search_text')) 
            )
            return itemsubgroups.order_by('-data_modi_on')

        if search_kwargs.get('search_text'):
            itemsubgroups = itemsubgroups.filter(
                Q(item_subgroup_code__icontains=search_kwargs.get('search_text')) |
                Q(item_subgroup_name__icontains=search_kwargs.get('search_text')) |
                Q(itemgroup_id__itemgroup_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text')) 
            )
        return itemsubgroups.order_by('-data_modi_on')

class ItemMasterAPI(ModuleAPI):
    model = ItemMaster

    relations_to_serialize = {
        'itemgroup_id': '',
        'item_subgroup_id':'',
        'brand_id':'',
        'unit_id':'',
        'size_id':'',
        'purchaseaccount_id':'',
        'pattern_id':'',

    }

    @transaction.atomic
    def save_item(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0,asd=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        form_errors ={}
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                item_code_obj = self.model.objects.filter(item_code=dict_to_save['item_code'])
                item_short_code_obj = self.model.objects.filter(item_short_code=dict_to_save['item_short_code'])
                item_name_obj = self.model.objects.filter(item_name=dict_to_save['item_name'])

                
                if item_code_obj:
                    form_errors.update({'item_code':'* This code already exists.'})
                    error = 1
                if item_short_code_obj:
                    form_errors.update({'item_short_code':'* This short name already exists.'})
                    error = 1
                if item_name_obj:
                    form_errors.update({'item_name':'* This name already exists.'})
                    error = 1
                if error==1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    brand_obj = BrandMaster.objects.get(pk=dict_to_save['brand_id'])
                    if dict_to_save['size_id']:
                        size_obj = SizeMaster.objects.get(pk=dict_to_save['size_id'])
                        if dict_to_save['pattern_id']:
                            pattern_obj = PatternMaster.objects.get(pk=dict_to_save['pattern_id'])
                            item_description = dict_to_save['item_name']+"-"+brand_obj.brand_name+"-"+size_obj.size_name+"-"+pattern_obj.pattern_name
                        else:
                            item_description = dict_to_save['item_name']+"-"+brand_obj.brand_name+"-"+size_obj.size_name
                    elif dict_to_save['pattern_id']:
                        pattern_obj = PatternMaster.objects.get(pk=dict_to_save['pattern_id'])
                        item_description = dict_to_save['item_name']+"-"+brand_obj.brand_name+"-"+pattern_obj.pattern_name
                    else:
                        size_obj = dict_to_save['size_id']
                        item_description = dict_to_save['item_name']+"-"+brand_obj.brand_name
                    unit_obj = UnitMaster.objects.get(pk=dict_to_save['unit_id'])
                    # print dict_to_save['item_image']
                    dictToSaveItem = {
                        'company_id': company_obj,
                        'item_code': dict_to_save['item_code'],
                        'item_short_code': dict_to_save['item_short_code'],
                        'item_name': dict_to_save['item_name'],
                        'item_description': item_description,
                        'brand_id_id': dict_to_save['brand_id'],
                        'size_id_id': dict_to_save['size_id'],
                        'unit_id_id': dict_to_save['unit_id'],
                        'pattern_id_id': dict_to_save['pattern_id'],
                        'itemgroup_id_id': dict_to_save['itemgroup_id'],
                        'item_subgroup_id_id': dict_to_save['item_subgroup_id'],
                        'reorder_level': 0.0,
                        'is_active': dict_to_save['is_active'],
                        'purchaseaccount_id_id': dict_to_save['purchaseaccount_id'],
                        'item_image': dict_to_save['item_image'],
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = ItemMaster(**dictToSaveItem)
                    form_data.save()

                    # Image Upload
                    # extension = os.path.splitext(request.POST[form_id_name].filename)[1]
                    # extension = 'jpeg'
                    # short_id = str(random.randint(1, 999999999))
                    # new_file_name =  short_id + extension
                    # input_file = request.POST[form_id_name].file
                    # file_path = os.path.join(os.environ['PROJECT_PATH'] + '/static/memberphotos/', new_file_name)

                    # output_file = open(file_path, 'wb')
                    # input_file.seek(0)
                    # while 1:
                    #     data = input_file.read(2<<16)
                    #     if not data:
                    #         break
                    #     output_file.write(data)
                    # output_file.close()
                    # End 
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_item(self,item_id):
        item_obj = self.model.objects.filter(pk=item_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                item_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
                )
        )
        return self.respond(
            objects=serialized_objects,
        )


    def edit_item(self, item_id, dict_to_save):
        error = 0
        form_errors = {}
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        
        item_code_obj = self.model.objects.filter(item_code=dict_to_save['item_code']).exclude(pk=item_id)
        item_short_code_obj = self.model.objects.filter(item_short_code=dict_to_save['item_short_code']).exclude(pk=item_id)
        item_name_obj = self.model.objects.filter(item_name=dict_to_save['item_name']).exclude(pk=item_id)

        
        if item_code_obj:
            form_errors.update({'item_code':'* This code already exists.'})
            error = 1
        if item_short_code_obj:
            form_errors.update({'item_short_code':'* This short name already exists.'})
            error = 1
        if item_name_obj:
            form_errors.update({'item_name':'* This name already exists.'})
            error = 1
        if error==1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            brand_obj = BrandMaster.objects.get(pk=dict_to_save['brand_id'])
            if dict_to_save['size_id']:
                size_obj = SizeMaster.objects.get(pk=dict_to_save['size_id'])
                if dict_to_save['pattern_id']:
                    pattern_obj = PatternMaster.objects.get(pk=dict_to_save['pattern_id'])
                    item_description = dict_to_save['item_name']+"-"+brand_obj.brand_name+"-"+size_obj.size_name+"-"+pattern_obj.pattern_name
                else:
                    item_description = dict_to_save['item_name']+"-"+brand_obj.brand_name+"-"+size_obj.size_name
            elif dict_to_save['pattern_id']:
                pattern_obj = PatternMaster.objects.get(pk=dict_to_save['pattern_id'])
                item_description = dict_to_save['item_name']+"-"+brand_obj.brand_name+"-"+pattern_obj.pattern_name
            else:
                size_obj = dict_to_save['size_id']
                item_description = dict_to_save['item_name']+"-"+brand_obj.brand_name
            unit_obj = UnitMaster.objects.get(pk=dict_to_save['unit_id'])

            dictToSaveItem = {
                'item_code': dict_to_save['item_code'],
                'item_short_code': dict_to_save['item_short_code'],
                'item_name': dict_to_save['item_name'],
                'item_description': item_description,
                'brand_id_id': dict_to_save['brand_id'],
                'size_id_id': dict_to_save['size_id'],
                'unit_id_id': dict_to_save['unit_id'],
                'pattern_id_id': dict_to_save['pattern_id'],
                'itemgroup_id_id': dict_to_save['itemgroup_id'],
                'item_subgroup_id_id': dict_to_save['item_subgroup_id'],
                'reorder_level': dict_to_save['reorder_level'],
                'is_active': dict_to_save['is_active'],
                'purchaseaccount_id_id': dict_to_save['purchaseaccount_id'],
                'item_image': dict_to_save['item_image'],
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_modi_mac'],
                'data_modi_by': logged_in_user,
            }
            ItemMaster.objects.filter(pk=item_id).update(
                **dictToSaveItem)
            return self.respond(success=True)

    def prepare_search(self, items, **search_kwargs):

        if search_kwargs.get('item_description'):
            items = items.filter(
                Q(item_code__icontains=search_kwargs.get('search_text')) |
                Q(item_short_code__icontains=search_kwargs.get('search_text')) |
                Q(item_name__icontains=search_kwargs.get('search_text')) |
                Q(item_description__icontains=search_kwargs.get('search_text'))
            )
            if search_kwargs.get('itemgroup_id')!="":
                if search_kwargs.get('item_subgroup_id')=="":
                    items = items.filter(
                        Q(itemgroup_id__pk=search_kwargs.get('itemgroup_id')) 
                    )
                else:
                    print "itemgroup_id",search_kwargs.get('itemgroup_id')
                    print "item_subgroup",search_kwargs.get('item_subgroup_id')
                    items = items.filter(
                        Q(itemgroup_id=search_kwargs.get('itemgroup_id')) &
                        Q(item_subgroup_id=search_kwargs.get('item_subgroup_id'))
                    )

            return items.order_by('-data_modi_on')

        if search_kwargs.get('search_text'):
            items = items.filter(
                Q(item_code__icontains=search_kwargs.get('search_text')) |
                Q(item_short_code__icontains=search_kwargs.get('search_text')) |
                Q(item_name__icontains=search_kwargs.get('search_text')) |
                Q(itemgroup_id__itemgroup_name__icontains=search_kwargs.get('search_text')) |
                Q(item_subgroup_id__item_subgroup_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text')) 
            )
        return items.order_by('-data_modi_on')


class CountryMasterAPI(ModuleAPI):
    model = CountryMaster

class StateMasterAPI(ModuleAPI):
    model = StateMaster

    def prepare_search(self, states, **search_kwargs):

        if search_kwargs.get('states_of_selected_country'):
            states = states.filter(
                Q(country_id__pk=search_kwargs.get('search_text')) 
            )
            return states

        if search_kwargs.get('search_text'):
            states = states.filter(
               
            )
        return states

class SupplierMasterAPI(ModuleAPI):
    model = SupplierMaster

    relations_to_serialize = {
        'country_id': '',
        'state_id':'',
    }
    @transaction.atomic
    def save_supplier(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        branch_id = self.httpRequest.session.get('default_branch_id')
        form_errors ={}
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                supplier_code_obj = self.model.objects.filter(supplier_code=dict_to_save['supplier_code'])
                supplier_name_obj = self.model.objects.filter(supplier_name=dict_to_save['supplier_name'])
                
                if supplier_code_obj:
                    form_errors.update({'supplier_code':'* This code already exists.'})
                    error = 1
                if supplier_name_obj:
                    form_errors.update({'supplier_name':'* This name already exists.'})
                    error = 1
                if error==1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                   
                    dictToSaveSupplier = {
                        'company_id': company_obj,
                        'branch_id_id' : branch_id,
                        'supplier_code': dict_to_save['supplier_code'],
                        'supplier_name': dict_to_save['supplier_name'],
                        'contact_person': dict_to_save['contact_person'],
                        'contact_phone': dict_to_save['contact_phone'],
                        'contact_mobile': dict_to_save['contact_mobile'],
                        'contact_email': dict_to_save['contact_email'],
                        'supplier_address': dict_to_save['supplier_address'],
                        'country_id_id': dict_to_save['country_id'],
                        'state_id_id': dict_to_save['state_id'],
                        'pincode': dict_to_save['pincode'],
                        'office_phone': dict_to_save['office_phone'],
                        'office_mobile': dict_to_save['office_mobile'],
                        'office_email_id': dict_to_save['office_email_id'],
                        'is_active': dict_to_save['is_active'],
                        'is_billwisesettle': dict_to_save['is_billwisesettle'],
                        'credit_limit': dict_to_save['credit_limit'],
                        'is_blacklisted': dict_to_save['is_blacklisted'],
                        'supplier_bank': dict_to_save['supplier_bank'],
                        'supplier_branch': dict_to_save['supplier_branch'],
                        'supplier_account_no': dict_to_save['supplier_account_no'],
                        'supplier_ifsc': dict_to_save['supplier_ifsc'],
                        'supplier_kgst_no': dict_to_save['supplier_kgst_no'],
                        'supplier_cst_no': dict_to_save['supplier_cst_no'],
                        'supplier_pan': dict_to_save['supplier_pan'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],

                        'link_account_id':1
                    }
                    form_data = SupplierMaster(**dictToSaveSupplier)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_supplier(self,supplier_id):
        supplier_obj = self.model.objects.filter(pk=supplier_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                supplier_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
                )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_supplier(self, supplier_id, dict_to_save):
        error = 0
        form_errors = {}
        branch_id = self.httpRequest.session.get('default_branch_id')
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        
        supplier_code_obj = self.model.objects.filter(supplier_code=dict_to_save['supplier_code']).exclude(pk=supplier_id)
        supplier_name_obj = self.model.objects.filter(supplier_name=dict_to_save['supplier_name']).exclude(pk=supplier_id)
        
        if supplier_code_obj:
            form_errors.update({'supplier_code':'* This code already exists.'})
            error = 1
        if supplier_name_obj:
            form_errors.update({'supplier_name':'* This name already exists.'})
            error = 1
        if error==1:
            return self.respond(success=False, form_errors=form_errors)
        else:
           
            dictToSaveSupplier = {
                'branch_id_id' : branch_id,
                'supplier_code': dict_to_save['supplier_code'],
                'supplier_name': dict_to_save['supplier_name'],
                'contact_person': dict_to_save['contact_person'],
                'contact_phone': dict_to_save['contact_phone'],
                'contact_mobile': dict_to_save['contact_mobile'],
                'contact_email': dict_to_save['contact_email'],
                'supplier_address': dict_to_save['supplier_address'],
                'country_id_id': dict_to_save['country_id'],
                'state_id_id': dict_to_save['state_id'],
                'pincode': dict_to_save['pincode'],
                'office_phone': dict_to_save['office_phone'],
                'office_mobile': dict_to_save['office_mobile'],
                'office_email_id': dict_to_save['office_email_id'],
                'is_active': dict_to_save['is_active'],
                'is_billwisesettle': dict_to_save['is_billwisesettle'],
                'credit_limit': dict_to_save['credit_limit'],
                'is_blacklisted': dict_to_save['is_blacklisted'],
                'supplier_bank': dict_to_save['supplier_bank'],
                'supplier_branch': dict_to_save['supplier_branch'],
                'supplier_account_no': dict_to_save['supplier_account_no'],
                'supplier_ifsc': dict_to_save['supplier_ifsc'],
                'supplier_kgst_no': dict_to_save['supplier_kgst_no'],
                'supplier_cst_no': dict_to_save['supplier_cst_no'],
                'supplier_pan': dict_to_save['supplier_pan'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_modi_mac'],
                'data_modi_by': logged_in_user,

                'link_account_id':1
            }

            SupplierMaster.objects.filter(pk=supplier_id).update(
                **dictToSaveSupplier)
            return self.respond(success=True)

    def prepare_search(self, suppliers, **search_kwargs):

        if search_kwargs.get('search_text'):
            suppliers = suppliers.filter(
                Q(supplier_code__icontains=search_kwargs.get('search_text')) |
                Q(supplier_name__icontains=search_kwargs.get('search_text')) |
                Q(contact_person__icontains=search_kwargs.get('search_text')) |
                Q(contact_phone__icontains=search_kwargs.get('search_text'))
            )
        return suppliers.order_by('-data_modi_on')

class CategoryAPI(ModuleAPI):
    model = Category


    @transaction.atomic
    def save_category(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        form_errors ={}
        try:
            with transaction.atomic():
                error = 0
                company_obj = CompanyMaster.objects.all().first()
                category_code_obj = self.model.objects.filter(category_code=dict_to_save['category_code'])
                category_name_obj = self.model.objects.filter(category_name=dict_to_save['category_name'])
                if category_code_obj:
                    form_errors = {
                        'category_code': '* This code already exists.',
                    }
                    error = 1
                if category_name_obj:
                    form_errors.update({'category_name' : '* This name already exists.'})
                    error = 1
                if error==1:
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSaveCategory = {
                        'company_id': company_obj,
                        'category_code': dict_to_save['category_code'],
                        'category_name': dict_to_save['category_name'],
                        'remarks': dict_to_save['remarks'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = Category(**dictToSaveCategory)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_category(self,category_id):
        category_obj = self.model.objects.filter(pk=category_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                category_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
                )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_category(self, category_id, dict_to_save):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        error = 0
        form_errors = {}
        category_code_obj = self.model.objects.filter(category_code=dict_to_save['category_code']).exclude(pk=category_id)
        category_name_obj = self.model.objects.filter(category_name=dict_to_save['category_name']).exclude(pk=category_id)
        if category_code_obj:
            form_errors = {
                'category_code': '* This code already exists.',
            }
            error = 1
        if category_name_obj:
            form_errors.update({'category_name' : '* This name already exists.'})
            error = 1
        if error==1:
            return self.respond(success=False, form_errors=form_errors)
        else:
            dictToSaveCategory = {
                'category_code': dict_to_save['category_code'],
                'category_name': dict_to_save['category_name'],
                'remarks': dict_to_save['remarks'],
                'data_modi_on': datetime.datetime.now(),
                'data_modi_mac': dict_to_save['data_add_mac'],
                'data_modi_by': logged_in_user,
            }
            Category.objects.filter(pk=category_id).update(
                **dictToSaveCategory)
            return self.respond(success=True)

    def prepare_search(self, categories, **search_kwargs):

        if search_kwargs.get('search_text'):
            categories = categories.filter(
                Q(category_code__icontains=search_kwargs.get('search_text')) |
                Q(category_name__icontains=search_kwargs.get('search_text')) |
                Q(remarks__icontains=search_kwargs.get('search_text'))
            )
        return categories.order_by('-data_modi_on')

class ItemRackMappingAPI(ModuleAPI):
    model = ItemRackMapping

    relations_to_serialize = {
        'item_id': '',
        'rack_id':'',
        'store_id':'',
    }

    @transaction.atomic
    def save_mapping(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        form_msgs ={}
        try:
            with transaction.atomic():
                item_rack_mapping_obj_exists = self.model.objects.filter(store_id=dict_to_save['store_id'],item_id=dict_to_save['item_id'])
                if item_rack_mapping_obj_exists:
                    if item_rack_mapping_obj_exists[0].rack_id != dict_to_save['rack_id']:
                        if dict_to_save['rack_id']==-1:
                            item_rack_mapping_obj_exists[0].delete()
                            form_msgs.update({'success':'Mapping Deleted Successfully'})
                        else:
                            item_rack_mapping_obj_exists[0].rack_id_id = dict_to_save['rack_id']
                            item_rack_mapping_obj_exists[0].save()
                            form_msgs.update({'success':'Mapping Edited Successfully'})

                else:
                    dictToSaveMapping = {
                        'store_id_id': dict_to_save['store_id'],
                        'rack_id_id': dict_to_save['rack_id'],
                        'item_id_id': dict_to_save['item_id'],
                    }
                    form_data = ItemRackMapping(**dictToSaveMapping)
                    form_data.save()
                    form_msgs.update({'success':'Mapping Added Successfully'})
                return self.respond(success=True,form_msgs=form_msgs)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

class RFQMasterAPI(ModuleAPI):
    model = RFQHead

    def save_rfq(self, dict_to_save):
        # user = UserMaster.objects.get(pk=user_id)
        # logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        # data_add_mac = ''
        print '$' * 25
        print dict_to_save
        try:

            logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
            # checks whether any branch exists with the branch code
            if not dict_to_save.get('forwarded_to'):
                form_errors = {
                    'user_select_error': '* This field is required',
                }
                return self.respond(success=False, form_errors=form_errors)
            forwarded_user = UserMaster.objects.get(
                pk=dict_to_save['forwarded_to'])
            if not dict_to_save.get('rfq_no'):
                form_errors = {
                    'rfq_no_error': '* This field is required',
                }
                return self.respond(success=False, form_errors=form_errors)
            rfq_no = dict_to_save['rfq_no']
            if not dict_to_save.get('rfq_date'):
                form_errors = {
                    'rfq_date_error': '* This field is required',
                }
                return self.respond(success=False, form_errors=form_errors)
            rfq_date = datetime.datetime.strptime(
                dict_to_save['rfq_date'], '%Y-%d-%m').strftime('%Y-%m-%d')
            if not dict_to_save.get('quot_date'):
                form_errors = {
                    'quot_date_error': '* This field is required',
                }
                return self.respond(success=False, form_errors=form_errors)
            quot_date = datetime.datetime.strptime(
                dict_to_save['quot_date'], '%Y-%d-%m').strftime('%Y-%m-%d')
            item_details = dict_to_save['item_details']
            print 'item_details'
            print item_details
            print len(item_details)
            if item_details:
                if len(item_details) > 1:
                    common_pair_dict = dict(set.intersection(*(set(d.iteritems())
                                                               for d in item_details)))
                    if common_pair_dict.get('req_quantity'):
                        del common_pair_dict['req_quantity']
                    print 'common_pair_dict', common_pair_dict
                    if common_pair_dict:
                        form_errors = {
                            'item_select_error': '* Item already selected',
                        }
                        return self.respond(success=False, form_errors=form_errors)
            else:
                form_errors = {
                    'item_select_error': '* This field is required',
                }
                return self.respond(success=False, form_errors=form_errors)

            terms = dict_to_save.get('terms', '')

            if self.model.objects.filter(rfq_no=rfq_no).all():
                form_errors = {
                    'rfq_no_error': '* RFQ number already exists.',
                }
                return self.respond(success=False, form_errors=form_errors)
            company_obj = CompanyMaster.objects.all().first()
            default_branch_id = self.httpRequest.session.get(
                'default_branch_id')
            if not default_branch_id:
                return self.respond(success=False)
            branch_obj = BranchMaster.objects.get(pk=default_branch_id)
            dictToSaveRFQ = {
                'company_id': company_obj,
                'branch_id': branch_obj,
                'rfq_no': rfq_no,
                'rfq_date': rfq_date,
                'rfq_last_date': quot_date,
                'terms_condition': terms,
                'forwarded_to': forwarded_user,
                'data_add_by': logged_in_user,
                'action': False,
                'data_add_mac': dict_to_save.get('data_add_mac', '')
            }
            rfq_data = self.model(**dictToSaveRFQ)
            rfq_data.save()
            for _item in item_details:
                item = ItemMaster.objects.get(pk=_item.get('item_id'))
                RFQDetails(
                    rfq_id=rfq_data, item_id=item, req_qty=_item.get('req_quantity')).save()
            return self.respond(success=True)
        except Exception, e:
            print '@' * 30
            print e

    def get_rfq(self, rfq_id):
        # returns the rfq details
        rfq_obj = self.model.objects.get(pk=rfq_id)
        rfq_details = model_to_dict(rfq_obj,  fields=[
                                    'rfq_id', 'rfq_no', 'rfq_date', 'rfq_last_date', 'terms_condition', 'forwarded_to'])
        dthandler = lambda obj: (
            obj.isoformat()
            if isinstance(obj, datetime.datetime)
            or isinstance(obj, datetime.date)
            else None)
        rfq_details.update({'items': []})
        for obj in rfq_obj.rfqdetails_set.all():
            rfq_details['items'].append(
                {'item_id': obj.item_id.pk, 'req_qty': obj.req_qty})

        print rfq_details
        rfq_details = json.dumps(rfq_details, default=dthandler)
        return self.respond(
            objects=rfq_details,
        )

    def edit_rfq(self, rfq_id, dict_to_save):
        print '^' * 34
        print 'editing...'
        print dict_to_save
        try:
            # edit an rfq
            logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
            item_details = dict_to_save['item_details']
            rfq_obj = self.model.objects.get(pk=rfq_id)
            print 'rfq_obj.action'
            print rfq_obj.action
            if rfq_obj.quotationhead_set.all() or rfq_obj.action != None:
                form_errors = {
                    'alert_failed': '* Edit not permitted.',
                }
                return self.respond(success=False, form_errors=form_errors)

            if item_details:
                if len(item_details) > 1:
                    common_pair_dict = dict(set.intersection(*(set(d.iteritems())
                                                               for d in item_details)))
                    if common_pair_dict.get('req_quantity'):
                        del common_pair_dict['req_quantity']
                    print 'common_pair_dict', common_pair_dict
                    if common_pair_dict:
                        form_errors = {
                            'item_select_error': '* Item already selected',
                        }
                        return self.respond(success=False, form_errors=form_errors)
            else:
                form_errors = {
                    'item_select_error': '* This field is required',
                }
                return self.respond(success=False, form_errors=form_errors)

            rfq_no = dict_to_save['rfq_no']
            # checks whether any rfq exists with the rfq code
            if self.model.objects.filter(
                    rfq_no=rfq_no).exclude(pk=rfq_id).all():
                form_errors = {
                    'rfq_no_error': '* RFQ number already exists.',
                }
                return self.respond(success=False, form_errors=form_errors)

            # all constraints satisfied, updates the rfq

            RFQDetails.objects.filter(rfq_id=rfq_obj.pk).delete()
            dictToSaveRFQ = {
                'rfq_no': dict_to_save['rfq_no'],
                'rfq_date': dict_to_save['rfq_date'],
                'rfq_last_date': dict_to_save['quot_date'],
                'terms_condition': dict_to_save['terms'],
                'forwarded_to': UserMaster.objects.get(pk=dict_to_save['forwarded_to']),
                'data_modi_by': logged_in_user,
                'data_modi_mac': dict_to_save['data_add_mac'],
            }
            self.model.objects.filter(pk=rfq_id).update(
                **dictToSaveRFQ)
            item_details = dict_to_save['item_details']
            for _item in item_details:
                item = ItemMaster.objects.get(pk=_item.get('item_id'))
                RFQDetails(
                    rfq_id=rfq_obj, item_id=item, req_qty=_item.get('req_quantity')).save()
            return self.respond(success=True)
        except Exception, e:
            import traceback
            print '+' * 34
            print traceback.format_exc()
            print e

    def get_all(self, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        objects = self.prepare_search(
            self.model.objects.all(), **search_kwargs)
        prev_page_no = 0
        next_page_no = 0
        current_page_number = 0
        num_of_pages = objects.count()
        if not int(show_all):
            pagination_values = paginator(objects, page_no, per_page)
            objects = pagination_values['objects']
            prev_page_no = pagination_values['prev_page_no']
            next_page_no = pagination_values['next_page_no']
            current_page_number = pagination_values['current_page_number']
            num_of_pages = pagination_values['num_of_pages']
        try:
            next_rfq_no = int(
                self.model.objects.order_by('-rfq_no')[0].rfq_no) + 1
        except Exception, e:
            next_rfq_no = 1
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                objects,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
            previous_page_number=prev_page_no,
            next_page_number=next_page_no,
            current_page_number=current_page_number,
            num_of_pages=num_of_pages,
            per_page=per_page,
            next_rfq_no=next_rfq_no
        )

    @transaction.atomic
    def delete_one(self, id, **kwargs):
        try:
            with transaction.atomic():
                RFQDetails.objects.filter(rfq_id=id).delete()
                object_to_get = self.model.objects.filter(pk__exact=id).first()
                object_to_get.delete()
                self.delete_trigger(**kwargs)
        except IntegrityError, e:
            print e
            return self.respond(success=False)
        return self.respond(success=True)
