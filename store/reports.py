from geraldo import Report, landscape, ReportBand, ObjectValue
from reportlab.lib.pagesizes import A5
from reportlab.lib.units import cm

class ReportPurchase(Report):
    title = 'My Family'
    title = 'Purchases list'
    author = 'John Smith Corporation'

    page_size = landscape(A5)
    margin_left = 2*cm
    margin_top = 0.5*cm
    margin_right = 0.5*cm
    margin_bottom = 0.5*cm

    class band_detail(ReportBand):
        height = 0.5*cm
        elements=(
            ObjectValue(attribute_name='item_id', left=0.5*cm),
            ObjectValue(attribute_name='item_name', left=3*cm),
            )

        