from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import viewsets
from .serializers import *

from rest_framework.views import APIView
# Create your views here.
# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# class FinancialyearListView(viewsets.ModelViewSet):
# 	queryset = FinancialYear.objects.all()
# 	serializer_class = FinancialYearSerializer


from administration.models import FinancialYear
# from api.serializers import SnippetSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import json


from rest_framework import mixins
from rest_framework import generics



from datetime import datetime

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial

class FinancialyearListView(APIView):
    
    queryset = FinancialYear.objects.all()
    def get(self, request, format=None):
        snippets = FinancialYear.objects.all()
        serializer = FinancialYearSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = FinancialYearSerializer(data=request.data)
        company_obj = CompanyMaster.objects.all().first()
        print "joseph",request.data
        dicts = {
        'company_id':company_obj.company_id,
        }
        # company_obj.data_add_on = str(company_obj.data_add_on)
        # company_obj.data_mod_ion = str(company_obj.data_mod_ion) 
        # request.data['company_ref'] = json.dumps(company_obj.__dict__)
        request.data['company_id'] = company_obj
        # from datetime import datetime
        # from json import dumps

        # request.data['company_ref'] =  dumps(datetime.now(), default=json_serial)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
# class FinancialyearListView(mixins.ListModelMixin,
#                      mixins.CreateModelMixin,
#                      generics.GenericAPIView):

#     queryset = FinancialYear.objects.all()
#     serializer_class = FinancialYearSerializer

#     def get(self, request, *args, **kwargs):
#         return self.list(request, *args, **kwargs)

#     def post(self, request, *args, **kwargs):
#         return self.create(request, *args, **kwargs)
