from django.conf.urls import url, include, patterns
from rest_framework import routers
from .views import *
from api import views

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
administration = routers.DefaultRouter()
# administration.register(r'^$',views.FinancialyearListView.as_view(),base_name='financialyear')



administration_urls = patterns('',
    # url(r'^/(?P<pk>\d+)$', PhotoDetail.as_view(), name='photo-detail'),
    url(r'^/financialyears', FinancialyearListView.as_view(), name='photo-list')
)
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^administration',include(administration_urls)),
]
