from kca.common.api import ModuleAPI
from django.db.models import Q
from models import *
from kca.common.helpers import paginator
from kca.common.constants import STARTING_PAGE, DEFAUT_PER_PAGE
from django.core import serializers
import datetime
from kca.common.permission_check import Manager
from kca.mailer.mailer import SendNotification
from django.core.mail import send_mail
import json
from django.core import serializers
from django.db import IntegrityError, transaction

company_obj = CompanyMaster.objects.all().first()


class FinancialyearAPI(ModuleAPI):
    model = FinancialYear
    # form_class = ProjectForm
    # relations_to_serialize = {
    #     'client': '',
    #     'project_manager': {
    #         'relations': ('user', ),
    #         'extras':('profile_image_url', )
    #     },
    #     'project_category': ''
    # }
    # extras = ('__unicode__', 'profile_url', 'status_to_label')

    @transaction.atomic
    def save_financialyear(self, dict_to_save, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        try:
            with transaction.atomic():
                company_obj = CompanyMaster.objects.all().first()
                fin_year_obj = FinancialYear.objects.filter(
                    fin_year_code=dict_to_save['fin_year_code'])
                if fin_year_obj:
                    form_errors = {
                        'fin_year_code': '* This code already exists',
                    }
                    return self.respond(success=False, form_errors=form_errors)
                else:
                    dictToSaveFinancialyear = {
                        'company_id': company_obj,
                        'fin_year_code': dict_to_save['fin_year_code'],
                        'starts_from': dict_to_save['startdate'],
                        'starts_to': dict_to_save['enddate'],
                        'data_add_by': logged_in_user,
                        'data_add_mac': dict_to_save['data_add_mac'],
                    }
                    form_data = FinancialYear(**dictToSaveFinancialyear)
                    form_data.save()
                    return self.respond(success=True)
        except IntegrityError:
            return self.respond(success=False)
            handle_exception()

    def get_financialyear(self, financialyear_id):
        fin_year_obj = FinancialYear.objects.filter(pk=financialyear_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                fin_year_obj,
            )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_financialyear(self, financialyear_id, dict_to_save):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        fin_year_obj = FinancialYear.objects.get(pk=financialyear_id)
        dictToSaveFinancialyear = {
            'company_id': fin_year_obj.company_id,
            'fin_year_code': dict_to_save['fin_year_code'],
            'starts_from': dict_to_save['startdate'],
            'starts_to': dict_to_save['enddate'],
            'data_add_by': fin_year_obj.data_add_by,
            'data_add_mac': fin_year_obj.data_add_mac,
            'data_modi_on': datetime.datetime.now(),
            'data_modi_mac': dict_to_save['data_add_mac'],
            'data_modi_by': logged_in_user,
        }

        FinancialYear.objects.filter(pk=financialyear_id).update(
            **dictToSaveFinancialyear)
        # for key, value in form_values:
        #         setattr(fin_year_obj, key, value)
        # fin_year_obj.save()
        return self.respond(success=True)

    def prepare_search(self, financialyears, **search_kwargs):
        if search_kwargs.get('search_text'):
            financialyears = financialyears.filter(
                Q(fin_year_code__icontains=search_kwargs.get('search_text'))
            )
        return financialyears.order_by('-data_modi_on')


class BranchMasterAPI(ModuleAPI):
    model = BranchMaster

    def save_branch(self, dict_to_save):
        # save branch details
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        # checks whether any branch exists with the branch code
        branch_obj = BranchMaster.objects.filter(
            branch_shortname=dict_to_save['branch_code'])
        if branch_obj:
            form_errors = {
                'branch_code_error': '* Branch code already exists',
            }
            return self.respond(success=False, form_errors=form_errors)

        # checks whether any branch exists with the branch name
        branch_obj = BranchMaster.objects.filter(
            branch_name=dict_to_save['branch_name'])
        if branch_obj:
            form_errors = {
                'branch_name_error': '* Branch name already exists',
            }
            return self.respond(success=False, form_errors=form_errors)

        # checks whether any branch as head office exists
        if dict_to_save['is_head_office']:
            branch_obj = BranchMaster.objects.filter(is_corporate_office=True)
            print 'branch_obj ', branch_obj
            if branch_obj:
                form_errors = {
                    'head_office_exists_error': '* Head office already exists',
                }
                return self.respond(success=False, form_errors=form_errors)

        # all constraints satisfied , add new branch
        company_obj = CompanyMaster.objects.all().first()
        dictToSaveBranch = {
            'company_id': company_obj,
            'branch_shortname': dict_to_save['branch_code'],
            'branch_name': dict_to_save['branch_name'],
            'branch_addressline1': dict_to_save['branch_address1'],
            'branch_addressline2': dict_to_save['branch_address2'],
            'branch_addressline3': dict_to_save['branch_address3'],
            'branch_phone': dict_to_save['branch_phone'],
            'branch_rptaddrline1': dict_to_save['branch_rptaddrline1'],
            'branch_rptaddrline2': dict_to_save['branch_rptaddrline2'],
            'is_corporate_office': dict_to_save['is_head_office'],
            'data_add_by': logged_in_user,
            'data_add_mac': dict_to_save['data_add_mac'],
        }
        form_data = BranchMaster(**dictToSaveBranch)
        form_data.save()
        return self.respond(success=True)

    def get_branch(self, branch_id):
        # returns the branch details
        branch_obj = BranchMaster.objects.filter(pk=branch_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                branch_obj,
            )
        )
        return self.respond(
            objects=serialized_objects,
        )

    def edit_branch(self, branch_id, dict_to_save):
        # edit a branch
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        # checks whether any branch exists with the branch code
        branch_obj = BranchMaster.objects.filter(
            branch_shortname=dict_to_save['branch_code']).exclude(pk=branch_id)
        if branch_obj:
            form_errors = {
                'branch_code_error': '* Branch code already exists',
            }
            return self.respond(success=False, form_errors=form_errors)

        # checks whether any branch exists with the branch name
        branch_obj = BranchMaster.objects.filter(
            branch_name=dict_to_save['branch_name']).exclude(pk=branch_id)
        if branch_obj:
            form_errors = {
                'branch_name_error': '* Branch name already exists',
            }
            return self.respond(success=False, form_errors=form_errors)

        # checks whether any branch as head office exists
        if dict_to_save['is_head_office']:
            branch_obj = BranchMaster.objects.filter(
                is_corporate_office=True).exclude(pk=branch_id)
            print 'branch_obj ', branch_obj
            if branch_obj:
                form_errors = {
                    'head_office_exists_error': '* Head office already exists',
                }
                return self.respond(success=False, form_errors=form_errors)

        # all constraints satisfied, updates the branch
        branch_obj = BranchMaster.objects.get(pk=branch_id)
        dictToSaveBranch = {
            'company_id': branch_obj.company_id,
            'branch_shortname': dict_to_save['branch_code'],
            'branch_name': dict_to_save['branch_name'],
            'branch_addressline1': dict_to_save['branch_address1'],
            'branch_addressline2': dict_to_save['branch_address2'],
            'branch_addressline3': dict_to_save['branch_address3'],
            'branch_phone': dict_to_save['branch_phone'],
            'branch_rptaddrline1': dict_to_save['branch_rptaddrline1'],
            'branch_rptaddrline2': dict_to_save['branch_rptaddrline2'],
            'is_corporate_office': dict_to_save['is_head_office'],
            'data_modi_by': logged_in_user,
            'data_modi_mac': dict_to_save['data_add_mac'],
        }
        BranchMaster.objects.filter(pk=branch_id).update(
            **dictToSaveBranch)
        return self.respond(success=True)

    def delete_one(self, branch_id):
        # delete a branch
        branch_obj = BranchMaster.objects.get(pk=branch_id)
        branch_obj.delete()
        return self.respond(success=True)

    def prepare_search(self, branches, **search_kwargs):
        # manages branch search
        if search_kwargs.get('search_text'):
            branches = branches.filter(
                Q(branch_shortname__icontains=search_kwargs.get('search_text')) |
                Q(branch_name__icontains=search_kwargs.get('search_text'))
            )
        return branches.order_by('-data_modi_on')


class UserPermissionsAPI(ModuleAPI):
    model = UserRights

    def save_branch(self, dict_to_save):
        # save branch details
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        # checks whether any branch exists with the branch code
        branch_obj = BranchMaster.objects.filter(
            branch_shortname=dict_to_save['branch_code'])
        if branch_obj:
            form_errors = {
                'branch_code_error': '* Branch code already exists',
            }
            return self.respond(success=False, form_errors=form_errors)

        # checks whether any branch exists with the branch name
        branch_obj = BranchMaster.objects.filter(
            branch_name=dict_to_save['branch_name'])
        if branch_obj:
            form_errors = {
                'branch_name_error': '* Branch name already exists',
            }
            return self.respond(success=False, form_errors=form_errors)

        # checks whether any branch as head office exists
        if dict_to_save['is_head_office']:
            branch_obj = BranchMaster.objects.filter(is_corporate_office=True)
            print 'branch_obj ', branch_obj
            if branch_obj:
                form_errors = {
                    'head_office_exists_error': '* Head office already exists',
                }
                return self.respond(success=False, form_errors=form_errors)

        # all constraints satisfied , add new branch
        company_obj = CompanyMaster.objects.all().first()
        dictToSaveBranch = {
            'company_id': company_obj,
            'branch_shortname': dict_to_save['branch_code'],
            'branch_name': dict_to_save['branch_name'],
            'branch_addressline1': dict_to_save['branch_address1'],
            'branch_addressline2': dict_to_save['branch_address2'],
            'branch_addressline3': dict_to_save['branch_address3'],
            'branch_phone': dict_to_save['branch_phone'],
            'branch_rptaddrline1': dict_to_save['branch_rptaddrline1'],
            'branch_rptaddrline2': dict_to_save['branch_rptaddrline2'],
            'is_corporate_office': dict_to_save['is_head_office'],
            'data_add_by': logged_in_user,
            'data_add_mac': dict_to_save['data_add_mac'],
        }
        form_data = BranchMaster(**dictToSaveBranch)
        form_data.save()
        return self.respond(success=True)

    def get_user_permission(self, user_id):
        # returns the branch details
        user_master = UserMaster.objects.get(pk=user_id)
        modules = user_master.modules.filter(active=True).all()
        branches = user_master.branches.all()
        module_details = []
        for module in modules:
            menus = []
            menu_list = module.menudetails_set.filter(active=True).all()
            for menu in menu_list:
                user_right = UserRights.objects.filter(user_id=user_id, menu_id=menu.menu_id).values(
                    'add_permitted', 'edit_permitted', 'delete_permitted', 'view_permitted').first()
                if not user_right:
                    user_right = {'add_permitted': False,
                                  'edit_permitted': False, 'delete_permitted': False, 'view_permitted': False}
                user_right.update(
                    {'menu_id': menu.menu_id, 'menu_name': menu.menu_name})
                menus.append(user_right)
            if menus:
                module_details.append(
                    {'module_name': module.module_name, 'module_id': module.module_id, 'menus': menus})
        module_details = json.dumps(module_details)
        return self.respond(
            objects=module_details,
        )

    def save_user_permission(self, user_id, *module_details):
        user = UserMaster.objects.get(pk=user_id)
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        data_add_mac = ''
        UserRights.objects.filter(user_id=user_id).all().delete()
        for module in module_details:
            _mac = module.get('data_add_mac')
            if _mac:
                data_add_mac = _mac
            for menu in module['menus']:
                if True in menu.values():
                    # add permission only user have atleast one permission
                    user_right_entry = UserRights(
                        user_id=user,
                        menu_id=MenuDetails.objects.get(
                            pk=menu['menu_id']),
                        add_permitted=menu['add_permitted'],
                        edit_permitted=menu['edit_permitted'],
                        delete_permitted=menu['delete_permitted'],
                        view_permitted=menu['view_permitted'],
                        data_add_by=logged_in_user,
                        data_add_mac=data_add_mac,
                    )
                    user_right_entry.save()
        return self.respond(success=True)

    def prepare_search(self, users, **search_kwargs):
        # manages branch search
        if search_kwargs.get('search_text'):
            users = users.filter(
                Q(email_id__icontains=search_kwargs.get('search_text')) |
                Q(user_name__icontains=search_kwargs.get('search_text'))
            )
        return users.order_by('-data_modi_on')


class ModuleMasterAPI(ModuleAPI):
    model = ModuleMaster


class UserMasterAPI(ModuleAPI):
    model = UserMaster
    relations_to_serialize = {
        'user': '',
    }
    extras = ('')

    @transaction.atomic
    def save_user(self, form_values, branch_list, module_list):
        subject = "Registartion with KCA"
        branch_list = sorted(branch_list)
        module_list = sorted(module_list)

        clean_email = User.objects.filter(email=form_values['email']).first()
        if clean_email:
            form_errors = {
                'email_error': 'This email Id already exists',
            }
            return self.respond(success=False, form_errors=form_errors)
        else:
            try:
                with transaction.atomic():
                    logged_in_user = Manager.get_logged_in_user(
                        self.httpRequest.user)
                    password = User.objects.make_random_password()
                    user_auth = User.objects.create_user(
                        first_name=form_values['username'],
                        username=form_values['email'],
                        password=password,
                        email=form_values['email'],
                    )
                    dictToSaveUserMaster = {
                        'company_id': company_obj,
                        'user': user_auth,
                        'address1': form_values['address1'],
                        'address2': form_values['address2'],
                        'phone': form_values['phone'],
                        'adminuser': False,
                        'data_add_by': logged_in_user,
                        'data_add_mac': form_values['data_add_mac'],
                        'default_branch_id': BranchMaster.objects.get(pk=branch_list[0]),
                        'default_module_id': ModuleMaster.objects.get(pk=module_list[0]),
                        'user_name': form_values['username'],
                        'is_active': True,
                        'email_id': form_values['email'],
                    }
                    usermaster_data = UserMaster(**dictToSaveUserMaster)
                    usermaster_data.save()
                    for branch in branch_list:
                        branch_obj = BranchMaster.objects.get(pk=branch)
                        usermaster_data.branches.add(branch_obj)
                    for module in module_list:
                        module_obj = ModuleMaster.objects.get(pk=module)
                        usermaster_data.modules.add(module_obj)
                    message = """This Email id is registered with Kca.com. Your Login details are
                                    username : """ + form_values['email'] + """
                                    password : """ + password + """

                                    please login to continue


                                    Thanks
                    """
                    send_mail('KCA Registration', message, 'kcsbydbsspl@gmail.com',
                              [user_auth.email], fail_silently=False)

                    # SendNotification(
                    #     usermaster_data,
                    #     'send_user_create_mail',
                    #     usermaster_data,
                    #     password,
                    #     [user_auth.email]
                    # ).start()
                    return self.respond(success=True)

            except IntegrityError:
                form_errors = {
                    'integrity_error': 'Something went wrong, Pls try again later',
                }
                return self.respond(success=False, form_errors=form_errors)
                # handle_exception()

    def get_active_users(self, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        objects = self.prepare_search(
            self.model.objects.filter(is_active=True).exclude(adminuser=True).all(), **search_kwargs)
        prev_page_no = 0
        next_page_no = 0
        current_page_number = 0
        num_of_pages = objects.count()
        if not int(show_all):
            pagination_values = paginator(objects, page_no, per_page)
            objects = pagination_values['objects']
            prev_page_no = pagination_values['prev_page_no']
            next_page_no = pagination_values['next_page_no']
            current_page_number = pagination_values['current_page_number']
            num_of_pages = pagination_values['num_of_pages']
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                objects,
                # relations=self.relations_to_serialize,
                # extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
            previous_page_number=prev_page_no,
            next_page_number=next_page_no,
            current_page_number=current_page_number,
            num_of_pages=num_of_pages,
            per_page=per_page
        )

    def get_all(self, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        # objects =
        # self.prepare_search(self.model.objects.filter(user__is_active=True),
        # **search_kwargs)
        objects = self.prepare_search(
            self.model.objects.all(), **search_kwargs)

        prev_page_no = 0
        next_page_no = 0
        current_page_number = 0
        num_of_pages = objects.count()
        if not int(show_all):
            pagination_values = paginator(objects, page_no, per_page)
            objects = pagination_values['objects']
            prev_page_no = pagination_values['prev_page_no']
            next_page_no = pagination_values['next_page_no']
            current_page_number = pagination_values['current_page_number']
            num_of_pages = pagination_values['num_of_pages']
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                objects,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
            previous_page_number=prev_page_no,
            next_page_number=next_page_no,
            current_page_number=current_page_number,
            num_of_pages=num_of_pages,
            per_page=per_page
        )

    def get_user(self, user_id):
        user_obj = self.model.objects.filter(pk=user_id)
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                user_obj,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
            success=True
        )

    @transaction.atomic
    def delete_one(self, user_id):
        try:
            with transaction.atomic():
                usermaster_obj = UserMaster.objects.get(pk=user_id)
                # Delete Usermaster-Branch Relation
                usermaster_obj.branches.clear()
                # Delete Usermaster- Module Relation
                usermaster_obj.modules.clear()
                authuser_obj = self.model.objects.get(pk=user_id).user
                authuser_obj.delete()
                usermaster_obj.delete()
                # authuser_obj.is_active=False;
                # authuser_obj.save()
                return self.respond(success=True)
        except IntegrityError:
            form_errors = {
                'user_delete_error': 'User deletion is not possible due to usage in other forms',
            }
            return self.respond(success=False, form_errors=form_errors)
            # handle_exception()

    def prepare_search(self, users, **search_kwargs):
        logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
        if search_kwargs.get('filter_branch'):
            branch_id = self.httpRequest.session.get('default_branch_id')
            print 'default_branch_id is ', branch_id
            users = UserMaster.objects.filter(
                Q(is_active=True),
                (Q(adminuser=True) |
                 Q(branches=branch_id))
            ).all()
            return users

        if search_kwargs.get('search_text'):
            users = users.filter(
                Q(user__email__icontains=search_kwargs.get('search_text')) |
                Q(address1__icontains=search_kwargs.get('search_text')) |
                Q(user__first_name__icontains=search_kwargs.get('search_text'))
            )
        users = users.exclude(user__pk=logged_in_user.pk)
        return users.order_by('-data_modi_on')

    # User edit
    def edit_user(self, user_id, form_values, branch_list, module_list):

        usermaster_obj = self.model.objects.get(pk=user_id)
        usermaster_email_obj = self.model.objects.get(
            user__email=form_values['email'])
        if usermaster_email_obj.pk != usermaster_obj.pk:
            form_errors = {
                'email_error': 'This email Id already exists',
            }
            return self.respond(success=False, form_errors=form_errors)
        else:
            branch_list = sorted(branch_list)
            module_list = sorted(module_list)
            logged_in_user = Manager.get_logged_in_user(self.httpRequest.user)
            usermaster_obj = self.model.objects.get(pk=user_id)
            dictToSaveUserMaster = {
                'address1': form_values['address1'],
                'address2': form_values['address2'],
                'phone': form_values['phone'],
                'data_modi_by': logged_in_user,
                'data_modi_mac': form_values['data_add_mac'],
                'default_branch_id': BranchMaster.objects.get(pk=branch_list[0]),
                'default_module_id': ModuleMaster.objects.get(pk=module_list[0]),
                'user_name': form_values['username'],
                'is_active': form_values['is_active'],
                'email_id': form_values['email'],
            }

            usermaster_saved_obj = UserMaster.objects.filter(pk=user_id).update(
                **dictToSaveUserMaster)
            dictToSaveAuthUser = {
                'first_name': form_values['username'],
                'username': form_values['email'],
                'email': form_values['email'],
                'is_active': form_values['is_active'],
            }
            User.objects.filter(pk=usermaster_obj.user.pk).update(
                **dictToSaveAuthUser)

            # User-Branch Update
            UserMaster.objects.get(pk=user_id).branches.clear()
            for branch in branch_list:
                if not usermaster_obj.branches.filter(pk=branch).exists():
                    branch_obj = BranchMaster.objects.get(pk=branch)
                    usermaster_obj.branches.add(branch_obj)
            # User-Module Update
            UserMaster.objects.get(pk=user_id).modules.clear()
            for module in module_list:
                if not usermaster_obj.modules.filter(pk=module).exists():
                    module_obj = ModuleMaster.objects.get(pk=module)
                    usermaster_obj.modules.add(module_obj)
            return self.respond(success=True)
