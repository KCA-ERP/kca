from django import forms
from models import *


class _BaseForm(object):

    def clean(self):
        for field in self.cleaned_data:
            if isinstance(self.cleaned_data[field], basestring):
                self.cleaned_data[field] = self.cleaned_data[field].strip()
        return self.cleaned_data


class AddCompanyForm(_BaseForm, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AddCompanyForm, self).__init__(*args, **kwargs)
        self.fields['company_addressline1'].required = False
        self.fields['company_name'].required = True

    class Meta:
        model = CompanyMaster
        widgets = {
            'company_name': forms.TextInput(attrs={'class': 'span6', 'autofocus': 'autofocus', 'id': 'compname'}),
            'company_addressline1': forms.TextInput(attrs={'class': 'span6', 'id': 'addr1'}),
            'company_addressline2': forms.TextInput(attrs={'class': 'span6', 'id': 'addr2'}),
            'company_addressline3': forms.TextInput(attrs={'class': 'span6', 'id': 'addr3'}),
            'company_phone': forms.TextInput(attrs={'class': 'span6', 'autofocus': 'autofocus', 'id': 'phone'}),
            'company_rptaddrline1': forms.TextInput(attrs={'class': 'span6', 'autofocus': 'autofocus', 'id': 'raddr1'}),
            'company_repaddrline2': forms.TextInput(attrs={'class': 'span6', 'autofocus': 'autofocus', 'id': 'raddr2'}),
            'company_logo': forms.FileInput(attrs={'class': 'btn btn-primary', 'autofocus': 'autofocus'}),

        }
        exclude = ['company_id', 'image_type', 'data_add_by', 'data_add_on',
                   'data_add_mac', 'data_modi_by', 'data_mod_ion', 'data_modi_mac']


class AddBranchForm(_BaseForm, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AddBranchForm, self).__init__(*args, **kwargs)
        self.fields['branch_shortname'].required = True
        self.fields['branch_name'].required = True

    class Meta:
        model = BranchMaster
        widgets = {
            'branch_shortname': forms.TextInput(attrs={'class': 'span2', 'autofocus': 'autofocus', 'id': 'branchcode'}),
            'branch_name': forms.TextInput(attrs={'class': 'span6', 'id': 'branchname'}),
            'branch_addressline1': forms.TextInput(attrs={'class': 'span6', 'id': 'addr1'}),
            'branch_addressline2': forms.TextInput(attrs={'class': 'span6', 'id': 'addr2'}),
            'branch_addressline3': forms.TextInput(attrs={'class': 'span6', 'id': 'addr3'}),
            'branch_phone': forms.TextInput(attrs={'class': 'span6', 'autofocus': 'autofocus', 'id': 'branchphone'}),
            'branch_rptaddrline1': forms.TextInput(attrs={'class': 'span6', 'autofocus': 'autofocus', 'id': 'raddr1'}),
            'branch_repaddrline2': forms.TextInput(attrs={'class': 'span6', 'autofocus': 'autofocus', 'id': 'raddr2'}),
            'is_corporate_office': forms.CheckboxInput(attrs={'class': 'btn btn-primary', 'autofocus': 'autofocus'}),

        }
        exclude = ['company_id', 'branch_id', 'data_add_by', 'data_add_on',
                   'data_add_mac', 'data_modi_by', 'data_modi_on', 'data_modi_mac']


# class FinancialyearForm(_BaseForm,forms.ModelForm):
#     class Meta:
#         model = FinancialYear
#         widgets = {
#             'fin_year_code' : forms.TextInput(attrs={'class': 'span4','id': 'fyc'}),
#             'starts_from' : forms.TextInput(attrs={'class': 'dp1 span4','data-provide':'datepicker-inline','id':'startdate'}),
#             'starts_to' : forms.TextInput(attrs={'class': 'dp1 span4','data-provide':'datepicker-inline','id':'enddate'}),

#         }
#         exclude = ['fin_year_id','data_add_by','company_id']
