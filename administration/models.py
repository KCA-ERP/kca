import os
import uuid
from django.db import models
from django.contrib.auth.models import User


def get_file_path(instance, filename):
    """ returns the file location to store images """
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('uploads/company', filename)


class UserMaster(models.Model):

    """ User master table model """

    company_id = models.ForeignKey(
        'CompanyMaster', db_column='nCompanyID', on_delete=models.PROTECT)
    user = models.OneToOneField(User)
    user_name = models.CharField(
        db_column='cUserName', max_length=150)
    address1 = models.CharField(
        db_column='cAddress1', max_length=50, blank=True)
    address2 = models.CharField(
        db_column='cAddress2', max_length=50, blank=True)
    phone = models.CharField(db_column='cPhone', max_length=50, blank=True)
    is_active = models.BooleanField(db_column='nActive', default=None)
    email_id = models.EmailField(db_column='cEmailId')
    adminuser = models.BooleanField(db_column='nAdminUser', default=None)
    data_add_by = models.ForeignKey(
        'self', db_column='nDataAddBy', blank=True, null=True, related_name='user_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True, blank=True, null=True)
    data_add_mac = models.CharField(
        db_column='cDataAddMac', max_length=50, blank=True)
    data_modi_by = models.ForeignKey(
        'self', db_column='nDataModiBy', blank=True, null=True, related_name='user_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True)
    default_branch_id = models.ForeignKey('BranchMaster',
                                          db_column='nDefaultBranchID', null=True, on_delete=models.PROTECT)
    default_module_id = models.ForeignKey('ModuleMaster',
                                          db_column='nDefaultModuleID', null=True, on_delete=models.PROTECT)
    branches = models.ManyToManyField(
        'BranchMaster', related_name='branch_relation')
    modules = models.ManyToManyField(
        'ModuleMaster', related_name='module_relation')

    def __unicode__(self):
        return unicode(self.user)

    class Meta:

        db_table = 'tsmusermaster'


class CompanyMaster(models.Model):

    """ Company Master table model """

    company_id = models.AutoField(db_column='nCompanyId', primary_key=True)
    company_name = models.CharField(db_column='cCompanyName', max_length=100)
    company_addressline1 = models.CharField(
        db_column='cCompanyAddressLine1', max_length=50, blank=True, null=True)
    company_addressline2 = models.CharField(
        db_column='cCompanyAddressLine2', max_length=50, blank=True, null=True)
    company_addressline3 = models.CharField(
        db_column='cCompanyAddressLine3', max_length=50, blank=True, null=True)
    company_phone = models.CharField(
        db_column='cCompanyPhone', max_length=50, blank=True, null=True)
    company_rptaddrline1 = models.CharField(
        db_column='cCompanyRptAddrLine1', max_length=50, blank=True, null=True)
    company_rptaddrline2 = models.CharField(
        db_column='cCompanyRptAddrLine2', max_length=50, blank=True, null=True)
    company_logo = models.ImageField(
        db_column='iCompanyLogo', upload_to=get_file_path, null=True)
    image_type = models.CharField(
        db_column='cImageType', max_length=50, blank=True, null=True)
    data_add_by = models.ForeignKey('UserMaster',
                                    db_column='nDataAddBy', blank=True, null=True, related_name='company_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey('UserMaster',
                                     db_column='nDataModiBy', blank=True, null=True, related_name='company_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.company_id)

    class Meta:
        db_table = 'tsmcompanymaster'


class BranchMaster(models.Model):

    """ branch master table model """

    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyId', related_name='branch_company_relation', on_delete=models.PROTECT)
    branch_id = models.AutoField(db_column='nBranchId', primary_key=True)
    branch_shortname = models.CharField(
        db_column='nBranchShortName', max_length=3)
    branch_name = models.CharField(db_column='cBranchName', max_length=100)
    branch_addressline1 = models.CharField(
        db_column='cBranchAddressLine1', max_length=50, blank=True, null=True)
    branch_addressline2 = models.CharField(
        db_column='cBranchAddressLine2', max_length=50, blank=True, null=True)
    branch_addressline3 = models.CharField(
        db_column='cBranchAddressLine3', max_length=50, blank=True, null=True)
    branch_phone = models.CharField(
        db_column='cBranchPhone', max_length=50, blank=True, null=True)
    branch_rptaddrline1 = models.CharField(
        db_column='cBranchRptAddrLine1', max_length=50, blank=True, null=True)
    branch_rptaddrline2 = models.CharField(
        db_column='cBranchRptAddrLine2', max_length=50, blank=True, null=True)
    is_corporate_office = models.BooleanField(
        db_column='cCorporateOffice', default=None)
    data_add_by = models.ForeignKey(
        UserMaster, db_column='nDataAddBy', blank=True, null=True, related_name='branch_creater', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(
        db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(
        UserMaster, db_column='nDataModiBy', blank=True, null=True, related_name='branch_updater', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.branch_id)

    class Meta:
        db_table = 'tsmbranchmaster'


class ModuleMaster(models.Model):

    """ module master table model """

    module_id = models.AutoField(db_column='nModuleId', primary_key=True)
    module_name = models.CharField(db_column='cModuleName', max_length=50)
    active = models.BooleanField(db_column='cActive', default=True)

    def __unicode__(self):
        return unicode(self.module_id)

    class Meta:
        db_table = 'tsmmodulemaster'


class MenuDetails(models.Model):

    """ menu details table model """

    menu_id = models.AutoField(db_column='nMenuId', primary_key=True)
    menu_name = models.CharField(db_column='cMenuName', max_length=100)
    parent_id = models.ForeignKey(
        'self', db_column='nParentId', blank=True, null=True)
    module_id = models.ForeignKey(
        ModuleMaster, db_column='nModuleID')
    active = models.BooleanField(db_column='cActive', default=True)
    linkpage = models.CharField(db_column='cLinkPage', max_length=150)
    sort_order = models.IntegerField(db_column='nSortOrder')
    icon_class = models.CharField(
        db_column='cIconClass', max_length=50, null=True)

    def __unicode__(self):
        return unicode(self.menu_id)

    class Meta:
        db_table = 'tsmmenudetails'


class UserRights(models.Model):

    """ user rights table model """

    user_id = models.ForeignKey(
        UserMaster, db_column='nUserID', related_name='user_user_rights_relation')
    menu_id = models.ForeignKey(
        MenuDetails, db_column='nMenuID',  related_name='menu_usr_rights_relation')
    add_permitted = models.BooleanField(
        db_column='cAddPermitted', default=None)
    edit_permitted = models.BooleanField(
        db_column='cEditPermitted', default=None)
    delete_permitted = models.BooleanField(
        db_column='cDeletePermitted', default=None)
    view_permitted = models.BooleanField(
        db_column='cViewPermitted', default=None)
    data_add_by = models.ForeignKey(
        UserMaster, db_column='nDataAddBy', related_name='usr_rights_creater', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(
        UserMaster, db_column='nDataModiBy', blank=True, null=True, related_name='usr_rights_updater', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'tsmuserrights'


class FinancialYear(models.Model):

    """ financial year table model """

    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyId', related_name='fin_year_company_relation', on_delete=models.PROTECT)
    fin_year_id = models.AutoField(db_column='nFinYearID', primary_key=True)
    fin_year_code = models.CharField(db_column='cFinYearCode', max_length=10)
    starts_from = models.DateTimeField(db_column='dStartsFrom')
    starts_to = models.DateTimeField(db_column='dStartsTo')
    data_add_by = models.ForeignKey(
        UserMaster, db_column='nDataAddBy', related_name='fin_year_creater', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(
        UserMaster, db_column='nDataModiBy', blank=True, null=True, related_name='fin_year_updater', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now_add=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.fin_year_id)

    class Meta:
        db_table = 'tsmfinancialyear'
