import os
from django.views.generic import View
from forms import *
from .models import *
from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.models import User
from kca.common.utils import get_client_ip
from kca.common.permission_check import Manager


class CompanyAddView(View):
    form_class = AddCompanyForm
    template_name = 'administration/add_company.html'

    def get(self, request):
        print '-' * 25
        menu_id = request.session.get('menu_id')
        form = self.form_class
        save_permitted = False
        user_right = None
        logged_in_user = Manager.get_logged_in_user(request.user)
        if menu_id:
            user_right = UserRights.objects.filter(user_id=logged_in_user, menu_id=menu_id).filter(
                Q(add_permitted=True) | Q(edit_permitted=True)).all()
        if user_right or logged_in_user.adminuser:
            save_permitted = True
        company_obj = CompanyMaster.objects.all().first()
        if company_obj:
            form = form(
                initial={
                    'company_name': company_obj.company_name,
                    'company_addressline1': company_obj.company_addressline1,
                    'company_addressline2': company_obj.company_addressline2,
                    'company_addressline3': company_obj.company_addressline3,
                    'company_phone': company_obj.company_phone,
                    'company_rptaddrline1': company_obj.company_rptaddrline1,
                    'company_rptaddrline2': company_obj.company_rptaddrline2,
                    'company_logo': company_obj.company_logo,
                }
            )
        else:
            print "No company objects"
        request.session['menu_id'] = menu_id
        context = {
            'form': form,
            'id': menu_id,
            'save_permitted': save_permitted
        }
        return render(request, self.template_name, context)

    def post(self, request):

        menu_id = request.session.get('menu_id')
        save_permitted = False
        user_right = None
        logged_in_user = Manager.get_logged_in_user(request.user)
        if menu_id:
            user_right = UserRights.objects.filter(user_id=logged_in_user, menu_id=menu_id).filter(
                Q(add_permitted=True) | Q(edit_permitted=True)).all()
        if user_right or logged_in_user.adminuser:
            save_permitted = True
        company_obj = CompanyMaster.objects.all().first()
        if company_obj:
            # manages the update operation
            print request.FILES
            
            form = self.form_class(
                request.POST, request.FILES, instance=company_obj)
            form_obj = form.save(commit=False)
            form_obj.data_modi_by = request.logged_in_user
            form_obj.data_modi_mac = get_client_ip(request)
            alert_success = {'success': 'Changes saved successfully.'}
            alert_failed = {'failed': 'Failed to Edit Company'}
        else:
            # manages the insert operation
            form = self.form_class(request.POST, request.FILES)
            form_obj = form.save(commit=False)
            form_obj.data_add_by = request.logged_in_user
            form_obj.data_add_mac = get_client_ip(request)
            alert_success = {'success': 'Company successfully created.'}
            alert_failed = {'failed': 'Failed to Create Company'}
        if form.has_changed():
            # checks whether any changes in the form or not
            if form.is_valid():
                # checks whether the form is valid or not
                existing_image = CompanyMaster.objects.all(
                ).first().company_logo.name
                new_image_name = form.cleaned_data['company_logo'].name
                if existing_image != new_image_name:
                    # cheks whether the user has uploaded new image
                    from django.conf import settings
                    path = os.path.join(settings.MEDIA_ROOT, existing_image)
                    from sys import platform
                    if platform == 'win32':
                        # for windows machines
                        path = path.replace('/', '\\')
                    file_extension = new_image_name[
                        new_image_name.rfind('.'):].replace('.', '')
                    if os.path.isfile(path):
                        os.remove(path)
                    form_obj.image_type = file_extension
                form_obj.save()
                alert = alert_success
            else:
                alert = alert_failed
        else:
            print 'no changes'
            alert = {'failed':  'No changes to save.'}

        context = {
            'alert': alert,
            'form': form,
            'id': menu_id,
            'save_permitted': save_permitted
        }
        return render(request, self.template_name, context)


class FinancialyearView(View):
    template_name = 'administration/financialyear.html'

    def get(self, request):
        return render(request, self.template_name)


class BranchView(View):

    form_class = AddBranchForm
    template_name = 'administration/add_branch.html'

    def get(self, request):
        return render(request, self.template_name)


class UserView(View):
    template_name = 'administration/user.html'

    def get(self, request):
        return render(request, self.template_name)


class UserPermissionView(View):
    template_name = 'administration/user_permission.html'

    def get(self, request):
        return render(request, self.template_name)
