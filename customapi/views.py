import json
from django.shortcuts import render
from django import http
from django.views.generic import View
from config import APISelector
from error_controller import ErrorController
from kca.common.utils import get_client_ip
# from projectcmd.common.permission_check import Manager

# Create your views here.


class ResponseGenerator(object):

    def generate_response(self, values, status_code=200, content_type='application/json'):
        response = http.HttpResponse()
        response.status_code = status_code
        response.write(values)
        response['Content-Type'] = content_type
        return response


class ResourcesAPIView(View, ResponseGenerator):

    def get(self, request, api_end_point):
        args = request.GET.dict()
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(**args)
        return self.generate_response(response_values)

    def post(self, request, api_end_point):
        post_args = json.loads(request.body)
        ip_addr = get_client_ip(request)
        post_args[0].update({'data_add_mac': ip_addr})
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(*post_args)
        return self.generate_response(response_values)

    def delete(self, request, api_end_point):
        args = request.GET.dict()
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(**args)
        return self.generate_response(response_values)


class ResourceAPIView(View, ResponseGenerator):

    def get(self, request, api_end_point, id):
        args = request.GET.dict()
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id, **args)
        return self.generate_response(response_values)

    def post(self, request, api_end_point, id):
        post_args = json.loads(request.body)
        ip_addr = get_client_ip(request)
        post_args[0].update({'data_add_mac': ip_addr})
        print "after", post_args
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id, *post_args)
        return self.generate_response(response_values)

    def put(self, request, api_end_point, id):
        post_args = json.loads(request.body)
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id, *post_args)
        return self.generate_response(response_values)

    def delete(self, request, api_end_point, id):
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id)
        return self.generate_response(response_values)


class AssociatedResourcesAPIView(View, ResponseGenerator):

    def get(self, request, api_end_point, id, associated_resources):
        args = request.GET.dict()
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id, **args)
        return self.generate_response(response_values)

    def put(self, request, api_end_point, id, associated_resource):
        post_args = json.loads(request.body)
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id, *post_args)
        return self.generate_response(response_values)

    def post(self, request, api_end_point, id, associated_resource):
        post_args = json.loads(request.body)
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id, *post_args)
        return self.generate_response(response_values)

    def delete(self, request, api_end_point, id, associated_resource):
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id)
        return self.generate_response(response_values)


class AssociatedResourceAPIView(View, ResponseGenerator):

    def get(self, request, api_end_point, id, associated_resource, associated_resource_id):
        args = request.GET.dict()
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id, associated_resource_id, **args)
        return self.generate_response(response_values)

    def put(self, request, api_end_point, id, associated_resource, associated_resource_id):
        post_args = json.loads(request.body)
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(
            id, associated_resource_id, *post_args)
        return self.generate_response(response_values)

    def post(self, request, api_end_point, id, associated_resource, associated_resource_id):
        post_args = json.loads(request.body)
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(
            id, associated_resource_id, *post_args)
        return self.generate_response(response_values)

    def delete(self, request, api_end_point, id, associated_resource, associated_resource_id):
        errors = ErrorController(
            http_request=request,
            api_end_point=request.path
        ).run()
        if errors:
            response_values = json.dumps({
                'success': False,
                'api_errors': errors
            })
            return self.generate_response(response_values)
        api_class, fn_name = APISelector.get(request.path, request.method)
        api_instance = api_class()
        api_instance.httpRequest = request
        api_instance.user = request.user
        funtion_to_call = getattr(api_instance, fn_name, None)
        response_values = funtion_to_call(id, associated_resource_id)
        return self.generate_response(response_values)
