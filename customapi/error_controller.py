from kca.common.utils import BaseEnum
from config import APISelector


class ErrorCodeEnum(BaseEnum):
    def __init__(self, *args, **kwargs):
        self.NO_API = 1000
        self.DISABLED_API = 1001
        self.NO_RESOURCE = 1002
        self.MISSING_MANDATORY_FIELDS = 1003
        self.INVALID_HTTP_METHOD = 1004
        self.EVALUTION_LIMIT_REACHED = 1005
        self.EVALUTION_COMPLETED = 1006
        self.INSUFFICIENT_QUESTIONS = 1007

ErrorCode = ErrorCodeEnum()

ERROR_CODE_LABEL = {
    ErrorCode.NO_API: 'No such API defined.',
    ErrorCode.DISABLED_API: 'API is disabled.',
    ErrorCode.NO_RESOURCE: 'The targeted resource is unavailable.',
    ErrorCode.MISSING_MANDATORY_FIELDS: 'Some or all of mandatory fields found missing. Read the API docs.',
    ErrorCode.INVALID_HTTP_METHOD: 'API does not support %s HTTP method.',
    ErrorCode.EVALUTION_LIMIT_REACHED: 'Evaluation Limit Reached',
    ErrorCode.EVALUTION_COMPLETED: 'You have already completed this evaluation',
    ErrorCode.INSUFFICIENT_QUESTIONS: "The question bank doesn't have enough question for this evaluation"
}


class ErrorController(object):
    _mandatory_fields = []
    _integer_fields = []
    _float_fields = []
    _test_cases = ['_base_test', '_mandatory_fields_test']

    def __init__(self, **kwargs):
        self.kwargs = kwargs
        self.http_request = kwargs['http_request']
        self.api_end_point = kwargs['api_end_point']

    def _clean(self, **args):
        for key in self._float_fields:
            args[key] = float(args[key])
        for key in self._integer_fields:
            args[key] = int(args[key])
        return args

    def _mandatory_fields_test(self):
        if len(self._mandatory_fields) > 0:
            intersection_list = list(
                set(
                    self._mandatory_fields
                ).intersection(set(
                    self.kwargs.keys()
                ))
            )
            intersection_list.sort()
            self._mandatory_fields.sort()
            if intersection_list != self._mandatory_fields:
                return self.raise_error(ErrorCode.MISSING_MANDATORY_FIELDS)

    def _base_test(self):
        api_config = APISelector.get_configuration(self.api_end_point)
        if not api_config:
            return self.raise_error(
                ErrorCode.NO_API
            )
        elif not api_config.get('status'):
            return self.raise_error(
                ErrorCode.DISABLED_API
            )
        elif not api_config.get(self.http_request.method):
            return self.raise_error(
                ErrorCode.INVALID_HTTP_METHOD,
                self.http_request.method
            )

    def _user_test(self, user_id):
        raise NotImplementedError

    def _data_test(self):
        raise NotImplementedError

    def raise_error(self, code, *additional_args):
        try:
            message = ERROR_CODE_LABEL[code] % additional_args
        except TypeError as e:
            message = ERROR_CODE_LABEL[code]

        return {
            'code': code,
            'message': message
        }

    def clean_data(self, **args):
        return self._clean(**args)

    def run(self, **args):
        for case in self._test_cases:
            if args.get(case):
                errors = getattr(self, case)(args.get(case))
            else:
                errors = getattr(self, case)()
            if errors:
                return errors
